<?php

namespace GeekStar\MarkupBundle\Twig\Extension;

use GeekStar\MarkupBundle\Markup;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MarkupExtension extends AbstractExtension
{
    /**
     * @var Markup
     */
    protected $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('load', [$this->markup, 'load']),
        ];
    }
}