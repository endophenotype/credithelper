<?php

namespace GeekStar\MarkupBundle\Routing;

use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


class MarkupLoader extends YamlFileLoader
{
    private static array $availableKeys = [
        'resource',
        'type',
        'prefix',
        'path',
        'host',
        'schemes',
        'methods',
        'defaults',
        'requirements',
        'options',
        'condition',
        'template',
    ];

    /**
     * {@inheritdoc}
     */
    public function supports($resource, string $type = null): bool
    {
        return 'markup' === $type;
    }

    /**
     * Parses a route and adds it to the RouteCollection.
     *
     * @param RouteCollection $collection A RouteCollection instance
     * @param string $name Route name
     * @param array $config Route definition
     * @param string $path Full path of the YAML file being processed
     */
    protected function parseRoute(RouteCollection $collection, string $name, array $config, string $path)
    {
        $defaults = $config['defaults'] ?? [];
        $requirements = $config['requirements'] ?? [];
        $options = $config['options'] ?? [];
        $host = $config['host'] ?? '';
        $schemes = $config['schemes'] ?? [];
        $methods = $config['methods'] ?? [];
        $condition = $config['condition'] ?? null;

        $defaults['_controller'] = 'geek_star_markup.controller.template_controller::renderAction';
        $defaults['template'] = $config['template'];

        $route = new Route($config['path'], $defaults, $requirements, $options, $host, $schemes, $methods, $condition);

        $collection->add($name, $route);
    }

    /**
     * Validates the route configuration.
     *
     * @param array $config A resource config
     * @param string $name The config key
     * @param string $path The loaded file path
     *
     * @throws \InvalidArgumentException If one of the provided config keys is not supported,
     *                                   something is missing or the combination is nonsense
     */
    protected function validate($config, string $name, string $path)
    {
        if (!is_array($config)) {
            throw new \InvalidArgumentException(sprintf('The definition of "%s" in "%s" must be a YAML array.', $name, $path));
        }
        if ($extraKeys = array_diff(array_keys($config), self::$availableKeys)) {
            throw new \InvalidArgumentException(sprintf(
                'The routing file "%s" contains unsupported keys for "%s": "%s". Expected one of: "%s".',
                $path, $name, implode('", "', $extraKeys), implode('", "', self::$availableKeys)
            ));
        }
        if (isset($config['resource']) && isset($config['path'])) {
            throw new \InvalidArgumentException(sprintf(
                'The routing file "%s" must not specify both the "resource" key and the "path" key for "%s". Choose between an import and a route definition.',
                $path, $name
            ));
        }
        if (!isset($config['resource']) && isset($config['type'])) {
            throw new \InvalidArgumentException(sprintf(
                'The "type" key for the route definition "%s" in "%s" is unsupported. It is only available for imports in combination with the "resource" key.',
                $name, $path
            ));
        }
        if (!isset($config['resource']) && !isset($config['path'])) {
            throw new \InvalidArgumentException(sprintf(
                'You must define a "path" for the route "%s" in file "%s".',
                $name, $path
            ));
        }
        if (!isset($config['resource']) && !isset($config['template'])) {
            throw new \InvalidArgumentException(sprintf(
                'You must define a "template" for the route "%s" in file "%s".',
                $name, $path
            ));
        }
    }
}