<?php

namespace GeekStar\StorageBundle\Admin\Page\Components;

use GeekStar\AdminBundle\Elements\Components\TableComponent;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\StorageBundle\StorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class StorageFieldTableComponent extends TableComponent
{
    protected StorageManager $manager;

    protected ?array $fieldConfig = null;
    protected ?array $fieldData = null;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, StorageManager $manager)
    {
        parent::__construct($container, $twig, $translator);

        $this->manager = $manager;
    }

    function configure()
    {
        $confirmDeleteMessage = $this->translator->trans('geek_star_storage.alert.delete_item');

        $this
            ->setWithOutBorder(true)
            ->setStriped(true)
            ->setSortable(true);

        $this
            ->addColumn('_control', 'id', false, '
                <a href="#" class="js-field-item-edit" data-key="{{ _key }}">
                    <b>{{ _key }}</b>
                </a>
            ');

        $this
            ->addAction('render', '() => {
                let component = this;
                let $component = $(component);
                let $editButtons = $component.find(".js-field-item-edit");
                let $copyButtons = $component.find(".js-field-item-copy");
                let $deleteButtons = $component.find(".js-field-item-delete");
                
                $editButtons.on("click", function(e) {
                    e.preventDefault();
                    let $button = $(this);
                    let popup = component.root.components["field_item_popup"];
                
                    popup.actions.show({
                        showField: component.query.showField,
                        showFieldItem: true,
                        showFieldItemId: $button.data("key")
                    });
                });
                                
                $copyButtons.on("click", function(e) {
                    e.preventDefault();
                    let $button = $(this);
                    component.actions.copyItem($button.data("key"));
                });
                
                $deleteButtons.on("click", function(e) {
                    e.preventDefault();
                    let $button = $(this);
                    if (confirm("'. $confirmDeleteMessage . '")) {
                        component.actions.deleteItem($button.data("key"));
                    }
                });
            }');

        $this->addAction('onLoad', '() => {
            this.root.components["field_popup"].components["field_table"] = this;
        }');

        $this->addAction('deleteItem', '(key) => {
            this.events.deleteItem({
                deleteItem: key
            });
        }');

        $this->addEvent('deleteItem', '(response) => {
            if(response.error === undefined) {
                this.actions.load();
            }
        }');

        $this->addHandler('deleteItem', [$this, 'deleteItemHandler']);

        $this->addAction('copyItem', '(key) => {
            this.events.copyItem({
                copyItem: key
            });
        }');

        $this->addEvent('copyItem', '(response) => {
            if(response.error === undefined) {
                this.actions.load();
            }
        }');

        $this->addHandler('copyItem', [$this, 'copyItemHandler']);

        $this->addEvent('sort', '(response) => {
            if(response.error === undefined) {
                this.actions.load();
            }
        }');
    }

    function initialize(ComponentRequest $request, ComponentResponse $response)
    {
        if($showField = $request->query->get('showField')) {
            $key = $showField['key'] ?? '';
            $path = $showField['path'] ?? '';
            $this->fieldConfig = $this->manager->getFieldConfig($key, $path);
            if($this->fieldConfig) {
                $this->fieldData = $this->manager->getFieldData($key, $path);
                foreach ($this->fieldConfig['table_columns'] as $fieldColumn) {
                    $title = $this->fieldConfig['items'][$fieldColumn]['title'] ?? null;
                    if($title) {
                        $this
                            ->addColumn($fieldColumn, $title, false, '{{ ' . $fieldColumn . ' | raw }}');
                    }
                }
            }

            $this->addColumn('_buttons', '', true, '
                <div style="display: flex">
                    <a href="#" class="btn btn-sm btn-outline-dark js-field-item-edit" data-key="{{ _key }}" style="margin-right: 5px">
                        <i class="fa fa-pencil" aria-hidden="true"></i> 
                    </a>
                    <a href="#" class="btn btn-sm btn-outline-dark js-field-item-copy" data-key="{{ _key }}" style="margin-right: 5px">
                        <i class="fa fa-copy" aria-hidden="true"></i> 
                    </a>
                    <a href="#" class="btn btn-sm btn-outline-danger js-field-item-delete" data-key="{{ _key }}">
                        <i class="fa fa-trash" aria-hidden="true"></i> 
                    </a>
                </div>
            ');
        }
    }

    public function loadTable(ComponentRequest $request, ComponentResponse $response, array &$table)
    {
        if($this->fieldConfig) {
            $context = $this->fieldConfig['table_context'] ?: ($this->manager->contexts[0] ?? '');
            foreach ($this->fieldData as $itemId => $item) {
                $row = [
                    'sortIndex' => $item['sortIndex']
                ];
                foreach ($this->fieldConfig['table_columns'] as $column) {
                    $type = $this->fieldConfig['items'][$column]['type'];
                    $row[$column] = $this->manager->decorateTableValue($type, $item['data'][$context][$column]['value']);
                }
                $table[$itemId] = $row;
            }
        }
    }

    public function deleteItemHandler(ComponentRequest $request, ComponentResponse $response)
    {
        if($showField = $request->query->get('showField')) {
            $key = $showField['key'] ?? '';
            $path = $showField['path'] ?? '';
            $field = $this->manager->getField($key, $path, false);

            if($field) {
                $idItem = $request->query->get('deleteItem');
                $item = $this->manager->getFieldItem($field, $idItem);
                if($item) {
                    $item->delete();
                }
            }
        }
    }

    public function copyItemHandler(ComponentRequest $request, ComponentResponse $response)
    {
        if($showField = $request->query->get('showField')) {
            $key = $showField['key'] ?? '';
            $path = $showField['path'] ?? '';
            $field = $this->manager->getField($key, $path, false);

            if($field) {
                $idItem = $request->query->get('copyItem');
                $item = $this->manager->getFieldItem($field, $idItem);
                if($item) {
                    $this->manager->copyItem($item);
                }
            }
        }
    }

    public function sortHandler(ComponentRequest $request, ComponentResponse $response)
    {
        if($showField = $request->query->get('showField')) {
            $key = $showField['key'] ?? '';
            $path = $showField['path'] ?? '';
            $field = $this->manager->getField($key, $path, false);

            if($field) {
                $sort = $request->query->get('sort');
                if($sortItems = ($sort['items'] ?? null)) {
                    foreach ($sortItems as $itemKey => $sortIndex) {
                        $item = $this->manager->getFieldItem($field, $itemKey);
                        if($item) {
                            $item
                                ->setSortableRank($sortIndex)
                                ->save();
                        }
                    }
                }
            }
        }
    }
}