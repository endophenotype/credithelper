<?php

namespace GeekStar\StorageBundle\Admin\Page\Components;

use GeekStar\AdminBundle\Elements\Components\TableComponent;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\StorageBundle\StorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class StorageTableComponent extends TableComponent
{
    protected StorageManager $manager;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, StorageManager $manager)
    {
        parent::__construct($container, $twig, $translator);

        $this->manager = $manager;
    }

    function configure()
    {
        $this
            ->setWithOutBorder(true)
            ->setStriped(true)
            ->setCollapsed(true)
            ->setCountFilters([
                '50' => 50,
                '150' => 100,
                'all' => $this->translator->trans('geek_star_admin.content.all'),
            ]);

        $this
            ->addColumn('_control', 'Наименование', false, '
                {% if isField %}
                    <a href="#" class="js-open-storage-field" data-key="{{ _key }}" data-path="{{ path }}" data-multiple="{{ multiple }}">
                        <i style = "text-align: center; width: 16px; height: 16px; margin-right: 5px;"
                           class="icon fa fa-file-o">
                        </i>
                        <b>{{ title }}</b>
                    </a>
                {% else %}
                    {{ title }}
                {% endif %}
            ')
            ->addColumn('key', 'Идентификатор', false, '<span>{{ _key }}</span>')
            ->addColumn('path', 'Путь', false, '<span>{{ path }}</span>');

        $this->addAction('render', '() => {
            let component = this;
            let $component = $(this);
            
            $component.find(".js-open-storage-field").on("click", function(e) {
                e.preventDefault();
                let $link = $(this);
                let isMultiple = $link.data("multiple");
                
                if(isMultiple) {
                    let popup = component.root.components["field_popup"];
                    popup.actions.show({
                        showField: {
                            key: $link.data("key"),
                            path: $link.data("path")
                        }
                    });
                } else {
                    let popup = component.root.components["field_item_popup"];
                    popup.actions.show({
                        showField: {
                            key: $link.data("key"),
                            path: $link.data("path")
                        },
                        showFieldItem: true
                    });
                }
            });
        }');
    }

    function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function loadTable(ComponentRequest $request, ComponentResponse $response, array &$table)
    {
        if($table = $this->manager->getTable()) {
            $table[array_key_first($table)]['_collapsed'] = true;
        }
    }

    public function decorate(ComponentRequest $request, ComponentResponse $response, array $tableRow, array &$templateRow, array $nodes = null, string $scope = null)
    {

    }
}