<?php

namespace GeekStar\StorageBundle\Admin\Page\Components;

use GeekStar\AdminBundle\Elements\Components\PanelComponent;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class StoragePanelComponent extends PanelComponent
{
    function configure()
    {
        $this
            ->setTitle($this->translator->trans('geek_star_storage.content.field_list'));
    }

    function initialize(ComponentRequest $request, ComponentResponse $response)
    {
        $this
            ->addContentItem(StorageTableComponent::class)->end();
    }
}