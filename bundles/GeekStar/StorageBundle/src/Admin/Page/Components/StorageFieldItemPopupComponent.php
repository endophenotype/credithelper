<?php

namespace GeekStar\StorageBundle\Admin\Page\Components;

use GeekStar\AdminBundle\Elements\Components\PopupComponent;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\StorageBundle\StorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class StorageFieldItemPopupComponent extends PopupComponent
{
    protected StorageManager $manager;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, StorageManager $manager)
    {
        parent::__construct($container, $twig, $translator);

        $this->manager = $manager;
    }

    function configure()
    {
        $this
            ->setTitle($this->translator->trans('geek_star_storage.content.edit_field_item'))
            ->setWidth('900px');

        $this->addAction('show', '(data) => {
            if(data.showField !== null) {
                this.components["field_form"].query.showField = data.showField;
            }
            this.events.open(data);
        }');

        $this->addEvent('open', '(response) => {
            let $component = $(this);
            let newComponent = $(response.content).insertAfter($component)[0];
            
            this.actions.destroy();
            
            this.root.initComponent(newComponent, response, this.root, this.parent);
            this.root.renderComponent(newComponent);
            
            newComponent.modal.show();
        }');

        $this->addAction('close', '() => {
            let table = this.root.components["field_popup"].components["field_table"];
            table.actions.load();
        }');

        $this->addHandler('open', [$this, 'openHandler']);
    }

    function initialize(ComponentRequest $request, ComponentResponse $response)
    {
        if($showField = $request->query->get('showField')) {
            $key  = $showField['key']  ?? '';
            $path = $showField['path'] ?? '';

            if($fieldConfig = $this->manager->getFieldConfig($key, $path))
            {
                $this->setTitle($fieldConfig['title']);
            }
        }

        $this
            ->addContentItem(StorageFieldFormComponent::class, 'field_form')->end();
    }

    public function openHandler(ComponentRequest $request, ComponentResponse $response)
    {
        $this->handle('load', $request, $response);
    }
}