<?php

namespace GeekStar\StorageBundle\Admin\Page\Components;

use GeekStar\AdminBundle\Elements\ComponentInterface;
use GeekStar\AdminBundle\Elements\Components\GroupComponent;
use GeekStar\AdminBundle\Elements\Components\TabComponent;
use GeekStar\AdminBundle\Elements\Components\FormComponent;
use GeekStar\AdminBundle\Elements\Components\TabItemComponent;
use GeekStar\AdminBundle\Elements\Fields\ButtonField;
use GeekStar\AdminBundle\Elements\Fields\CheckboxField;
use GeekStar\AdminBundle\Elements\Fields\DateField;
use GeekStar\AdminBundle\Elements\Fields\ImageField;
use GeekStar\AdminBundle\Elements\Fields\RichTextField;
use GeekStar\AdminBundle\Elements\Fields\TextAreaField;
use GeekStar\AdminBundle\Elements\Fields\TextField;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\AdminBundle\Service\Api\FormComponentValidator;
use GeekStar\StorageBundle\PropelModel\StorageField;
use GeekStar\StorageBundle\PropelModel\StorageFieldItem;
use GeekStar\StorageBundle\StorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class StorageFieldFormComponent extends FormComponent
{
    protected StorageManager $manager;
    protected ?array $fieldConfig = null;
    protected ?array $fieldData = null;

    protected array $contexts = [];

    protected ?int $fieldItemId = null;

    protected ?string $fieldKey = null;
    protected ?string $fieldPath = null;

    protected ?StorageField $field = null;
    protected ?StorageFieldItem $fieldItem = null;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, StorageManager $manager)
    {
        parent::__construct($container, $twig, $translator);

        $this->manager = $manager;
    }

    function configure()
    {
    }

    function initialize(ComponentRequest $request, ComponentResponse $response)
    {
        $events = $request->query->get('events');
        $isNotSubmit = (is_array($events) and !in_array('submit', $events));
        $isShowItem = $request->query->get('showFieldItem');
        $showField = $request->query->get('showField');

        if($isShowItem and $showField) {
            $this->fieldKey = $showField['key'] ?? null;
            $this->fieldPath = $showField['path'] ?? null;
            $this->fieldConfig = $this->manager->getFieldConfig($this->fieldKey, $this->fieldPath);

            if($this->fieldConfig and $isNotSubmit)
            {
                /** @var TabComponent $tabs */
                $tabs = $this->addContentItem(TabComponent::class);

                $this->fieldData = $this->manager->getFieldData($this->fieldKey, $this->fieldPath);

                if(!$this->fieldConfig['multiple']) {
                    $this->fieldItemId = array_key_first($this->fieldData);
                } else {
                    $this->fieldItemId = $request->query->get('showFieldItemId');

                    if($this->fieldItemId !== null and !isset($this->fieldData[$this->fieldItemId])) {
                        $this->fieldItemId = null;
                    }
                }

                if($this->fieldItemId === null) {
                    $response->error->send($this->translator->trans('geek_star_storage.alert.field_item_not_found'));
                }

                $item = $this->fieldData[$this->fieldItemId]['data'];

                $isActive = false;
                foreach ($item as $context => $contextValues) {
                    $tabId = 'component-' . $tabs->getKey() . '-' . $context;

                    $tabs->addTab($tabId, $context);
                    $tabItem = $tabs->addContentItem(TabItemComponent::class);
                    $tabItem->setId($tabId);

                    if(!$isActive) {
                        $isActive = true;
                        $tabItem->setActive(true);
                    }

                    $wrapper = $tabItem->addContentItem('
                        <div style="margin-top: 20px">
                            {{ content | raw }}
                        </div>
                    ');

                    foreach ($contextValues as $keyValue => $value) {
                        $this->addTabItemField($wrapper, $this->fieldItemId, $keyValue, $context, $value);
                    }
                }

                $this->addContentItem('
                    <div style="height: 2rem"></div>
                    <div style="
                        display: flex; 
                        justify-content: flex-end;
                        width: 100%;
                        position: absolute;
                        bottom: 0;
                        left: 0;
                        padding: 1.5rem;
                    ">{{ content | raw }}</div>
                ')
                    ->addContentItem(ButtonField::class, 'submit')
                        ->setType(ButtonField::TYPE_SUBMIT)
                        ->setTheme(ButtonField::THEME_PRIMARY)
                        ->setSize(ButtonField::SIZE_SMALL)
                        ->setTitle('Отправить')
                    ->end()
                    ->addContentItem('<div style="width: 10px"></div>')->end()
                    ->addContentItem(ButtonField::class, 'reset')
                        ->setType(ButtonField::TYPE_RESET)
                        ->setTheme(ButtonField::THEME_OUTLINE_SECONDARY)
                        ->setSize(ButtonField::SIZE_SMALL)
                        ->setTitle('Сбросить')
                    ->end()
                ->end();
            }
        }
    }

    private function addTabItemField(ComponentInterface $component, int $idItem, string $keyValue, string $context, array $value)
    {
        $title = $value['title'];
        $fieldId = $idItem . '[' . $context . ']' . '[' . $keyValue . ']';

        $group = $component->addContentItem(GroupComponent::class);

        if($value['type'] != 'checkbox') {
            $group
                ->setTitle($title)
                ->setNote($keyValue);
        }

        switch ($value['type']) {
            case 'text':
                $field = $group->addContentItem(TextField::class, $fieldId);
                if($value['value'] !== null) {
                    $field->setValue($value['value']);
                }
                break;

            case 'textarea':
                $field = $group->addContentItem(TextAreaField::class, $fieldId);
                $field->setHeight('150px');
                if($value['value'] !== null) {
                    $field->setValue($value['value']);
                }
                break;

            case 'richtext':
                $field = $group->addContentItem(RichTextField::class, $fieldId);
                $field->setHeight('500px');
                if($value['value'] !== null) {
                    $field->setValue($value['value']);
                }
                break;

            case 'float':
            case 'number':
                $field = $group->addContentItem(TextField::class, $fieldId);
                $field->setType('number');

                if($value['value'] !== null) {
                    $field
                        ->setValue($value['value']);
                }
                break;

            case 'checkbox':
                $field = $group->addContentItem(CheckboxField::class, $fieldId);
                $field->setTitle($title);

                if($value['value'] !== null) {
                    $field->setChecked($value['value']);
                }
                break;

            case 'date':
                $field = $group->addContentItem(DateField::class, $fieldId);
                $field->setFormat('Y-m-d');
                if($value['value'] !== null) {
                    $field->setValue($value['value']);
                }
                break;

            case 'datetime':
                $field = $group->addContentItem(DateField::class, $fieldId);
                $field->setFormat('Y-m-d H:i');

                if($value['value'] !== null) {
                    $field->setValue($value['value']);
                }
                break;

            case 'image':
                $field = $group->addContentItem(ImageField::class, $fieldId);

                if($value['value'] !== null) {
                    $field->setUrl($value['value']['url'] ?? '');
                    $field->setPreview($value['value']['preview'] ?? '');
                }
                break;
        }
    }

    public function validate(ComponentRequest $request, ComponentResponse $response, array $formData, FormComponentValidator $validator)
    {
        if(!$this->fieldKey or !$this->fieldPath or !$this->fieldConfig) {
            $response->error->send($this->translator->trans('geek_star_storage.alert.field_not_found'));
        }

        $this->field = $this->manager->getField($this->fieldKey, $this->fieldPath);

        if($this->fieldConfig['multiple']) {
            $this->fieldItemId = $request->query->get('showFieldItemId');
            $this->fieldItem = $this->manager->getFieldItem($this->field, $this->fieldItemId);
        } else {
            $this->fieldItem = $this->manager->getFieldItem($this->field);
        }

        if(!$this->fieldItem) {
            $response->error->send($this->translator->trans('geek_star_storage.alert.field_item_not_found'));
        }
    }

    public function save(ComponentRequest $request, ComponentResponse $response, array $formData)
    {
        $values = $this->fieldItem->getStorageFieldValuesJoinFileHolder();
        $data = [];
        foreach ($values as $value) {
            $data[$value->getContext()][$value->getKey()] = $value;
        }

        foreach ($this->manager->contexts as $context) {
            foreach ($this->fieldConfig['items'] as $keyItemValue => $itemValue) {
                $type = $itemValue['type'];
                $dataValue['query'] = $formData['query'][$this->fieldItem->getId()][$context][$keyItemValue] ?? null;
                $dataValue['file'] = $formData['files'][$this->fieldItem->getId()][$context][$keyItemValue] ?? null;

                $value = $data[$context][$keyItemValue] ?? null;
                if(!$value) {
                    $value = $this->manager->getFieldValue($this->fieldItem, $context, $keyItemValue);
                }

                $this->manager->saveFieldValue($value, $type, $dataValue);
            }
        }
    }
}