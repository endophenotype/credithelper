<?php

namespace GeekStar\StorageBundle\Admin\Page\Components;

use GeekStar\AdminBundle\Elements\Components\ButtonComponent;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\StorageBundle\StorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class AddItemButtonComponent extends ButtonComponent
{
    protected StorageManager $manager;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, StorageManager $manager)
    {
        parent::__construct($container, $twig, $translator);

        $this->manager = $manager;
    }

    function configure()
    {
        $this
            ->setTheme(self::THEME_SUCCESS)
            ->setSize(self::SIZE_SMALL)
            ->setTitle($this->translator->trans('geek_star_storage.content.add_field_item'));

        $this->addAction('click', '() => {
            this.events.addItem();
        }');

        $this->addEvent('addItem', '(response) => {
            let component = this;
            let popup = component.root.components["field_item_popup"];
                
            popup.actions.show({
                showField: component.query.showField,
                showFieldItem: true,
                showFieldItemId: response.data.itemId
            });
        }');

        $this->addHandler('addItem', [$this, 'addItemHandler']);
    }

    function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function addItemHandler(ComponentRequest $request, ComponentResponse $response)
    {
        if($showField = $request->query->get('showField')) {
            $key = $showField['key'] ?? '';
            $path = $showField['path'] ?? '';
            $field = $this->manager->getField($key, $path);

            if($field) {
                $item = $this->manager->addFieldItem($field);
                $response->data->set('itemId', $item->getPrimaryKey());
            }
        }
    }
}