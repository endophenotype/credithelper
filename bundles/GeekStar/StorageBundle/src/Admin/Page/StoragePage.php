<?php

namespace GeekStar\StorageBundle\Admin\Page;

use GeekStar\AdminBundle\Elements\Page;
use GeekStar\StorageBundle\Admin\Page\Components\StorageFieldItemPopupComponent;
use GeekStar\StorageBundle\Admin\Page\Components\StorageFieldPopupComponent;
use GeekStar\StorageBundle\Admin\Page\Components\StoragePanelComponent;

class StoragePage extends Page
{

    public function configure()
    {
        $this
            ->setTitle('Storage page')
            ->setName('storage')
            ->setVisible(true)
            ->setPermission('ROLE_ADMIN');
    }

    public function initialize()
    {
        $this
            ->addContentComponent(StoragePanelComponent::class, 'table_panel')->end()
            ->addContentComponent(StorageFieldPopupComponent::class, 'field_popup')->end()
            ->addContentComponent(StorageFieldItemPopupComponent::class, 'field_item_popup')->end();
    }
}