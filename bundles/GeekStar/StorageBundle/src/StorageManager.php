<?php

namespace GeekStar\StorageBundle;

use GeekStar\FileBundle\FileManager;
use GeekStar\FileBundle\PropelModel\File;
use GeekStar\FileBundle\PropelModel\FileHolder;
use GeekStar\StorageBundle\PropelModel\StorageField;
use GeekStar\StorageBundle\PropelModel\StorageFieldItem;
use GeekStar\StorageBundle\PropelModel\StorageFieldItemQuery;
use GeekStar\StorageBundle\PropelModel\StorageFieldQuery;
use GeekStar\StorageBundle\PropelModel\StorageFieldValue;
use GeekStar\StorageBundle\PropelModel\StorageFieldValueQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StorageManager
{
    protected ContainerInterface $container;
    protected FileManager $fileManager;

    public array $config;
    public array $nodes;
    public array $contexts;

    public array $fieldsData = [];
    public array $normalizeStorageData = [];

    public function __construct(ContainerInterface $container, FileManager $fileManager)
    {
        $this->container = $container;
        $this->fileManager = $fileManager;

        $this->config = $container->getParameter('geek_star_storage.config') ?? [];
        $this->nodes = $this->getTreeNodes($this->config);
        $this->contexts = $this->config['contexts'] ?? [];
    }

    private function getTreeNodes(array $config): array
    {
        $rootNode = [
            'path' => '/',
            'title' => 'root',
            'nodes' => []
        ];

        foreach ($config['nodes'] as $node) {
            $path = trim($node['path'], '/');
            $currentNode = &$rootNode;
            if($path) {
                $pathNodes = explode("/", $path);
                foreach ($pathNodes as $pathNode) {
                    $currentNode = &$currentNode['nodes'][$pathNode];
                }
            }

            $currentNode['path'] = $node['path'];
            $currentNode['title'] = $node['title'];
            $currentNode['fields'] = $node['fields'];
        }

        return $rootNode;
    }

    //======================

    public function getTable(): array
    {
        $table = $this->getTableItems($this->nodes['nodes']);

        return $table;
    }

    private function getTableItems(array $nodes): array
    {
        $items = [];

        foreach ($nodes as $nodeKey => $node) {
            $withNodes = (isset($node['nodes']) and $node['nodes']);
            $withFields = (isset($node['fields']) and $node['fields']);

            if($withNodes or $withFields)
            {
                $items[$nodeKey] = [
                    'title' => $node['title'],
                    'path' => $node['path'],
                    'isField' => false,
                ];

                if($withNodes) {
                    $items[$nodeKey]['items'] = $this->getTableItems($node['nodes'] ?? []);
                }

                if($withFields) {
                    foreach ($node['fields'] as $fieldKey => $field) {
                        $items[$nodeKey]['items']['field_' . $fieldKey] = [
                            '_key' => $fieldKey,
                            'title' => $field['title'],
                            'path' => $node['path'],
                            'multiple' => $field['multiple'],
                            'isField' => true,
                        ];
                    }
                }
            }
        }

        return $items;
    }

    //======================

    public function getFieldConfig(string $key = '', string $path= ''): ?array
    {
        $path = trim($path, '/');
        $currentNode = &$this->nodes;
        if($path) {
            $pathNodes = explode("/", $path);
            foreach ($pathNodes as $pathNode) {
                $currentNode = &$currentNode['nodes'][$pathNode];
            }
        }

        return $currentNode['fields'][$key] ?? null;
    }

    public function getField(string $key = '', string $path= '', bool $isCreate = true): StorageField
    {
        $field = StorageFieldQuery::create()
            ->filterByKey($key)
            ->filterByPath($path)
            ->findOne();

        if(!$field and $isCreate) {
            $field = new StorageField();
            $field
                ->setKey($key)
                ->setPath($path)
                ->save();
        }

        return $field;
    }

    public function getFieldData(string $key = '', string $path= '', bool $isUpdate = false): ?array
    {
        if(!$isUpdate and isset($this->fieldsData[$key][$path])) {
            return $this->fieldsData[$key][$path];
        }

        $fieldConfig = $this->getFieldConfig($key, $path);
        $data = null;

        if($fieldConfig) {
            $field = $this->getField($key, $path);
            $items = $this->getFieldItems($field);
            if(!$items->count()) {
                if (!$fieldConfig['multiple']) {
                    $items = [];
                    $item = $this->addFieldItem($field);

                    $items[] = $item;
                } else {
                    $items = [];
                }
            }

            $data = [];

            if($fieldConfig['multiple']) {
                foreach ($items as $item) {
                    $data[$item->getId()]['data'] = $this->getFieldItemData($item, $fieldConfig);
                    $data[$item->getId()]['sortIndex'] = $item->getSortableRank();
                }
            } else {
                $data[$items[0]->getId()]['data'] = $this->getFieldItemData($items[0], $fieldConfig);
            }
        }

        $this->fieldsData[$key][$path] = $data;

        return $data;
    }

    public function addFieldItem(StorageField $field): StorageFieldItem
    {
        $item = new StorageFieldItem();
        $item
            ->setStorageField($field)
            ->save();

        return $item;
    }


    public function getFieldItem(StorageField $field, int $itemId = null): ?StorageFieldItem
    {
        if($itemId !== null) {
            $item = StorageFieldItemQuery::create()
                ->filterByStorageField($field)
                ->findPk($itemId);
        } else {
            $item = StorageFieldItemQuery::create()
                ->filterByStorageField($field)
                ->findOne();
        }

        return $item;
    }

    public function getFieldItems(StorageField $field)
    {
        return $item = StorageFieldItemQuery::create()
            ->filterByStorageField($field)
            ->orderBySortableRank()
            ->find();
    }

    private function getFieldItemData(?StorageFieldItem $item, array $fieldConfig): array
    {
        $values = [];

        if($item) {
            $itemValues = $item->getStorageFieldValues();
            foreach ($itemValues as $itemValue) {
                if($itemValue->getContext()) {
                    $values[$itemValue->getContext()][$itemValue->getKey()] = [
                        'text_value' => $itemValue->getTextValue(),
                        'float_value' => $itemValue->getFloatValue(),
                        'int_value' => $itemValue->getIntValue(),
                        'datetime_value_at' => $itemValue->getDatetimeValueAt(),
                        'file_value' => $itemValue->getFileHolder() ? $itemValue->getFileHolder()->getFile() : null,
                    ];
                }
            }
        }

        $data = [];
        foreach ($this->contexts as $context) {
            foreach ($fieldConfig['items'] as $keyValue => $confValue) {
                $data[$context][$keyValue] = $confValue;

                if(isset($values[$context][$keyValue])) {
                    $data[$context][$keyValue]['value'] = $this->normalizeFieldValueData($confValue['type'], $values[$context][$keyValue]);
                } else {
                    $data[$context][$keyValue]['value'] = null;
                }
            }
        }

        return $data;
    }

    public function getFieldValue(StorageFieldItem $item, string $context, string $key): StorageFieldValue
    {
        $value = StorageFieldValueQuery::create()
            ->filterByStorageFieldItem($item)
            ->filterByContext($context)
            ->filterByKey($key)
            ->findOne();

        if(!$value) {
            $value = new StorageFieldValue();
            $value
                ->setStorageFieldItem($item)
                ->setContext($context)
                ->setKey($key)
                ->save();
        }

        return $value;
    }

    public function saveFieldValue(StorageFieldValue $itemValue, string $type, array $data)
    {
        switch ($type) {
            case 'file':
            case 'image':
                $uploadFile = $data['file']['file'] ?? null;

                if(($data['query']['delete'] ?? null) or $uploadFile) {
                    $holder = $itemValue->getFileHolder();
                    if($holder) {
                        $holder->delete();
                    }
                }

                if($uploadFile) {
                    $file = $this->fileManager->uploadFile($data['file']['file'], null, true);

                    $holder = new FileHolder();
                    $holder
                        ->setFile($file)
                        ->save();

                    $itemValue
                        ->setFileHolder($holder)
                        ->save();
                }
                break;

            case 'text':
            case 'textarea':
            case 'richtext':
                $itemValue->setTextValue($data['query']);
                break;

            case 'date':
            case 'datetime':
                $itemValue->setDatetimeValueAt(new \DateTime($data['query']));
                break;

            case 'checkbox':
                $itemValue->setIntValue($data['query'] ? 1 : 0);
                break;

            case 'number':
                $itemValue->setIntValue($data['query']);
                break;

            case 'float':
                $itemValue->setFloatValue($data['query']);
                break;
        }

        $itemValue->save();
    }

    public function decorateTableValue(string $type, $data): string
    {
        switch ($type) {
            case 'image':
                $preview = $data['preview'] ?? $data['url'] ?? null;
                if($preview) {
                    return '<img src="' . $preview . '" style="max-height: 40px">';
                } else {
                    return '<i class="fa fa-image"></i>';
                }

            case 'text':
            case 'textarea':
            case 'richtext':
            case 'number':
            case 'float':
                return '<span>' . $data . '</span>';

            case 'date':
                if($data !== null) {
                    $data = $data->format('Y-m-d');
                } else {
                    $data = '';
                }
                return '<span>' . $data . '</span>';

            case 'datetime':
                if($data !== null) {
                    $data = $data->format('Y-m-d H:i');
                } else {
                    $data = '';
                }
                return '<span>' . $data . '</span>';

            case 'checkbox':
                if($data) {
                    return '<i class="fa fa-check text-success"></i>';
                } else {
                    return '<i class="fa fa-times text-danger"></i>';
                }

            default:
                return '';
        }
    }

    private function normalizeFieldValueData(string $type, array $data)
    {
        switch ($type) {
            case 'image':
                /** @var File $file */
                $file = $data['file_value'];
                if($file) {
                    $url = $file->getFullPath();
                } else {
                    $url = null;
                }

                return [
                    'url' => $url
                ];

            case 'text':
                return $data['text_value'] ?? null;

            case 'textarea':
            case 'richtext':
                $data = $data['text_value'] ?? null;
                if($data) {
                    $data = htmlspecialchars_decode($data);
                }
                return $data;

            case 'date':
            case 'datetime':
                return $data['datetime_value_at'] ?? null;

            case 'checkbox':
            case 'number':
                return $data['int_value'] ?? null;

            case 'float':
                return $data['float_value'] ?? null;

            default:
                return null;
        }
    }

    public function getStorageData(string $key = '', string $path= '', ?string $context = null, ?string $itemId = null, ?string $value = null)
    {
        $fieldData = $this->getNormalizeStorageData($key, $path);
        $fieldConfig = $this->getFieldConfig($key, $path);

        if($fieldData) {
            if($context !== null) {
                if($fieldConfig['multiple']) {
                    if($itemId === null) {
                        $itemId = array_key_first($fieldData);
                    }

                    $contextData = $fieldData[$itemId][$context] ?? null;
                } else {
                    $contextData = $fieldData[$context] ?? null;
                }

                if($contextData) {
                    if($value !== null) {
                        return $contextData[$value] ?? null;
                    } else {
                        return $contextData;
                    }

                } else {
                    return null;
                }

            } else {
                return $fieldData;
            }
        }

        return null;
    }

    public function getNormalizeStorageData(string $key = '', string $path= '', bool $isUpdate = false): ?array
    {
        if(!$isUpdate and isset($this->normalizeStorageData[$key][$path])) {
            return $this->normalizeStorageData[$key][$path];
        }

        $normalizeData = [];
        $data = $this->getFieldData($key, $path);
        $fieldConfig = $this->getFieldConfig($key, $path);

        if($data) {
            if($fieldConfig['multiple']) {
                foreach ($data as $keyItem => $item) {
                    foreach ($item['data'] as $context => $contextData) {
                        if(isset($item['sortIndex'])) {
                            $normalizeData[$keyItem][$context]['_sortIndex'] = $item['sortIndex'];
                        }

                        foreach ($contextData as $keyValue => $value) {
                            $normalizeData[$keyItem][$context][$keyValue] = $value['value'];
                        }
                    }
                }
            } else {
                $item = $data[array_key_first($data)] ?? null;
                if($item) {
                    foreach ($item['data'] as $context => $contextData) {
                        foreach ($contextData as $keyValue => $value) {
                            $normalizeData[$context][$keyValue] = $value['value'];
                        }
                    }
                }
            }
        }

        $this->normalizeStorageData[$key][$path] = $normalizeData;

        return $normalizeData;
    }

    public function copyItem(StorageFieldItem $item)
    {
        $newItem = new StorageFieldItem();
        $newItem
            ->setStorageFieldId($item->getStorageFieldId())
            ->save();

        $values = $item->getStorageFieldValuesJoinFileHolder();
        foreach ($values as $value) {
            $newValue = new StorageFieldValue();
            $newValue
                ->setStorageFieldItem($newItem)
                ->setContext($value->getContext())
                ->setKey($value->getKey())
                ->setIntValue($value->getIntValue())
                ->setFloatValue($value->getFloatValue())
                ->setTextValue($value->getTextValue())
                ->setDatetimeValueAt($value->getDatetimeValueAt());

            if($holder = $value->getFileHolder()) {
                $newHolder = new FileHolder();
                $newHolder
                    ->setFileId($holder->getFileId())
                    ->save();

                $newValue->setFileHolder($newHolder);
            }

            $newValue->save();
        }
    }
}