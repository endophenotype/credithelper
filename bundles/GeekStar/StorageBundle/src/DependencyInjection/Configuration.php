<?php

namespace GeekStar\StorageBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('geek_star_storage');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('contexts')
                    ->scalarPrototype()->end()
                ->end()
                ->arrayNode('nodes')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('path')
                                ->isRequired()
                            ->end()
                            ->scalarNode('title')
                                ->defaultValue('')
                            ->end()
                            ->arrayNode('fields')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('title')
                                            ->defaultValue('')
                                        ->end()
                                        ->scalarNode('multiple')
                                            ->defaultValue(false)
                                        ->end()
                                        ->scalarNode('table_context')
                                            ->defaultValue('')
                                        ->end()
                                        ->arrayNode('table_columns')
                                            ->scalarPrototype()->end()
                                        ->end()
                                        ->arrayNode('items')
                                            ->arrayPrototype()
                                                ->children()
                                                    ->scalarNode('title')
                                                        ->defaultValue('')
                                                    ->end()
                                                    ->scalarNode('type')
                                                        ->defaultValue('text')
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
