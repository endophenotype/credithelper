<?php

namespace GeekStar\StorageBundle\PropelModel;

use GeekStar\StorageBundle\PropelModel\Base\StorageFieldItem as BaseStorageFieldItem;

/**
 * Skeleton subclass for representing a row from the 'storage_field_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class StorageFieldItem extends BaseStorageFieldItem
{

}
