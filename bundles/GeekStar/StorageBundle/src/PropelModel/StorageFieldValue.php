<?php

namespace GeekStar\StorageBundle\PropelModel;

use GeekStar\StorageBundle\PropelModel\Base\StorageFieldValue as BaseStorageFieldValue;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'storage_field_value' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class StorageFieldValue extends BaseStorageFieldValue
{
    public function preDelete(ConnectionInterface $con = null)
    {
        if($fileHolder = $this->getFileHolder()) {
            $fileHolder->delete($con);
        }

        return parent::preDelete($con);
    }
}
