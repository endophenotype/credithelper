<?php

namespace GeekStar\StorageBundle\PropelModel;

use GeekStar\StorageBundle\PropelModel\Base\StorageFieldValueQuery as BaseStorageFieldValueQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'storage_field_value' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class StorageFieldValueQuery extends BaseStorageFieldValueQuery
{

}
