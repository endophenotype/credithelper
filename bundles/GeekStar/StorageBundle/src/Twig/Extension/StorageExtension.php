<?php

namespace GeekStar\StorageBundle\Twig\Extension;

use GeekStar\StorageBundle\StorageManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StorageExtension extends AbstractExtension
{
    protected StorageManager $manager;

    public function __construct(StorageManager $manager)
    {
        $this->manager = $manager;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('storage', [$this->manager, 'getStorageData']),
            new TwigFunction('json_decode', [$this, 'jsonDecode']),
        ];
    }

    public function jsonDecode($data)
    {
        return json_decode($data);
    }
}