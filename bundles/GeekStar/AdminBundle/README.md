yarn install
yarn build

webpack_encore.yaml add build:
Resources/config/packages/webpack_encore.yaml to webpack_encore.yaml

routes.yaml
admin:
    resource: "@GeekStarAdminBundle/Controller/"
    type: annotation
    prefix: /admin

packages/geek_star_admin.yaml
geek_star_admin:
    security:
        user_manager: '@App\Business\Security\Model\UserManager'
        firewall: main

UserManager implements GeekStar\AdminBundle\Service\UserManagerInterface

User implements GeekStar\AdminBundle\Service\UserInterface

UserInterfaceNormalizer
    if(in_array('geek_star_admin', $context))
    {
        $data = [
            'avatarInAdmin' => $object->getAvatarInAdmin(),
            'nameInAdmin' => $object->getNameInAdmin(),
        ];
    }