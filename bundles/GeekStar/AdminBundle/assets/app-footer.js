import ApiHandler from "./js/api/ApiHandler";

import './js/navigation/sidebar';
import './js/component/component';
import './js/modal/modal';

let apiHandler = new ApiHandler(baseUrl, apiCsrfToken);

// ===== Custom Scrollbar =======
$('.mCustomScrollbar').mCustomScrollbar({
    axis: "y",
    scrollInertia: 150
});

// ===== TinyMCE modal =======
document.addEventListener('focusin', function (e) { if (e.target.closest('.tox-tinymce-aux, .moxman-window, .tam-assetmanager-root') !== null) { e.stopImmediatePropagation(); } });

// ===== logout button =======
$('.js-gsa-logout').on("click", function (e) {
    e.preventDefault();
    apiHandler.logout();
    window.location.href = baseUrl + 'login/';
});