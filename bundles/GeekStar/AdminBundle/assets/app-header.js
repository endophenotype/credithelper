import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './js/custom-libs/mCSB/mCSB.css';
import 'apexcharts/dist/apexcharts.css';
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css';
import 'jquery-ui/themes/base/slider.css'
import 'jquery-ui/themes/base/sortable.css'
import './js/custom-libs/gsFilesUploader/gsFilesUploader.css';

import './styles/app/app.sass';

import './js/common/common'; //jQuery, bootstrap

import 'jquery-mousewheel';
import './js/custom-libs/mCSB/mCSB';
import './js/common/tinymce/tinymce';
import 'eonasdan-bootstrap-datetimepicker';
import './js/common/datetimepicker/datetimepicker';
import 'jquery-ui/ui/widgets/slider';
import 'jquery-ui/ui/widgets/sortable';
import './js/custom-libs/gsFilesUploader/gsFilesUploader';

import ApexCharts from 'apexcharts';
window.ApexCharts = ApexCharts;