export default class ApiHandler {
    constructor(baseUrl, apiCsrfToken = null) {
        this.apiCsrfToken = apiCsrfToken
        this.baseUrl = baseUrl
        this.apiURl = this.baseUrl + 'api/'
    }

    getUrlVars(){
        let vars = {}, hash;

        if(window.location.href.indexOf('?') !== -1) {
            let hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

            for(let i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars[hash[0]] = hash[1];
            }
        }

        return vars;
    };

    getFormData(formData, data, previousKey) {
        if (data instanceof Object) {
            Object.keys(data).forEach(key => {
                const value = data[key];
                if (value instanceof Object && !Array.isArray(value)) {
                    return this.getFormData(formData, value, key);
                }
                if (previousKey) {
                    key = `${previousKey}[${key}]`;
                }
                if (Array.isArray(value)) {
                    value.forEach(val => {
                        formData.append(`${key}[]`, val);
                    });
                } else {
                    formData.append(key, value);
                }
            });
        }
    }

    send(url, method, data, callback) {
        let options = {
            type: method,
            url: url,
            data: data,
            success: function(msg){
                callback(msg)
            },
            error: function (jqXHR) {
                callback(JSON.parse(jqXHR.responseText))
            }
        };

        if(typeof(data) ==="undefined" || data === null) {
            data = {};
        }

        if(data instanceof FormData) {
            options.processData = false;
            options.contentType = false;
        }

        if(typeof(callback) !== "function") {
            callback = (msg) => {};
        }

        if(this.apiCsrfToken) {
            if(data instanceof FormData) {
                data.set('csrf_token', this.apiCsrfToken);
            } else {
                data.csrf_token = this.apiCsrfToken
            }
        }

        $.ajax(options);
    }

    /* =======================================
                    API methods
    ======================================= */

    login(username, password, remember_me, callback = null) {
        let data = {
            username: username,
            password: password,
            remember_me: remember_me
        }

        this.send(this.apiURl + 'login', 'post', data, callback);
    }

    logout(callback = null) {
        this.send(this.apiURl + 'logout', 'post', null, callback);
    }

    page(query, category, module, page, callback = null) {
        let data = $.extend(query, {
            category: category,
            module: module,
            page: page
        });

        this.send(this.apiURl + 'page', 'post', data, callback);
    }

    component(query, category, module, page, component, callback = null, formData = null) {
        let data = $.extend(query, {
            category: category,
            module: module,
            page: page,
            component: component
        });

        if(formData instanceof FormData) {
            this.getFormData(formData, data, '');
            data = formData;
        }

        this.send(this.apiURl + 'component', 'post', data, callback);
    }

    field(query, category, module, page, component, field, callback = null, formData = null) {
        let data = $.extend(query, {
            category: category,
            module: module,
            page: page,
            component: component,
            field: field,
        });

        if(formData instanceof FormData) {
            this.getFormData(formData, data, '');
            data = formData;
        }

        this.send(this.apiURl + 'field', 'post', data, callback);
    }
}