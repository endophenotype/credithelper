import $ from 'jquery'
import ApiHandler from "./ApiHandler";

export default class ApiFormHandler {
    constructor(apiHandler = new ApiHandler(baseUrl, apiCsrfToken)) {
        this.apiHandler = apiHandler;

        this.config = {
            formSelector: '.js-gsaf-form',
            fieldSelector: '.js-gsaf-field',
            fieldErrorSelector: '.js-gsaf-field-error',
            inputSelector: '.js-gsaf-input',
            submitSelector: '.js-gsaf-submit',
            errorSelector: '.js-gsaf-error',
        }
    }

    init() {
        let apiFormHandler = this;

        $(document).ready(function() {
            let formObjects = $(apiFormHandler.config.formSelector);
            let forms = [];

            formObjects.each(function( formIndex ) {
                forms[formIndex] = apiFormHandler.initForm($(this), apiFormHandler.config);
            });

            return forms;
        });
    }

    initForm(form, config, onSuccessSubmit, onErrorSubmit) {
        let apiFormHandler = this;

        if(typeof(config) ==="undefined" || config === null) {
            config = this.config;
        }

        if(typeof(onSuccessSubmit) !== "function") {
            onSuccessSubmit = (response) => {};
        }

        if(typeof(onErrorSubmit) !== "function") {
            onErrorSubmit = (response) => {};
        }

        let formObject = {
            form: form,
            url: form.attr('action'),
            method: form.attr('method'),
            submit: form.find(config.submitSelector),
            error: form.find(config.errorSelector),
            fields: {}
        };

        apiFormHandler.initFormFields(formObject, config);

        formObject.submit.on("click", function(e){
            e.preventDefault();
            apiFormHandler.formSubmit(formObject, onSuccessSubmit, onErrorSubmit)
        });

        return formObject;
    }

    initFormFields(formObject, config) {
        let formFields = formObject.form.find(config.fieldSelector);

        formFields.each(function( fieldIndex ) {
            let field = $(this);
            let input = field.find(config.inputSelector);

            formObject.fields[input.attr("name")] = {
                input: input,
                error: field.find(config.fieldErrorSelector)
            }
        });
    }

    formSubmit(formObject, onSuccess, onError) {
        let apiFormHandler = this;
        let data = this.formSerialize(formObject.form);

        this.apiHandler.send(formObject.url, formObject.method, data, (response) =>  {
            apiFormHandler.clearFormError(formObject);

            if(typeof response.error !== 'undefined') {
                if(response.error.message !== '') {
                    formObject.error.html(response.error.message);
                }

                let fieldErrors = $.extend(
                    {},
                    response.error.request,
                    response.error.query,
                    response.error.file,
                );

                $.each(fieldErrors, function( key, value ) {
                    if(typeof formObject.fields[key] !== 'undefined') {
                        formObject.fields[key].error.html(value);
                    }
                });
                onError(response);
            } else {
                onSuccess(response);
            }
        })
    }

    formSerialize(form) {
        let formData = form.serializeArray();
        let data = {};

        $(formData).each(function(index, obj){
            data[obj.name] = obj.value;
        });

        return data;
    }

    clearFormError(formObject) {
        formObject.error.html('');
        $.each(formObject.fields, function( key, value ) {
            value.error.html('');
        });
    }
}