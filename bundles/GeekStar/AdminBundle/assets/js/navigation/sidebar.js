let $mainMenu = $('header > .main_menu');
let $sidebar = $('.sidebar');

$('.js-toggle-sidebar-menu').on("click", function(e){
    $mainMenu.toggleClass('active');
    if($mainMenu.hasClass('active')) {
        $sidebar.addClass('active');
        $sidebar.removeClass('pre-not-active');
    } else {
        $sidebar.addClass('pre-not-active');
        setTimeout(function(){
            $sidebar.removeClass('active');
        }, 10);
    }
});

$('.js-toggle-sidebar-menu-dropdown').on("click", function(e){
    let $button = $(this);
    let $buttonIcon = $('.fa', $button);
    let $list = $(' + ul', $button);
    let speed = 600;

    $button.toggleClass('show');
    if($button.hasClass('show')) {
        autoHeightAnimate($list, speed);
        $buttonIcon.addClass('fa-angle-up');
        $buttonIcon.removeClass('fa-angle-down');
    } else {
        $list.stop().animate({ height: '0' }, speed);
        $buttonIcon.removeClass('fa-angle-up');
        $buttonIcon.addClass('fa-angle-down');
    }

    function autoHeightAnimate(element, time){
        let autoHeight = element.css('height', 'auto').height();
        element.height(0);
        element.stop().animate({ height: autoHeight }, time);
    }
});

$('.js-sidebar-menu-item-parent').on('mouseover', function() {
    let $menuItem = $(this);
    let $submenuWrapper = $('> ul', $menuItem);
    let $scrollContainer = $('.mCSB_container', $sidebar);
    let menuItemPos = $menuItem.position();

    let topPos =  menuItemPos.top;
    let maxTopPos = $(window).height() - $sidebar.position().top - $scrollContainer.position().top - $submenuWrapper.outerHeight() - 10;

    if(maxTopPos < topPos) {
        topPos = maxTopPos;
    }

    $submenuWrapper.css({
        top: topPos
    });
});