import ApiHandler from "../api/ApiHandler";

let apiHandler = new ApiHandler(baseUrl, apiCsrfToken);
let root = $('.js-component-root')[0];

$( document ).ready(function() {
    initPage();
});

function initPage() {
    let $root = $(root);
    root.data = {
        key: 'root',
        category: $root.data('category'),
        module: $root.data('module'),
        page: $root.data('page')
    };

    root.initComponent = initComponent;
    root.renderComponent = renderComponent;
    root.initField = initField;
    root.renderField = renderField;

    apiHandler.page(
        apiHandler.getUrlVars(),
        root.data.category,
        root.data.module,
        root.data.page,
        (response) => {
            if(response.error !== undefined && response.error.message !== '') {
                showError(response.error.message);
            }

            $root.html(response.content);
            initComponents(root, response);
        }
    );
}

function initComponents(node, componentResponses) {
    node.components = {};

    $(node).find('.js-component').each(function() {
        let $component = $(this);
        let componentKey = $component.data('key');
        let newComponent = $(componentResponses.data.components[componentKey].content).insertAfter($component)[0];
        $component.remove();

        initComponent(newComponent, componentResponses.data.components[componentKey], root, node);
    });

    if(node.data.key === 'root') {
        renderComponents(node);
    }
}

function initComponent(component, componentResponse, root, parent = null) {
    let componentKey = $(component).data('key');

    let customActions = componentResponse.actions;
    let customEvents = componentResponse.events;

    if(parent === null) {
        parent = root;
    }

    parent.components[componentKey] = component;

    component.root = root;

    /* ======== config ========= */
    component.config = componentResponse.config;

    /* ======== data ========= */
    component.data = $.extend({}, componentResponse.data);
    component.data.key = componentKey;
    component.data.parent = componentResponse.config._parent;
    component.data.fields = componentResponse.fields;

    /* ======== query ========= */
    component.query = $.extend({}, componentResponse.query);

    /* ======== events ========= */
    component.events = {};

    if(customEvents !== undefined && customEvents.length !== 0) {
        for (let key in customEvents) {
            component.events[key] = (data) => {
                let query = $.extend({}, componentResponse.query, {events: [key]})
                let formData = null;

                if(data instanceof FormData) {
                    formData = data;
                } else {
                    query = $.extend(query, data)
                }

                apiHandler.component(
                    query,
                    root.data.category,
                    root.data.module,
                    root.data.page,
                    componentKey,
                    (response) => {
                        let callback = (function(response) {
                            eval(customEvents[key])(response);
                         }).bind(component);
                        let handle = handleEventResponse.bind(component);
                        handle(response, callback);
                    },
                    formData
                );
            }
        }
    }

    /* ======== actions ========= */
    component.actions = {
        'default': () => {},
        'render': () => {},
        'destroy': () => {},
        'onDestroy': () => {},
        'load': () => {}
    };

    if(customActions !== undefined && customActions.length !== 0) {
        for (let key in customActions) {
            setAction.bind(component)(key, customActions[key]);
        }
    }

    /* ======== components ========= */
    initComponents(component, componentResponse);
}

function initField(field, fieldResponse, component) {
    let fieldKey = $(field).data('key');

    component.fields[fieldKey] = field;

    //data
    field.data = $.extend({}, fieldResponse.data);
    field.data.key = fieldKey;

    // config
    field.config = fieldResponse.config;

    //actions
    field.form = component;
    field.actions = {
        'default': () => {},
        'render': () => {},
        'showError': () => {},
        'destroy': () => {},
        'onDestroy': () => {}
    };
    for (let actionKey in fieldResponse.actions) {
        setAction.bind(field)(actionKey, fieldResponse.actions[actionKey]);
    }

    //events
    field.events = {};
    for (let eventKey in fieldResponse.events) {
        field.events[eventKey] = (data) => {
            let query = $.extend({}, fieldResponse.query, {events: [eventKey]})
            let formData = null;

            if(data instanceof FormData) {
                formData = data;
            } else {
                query = $.extend(query, data)
            }

            apiHandler.field(
                query,
                root.data.category,
                root.data.module,
                root.data.page,
                component.data.key,
                fieldKey,
                (response) => {
                    let callback = (function(response) {
                        eval(fieldResponse.events[eventKey])(response);
                    }).bind(field);
                    let handle = handleFieldEventResponse.bind(field);
                    handle(response, callback);
                },
                formData
            );
        }
    }
}

function renderComponents(node) {
    $.each(node.components, function() {
        renderComponent(this);
    })
}

function renderComponent(component) {
    component.actions.default();
    component.actions.render();

    let $mCSB = $(component).find('.scrollContent');
    if($mCSB) {
        $mCSB.mCustomScrollbar({
            axis: "xy",
            scrollInertia: 150,
        });
    }

    if(component.fields !== undefined) {
        $.each(component.fields, function() {
            renderField(this);
        });
    }

    renderComponents(component);
}

function renderField(field) {
    field.actions.default();
    field.actions.render();

    let $mCSB = $(field).find('.scrollContent');
    if($mCSB) {
        $mCSB.mCustomScrollbar({
            axis: "xy",
            scrollInertia: 150,
        });
    }
}

function handleEventResponse(response, callback) {
    let component = this;

    if(response.error !== undefined ) {
        if(response.error.query !== undefined || response.error.files !== undefined) {
            let fieldErrors = $.extend(response.error.query, response.error.files, {})
            $.each(fieldErrors, function (key, value) {
                if(component.fields !== undefined && component.fields[key] !== undefined) {
                    component.fields[key].actions.showError(value);
                } else {
                    console.log("Error: ", key, ": ", value);
                }
            });
        }

        if(response.error.message !== '') {
            showError(response.error.message);
        }
    }

    callback(response);
}

function handleFieldEventResponse(response, callback) {
    if(response.error !== undefined ) {
        if(response.error.message !== '') {
            showError(response.error.message);
        }
    }

    callback(response);
}

function showError(message) {
    errorModal.setMessage(message);
    errorModal.modal.show();
}

function setAction(key, action) {
    this.actions[key] = eval(action);
}