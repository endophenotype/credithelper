let errorModal = $('#errorModal')[0];
let errorModalMessage = $(errorModal).find('.modal-body .message');

errorModal.setMessage = function(message) {
    errorModalMessage.html(message);
}

errorModal.modal = new bootstrap.Modal(errorModal)