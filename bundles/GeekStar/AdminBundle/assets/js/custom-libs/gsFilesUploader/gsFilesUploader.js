(function ($) {
    let fields = [];

    let methods = {
        init: init,
        addItem: addItem,
        successLoad: successLoad,
        submit: submit,
        reset: reset
    };

    function init(options) {
        let uploader = this;
        let $uploader = $(this);
        let $fileList, $uploadArea, $input;
        let key;

        if(fields.length === 0) {
            let $html = $("html");
            $html.on("dragover", function(e) { e.preventDefault(); e.stopPropagation(); });
            $html.on("drop", function(e) { e.preventDefault(); e.stopPropagation(); });
        }

        if(uploader.isInit) {
            key = uploader.config.key;
        } else {
            key = fields.push(uploader);
        }

        options = $.extend(true, {}, {
            'key' : key,
            'singleLoad' : true,
            'sortable' : true,
            'withOutUpload' : false,
            'withOutControls' : false,
            'imgExtensions': ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'ico', 'tif', 'svg'],
            'translate': {
                'add_files': 'Add files',
                'drag_and_drop_files_to_upload': 'Drag and drop files to upload',
                'delete_file': 'Delete a file?'
            },
            'onUpload': function (item) {},
            'onSubmit': function () {}
        }, options);

        uploader.isInit = true;
        uploader.config = options;
        uploader.items = [];

        $uploader.html(getContent.apply(this));
        uploader.fileList = $uploader.find('.gsFilesUploader-list')[0];

        if(!uploader.config.withOutUpload) {
            uploader.uploadArea = $uploader.find('.gsFilesUploader-file-uploader')[0];
            $uploadArea = $(uploader.uploadArea);
            uploader.input = $uploader.find('input[type="file"]')[0];
            $input = $(uploader.input);

            $uploadArea.on("dragover", function (e) {
                $uploadArea.addClass("focus");
            });

            $uploadArea.on("dragleave", function (e) {
                $uploadArea.removeClass("focus");
            });

            $uploadArea.on("drop", function (e) {
                $uploadArea.removeClass("focus");
                let files = e.originalEvent.dataTransfer.files;
                addFiles.call(uploader, files);
            });

            $input.on("change", function (e) {
                addFiles.call(uploader, this.files);
            });
        }

        if(uploader.config.sortable) {
            $fileList = $(uploader.fileList);

            $fileList.sortable({
                axis: "y",
                cursor: "move",
                start: function(e, ui){
                    ui.placeholder.height(ui.item.height());
                },
                stop: function(e, ui){
                    updateSortIndex.call(uploader);
                }
            });
        }
    }

    function addItem(item, index) {
        let uploader = this;
        let $fileList = $(uploader.fileList);
        let itemContent, $itemContent;
        let $link, $input;
        let $controls, $ctrlDelete, $ctrlEdit, $ctrlEditOk, $ctrlEditCancel, $ctrlUpload;

        if(!item.preview && uploader.config.imgExtensions.indexOf(item.extension) !== -1) {
            if(item.url) {
                item.preview = item.url;
            } else if(item.file) {
                item.preview = window.URL.createObjectURL(item.file)
            }
        }

        $itemContent = $(getItemContent.call(uploader, item));
        itemContent = $itemContent[0];
        itemContent.data = item;
        if(index) {
            let $oldItem = $(uploader.items[index]);
            $oldItem.before($itemContent);
            $oldItem.remove();
            uploader.items[index] = itemContent;
        } else {
            $fileList.append($itemContent);
            itemContent.data.index = uploader.items.push(itemContent) - 1;
        }

        updateSortIndex.call(uploader);

        $link = $itemContent.find('a');
        $link.on("dragstart", function(e) { e.preventDefault(); });
        if(!item.url) {
            $link.on("click", function(e) { e.preventDefault(); });
        }

        if(!uploader.config.withOutControls) {
            $input = $link.find('input');
            $controls = $itemContent.find('.controls');
            $ctrlEdit = $controls.find('.edit');
            $ctrlEditOk = $controls.find('.edit-ok');
            $ctrlEditCancel = $controls.find('.edit-cancel');
            $ctrlUpload = $controls.find('.upload');
            $ctrlDelete = $controls.find('.delete');

            $input.on("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
            });

            $ctrlEdit.on("click", function () {
                $itemContent.addClass('edit');
            });

            $ctrlEditOk.on("click", function () {
                endEdit(itemContent, true);
                $itemContent.removeClass('edit');
            });

            $ctrlEditCancel.on("click", function () {
                endEdit(itemContent, false);
                $itemContent.removeClass('edit');
            });

            $ctrlDelete.on("click", function () {
                if (confirm(uploader.config.translate.delete_file)) {
                    deleteItem.call(uploader, itemContent);
                }
            });

            $ctrlUpload.on("click", function () {
                onUpload.call(uploader, itemContent);
            });
        }

        return itemContent;
    }

    function successLoad(loadItem, index) {
        addItem.call(this, loadItem, index);
    }

    function submit() {
        this.config.onSubmit();
    }

    function reset() {
        let uploader = this;
        $.each(uploader.items, function(key, item) {
            if(item !== undefined && item.data.file !== undefined) {
                deleteItem.call(uploader, item);
            }
        });
        updateSortIndex.call(uploader);
    }

    //=======================

    function deleteItem(item) {
        this.items[item.data.index] = undefined;
        $(item).remove();
        updateSortIndex.call(this);
    }

    function addFiles(files) {
        let uploader = this;
        $.each(files, function(key, file) {
            let item = {
                'fileName': file.name,
                'title': getFileTitle(file.name),
                'size': file.size,
                'extension': getFileExtension(file.name),
                'file': file,
            }

            addItem.call(uploader, item);
        });
    }

    function getFileTitle(fileName) {
        let pos = fileName.lastIndexOf('.');
        return pos !== -1 ? fileName.substring(0, pos) : fileName;
    }

    function getFileExtension(fileName) {
        let pos = fileName.lastIndexOf('.');
        return pos !== -1 ? fileName.substring(pos + 1) : '';
    }

    function endEdit(item, isSave) {
        let $item = $(item);
        let $link = $item.find('a');
        let $input = $link.find('input');
        let $title = $link.find('.title');

        if(isSave) {
            item.data.title = $input.val();
            $title.html($input.val());
        } else {
            $input.val(item.data.title);
        }
    }

    function updateSortIndex() {
        if(this.config.sortable) {
            $(this.fileList).find('.gsFilesUploader-list-item').each(function( index ) {
                this.data.sortIndex = index;
            });
        }
    }

    //===================================

    function onUpload(item) {
        $(item).addClass('upload');
        this.config.onUpload(item.data);
    }

    //===================================

    function getContent() {
        let content = '' +
            '    <ul class="gsFilesUploader-list">\n' +
            '    </ul>\n';

        if(!this.config.withOutUpload) {
            content +=
                '    <input class="form-control" type="file" id="gsFilesUploader' + this.config.key + '" multiple>\n' +
                '    <label class="gsFilesUploader-file-uploader" for="gsFilesUploader' + this.config.key + '">\n' +
                '        <div class="text">' + this.config.translate.drag_and_drop_files_to_upload + '</div>\n' +
                '        <div class="btn btn-primary btn-sm">' + this.config.translate.add_files + '</div>\n' +
                '    </label>';
        }

        return content;
    }

    function getItemContent(item) {
        let uploader = this;
        let url = item.url ? item.url : '#';
        let size = convertSize(item.size);
        let title = item.title ? item.title : item.fileName;

        let content =
            '        <li class="gsFilesUploader-list-item">\n' +
            '            <a class="title" href="' + url + '" target="_blank" download="' + item.fileName + '">\n' +
            '                <div class="logo">\n' + getItemLogoContent.call(uploader, item) +
            '                </div>\n' +
            '                <div class="text">\n' +
            '                    <div class="title">' + title + '</div>\n' +
            '                    <input type="text" value="' + title + '">\n' +
            '                </div>\n' +
            '            </a>\n' +
            '            <div class="extension">' + item.extension + '</div>\n' +
            '            <div class="size">' + size + '</div>\n'

        if(!this.config.withOutControls) {
        content +=
            '            <div class="controls">\n' + getItemControlsContent.call(uploader, item)
        }

        content +=
            '            </div>\n' +
            '            <div class="preloader"></div>' +
            '        </li>';

        return content;
    }

    function getItemControlsContent(item) {
        let content = '';

        content += '<button type="button" class="edit btn"><i class="fa fa-pencil"></i></button>\n';
        content += '<button type="button" class="edit-ok btn"><i class="fa fa-check"></i></button>\n';
        content += '<button type="button" class="edit-cancel btn"><i class="fa fa-times"></i></button>\n';

        if(item.url) {
            content += '<div class="success btn"><i class="fa fa-check"></i></div>\n';
        } else if (this.config.singleLoad) {
            content += '<button type="button" class="upload btn"><i class="fa fa-upload"></i></button>\n';
        }

        content += '<button type="button" class="delete btn"><i class="fa fa-trash"></i></button>\n';

        return content;
    }

    function getItemLogoContent(item) {
        let logo = '';
        if(item.preview) {
            logo += '<img src="' + item.preview + '">\n';
        } else {
            logo += '<i class="fa fa-file-o"></i><div class="extension">' + item.extension + '</div>'
        }

        return logo;
    }

    //===================================

    function convertSize(size) {
        let i = Math.floor( Math.log(size) / Math.log(1024) );
        return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + " " + ["B", "kB", "MB", "GB", "TB"][i];
    }

    //===================================

    $.fn.gsFilesUploader = function (options) {
        if (!!options && !methods[options] && typeof options !== 'object' ) {
            $.error('Method ' + options + ' not found');
        }

        let params = arguments;
        return this.each(function() {
            if (typeof options === 'object' || !options) {
                init.apply(this, params);
            } else {
                methods[options].apply(this, Array.prototype.slice.call(params, 1));
            }
        });
    }
})(jQuery);