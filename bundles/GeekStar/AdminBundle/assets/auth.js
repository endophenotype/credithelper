import './styles/auth/auth.sass';
import 'font-awesome/css/font-awesome.min.css';
import './js/common/common'

import ApiHandler from "./js/api/ApiHandler";
import ApiFormHandler from "./js/api/ApiFormHandler";

let apiHandler = new ApiHandler(baseUrl, apiCsrfToken);
let apiFormHandler = new ApiFormHandler(apiHandler);

// ===== authorize forms =======
let authorizeForm = $('#authorize-form');
if(authorizeForm) {
    apiFormHandler.initForm(authorizeForm, null, function(response) {
        window.location.href = baseUrl;
    })
}

// ===== logout button =======
$('.js-gsa-logout').on("click", function(e){
    e.preventDefault();
    apiHandler.logout();
});