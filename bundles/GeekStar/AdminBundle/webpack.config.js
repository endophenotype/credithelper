let Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

let outputPath = './src/Resources/public/build/'
let publicPath = '/bundles/geekstaradmin/build/'

if(Encore.isDev()) {
    outputPath = '../../../public' + publicPath
}

Encore
    .setOutputPath(outputPath)
    .setManifestKeyPrefix('build') //для ручного задания PublicPath
    .setPublicPath(publicPath)

    .addEntry('app-header', './assets/app-header.js')
    .addEntry('app-footer', './assets/app-footer.js')
    .addEntry('auth', './assets/auth.js')

    .enableStimulusBridge('./assets/controllers.json')

    .splitEntryChunks()

    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .configureBabel((config) => {
        config.plugins.push('@babel/plugin-proposal-class-properties');
    })

    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    .enableSassLoader()

    .addPlugin(new CopyWebpackPlugin({
        patterns: [
            { from: './assets/img', to: 'img' },
            { from: './node_modules/tinymce/plugins', to: 'plugins' },
            { from: './node_modules/tinymce/themes', to: 'themes' },
            { from: './node_modules/tinymce/skins', to: 'skins' },
            { from: './node_modules/tinymce/icons', to: 'icons' }
        ]
    }));

module.exports = Encore.getWebpackConfig();