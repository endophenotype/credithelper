<?php

namespace GeekStar\AdminBundle\Normalizer;

use GeekStar\AdminBundle\Elements\User\UserInterface;
use GeekStar\AdminBundle\Util\Normalizer\AbstractNormalizer;

class UserNormalizer extends AbstractNormalizer
{
    /**
     * @param UserInterface $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|\Countable|float|int|mixed|string|\Traversable|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = [
            'avatarInAdmin' => $object->getAvatarInAdmin(),
            'nameInAdmin' => $object->getNameInAdmin(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof UserInterface;
    }
}