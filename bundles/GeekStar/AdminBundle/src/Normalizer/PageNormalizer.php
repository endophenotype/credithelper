<?php

namespace GeekStar\AdminBundle\Normalizer;

use GeekStar\AdminBundle\Elements\PageInterface;
use GeekStar\AdminBundle\Util\Normalizer\AbstractNormalizer;

class PageNormalizer extends AbstractNormalizer
{
    /**
     * @param PageInterface $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|\Countable|float|int|mixed|string|\Traversable|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = [
            'title' => $object->getTitle(),
            'permission'=> $object->getPermission(),
            'uri'=> $object->getUri(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof PageInterface;
    }
}