<?php

namespace GeekStar\AdminBundle\Normalizer;

use GeekStar\AdminBundle\Model\FileListItem;
use GeekStar\AdminBundle\Util\Normalizer\AbstractNormalizer;

class FileListItemNormalizer extends AbstractNormalizer
{
    /**
     * @param FileListItem $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|void|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = [
            'key' => $object->getKey(),
            'title' => $object->getTitle(),
            'fileName' => $object->getFileName(),
            'extension' => $object->getExtension(),
            'size' => $object->getSize(),
            'url' => $object->getUrl(),
            'preview' => $object->getPreview(),
            'sortIndex' => $object->getSortIndex(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof FileListItem;
    }
}