<?php

namespace GeekStar\AdminBundle\Normalizer;

use GeekStar\AdminBundle\Elements\CategoryInterface;
use GeekStar\AdminBundle\Util\Normalizer\AbstractNormalizer;

class CategoryNormalizer extends AbstractNormalizer
{
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = [
            'title' => $object->getTitle(),
            'permission'=> $object->getPermission(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof CategoryInterface;
    }
}