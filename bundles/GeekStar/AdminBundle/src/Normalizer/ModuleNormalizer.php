<?php

namespace GeekStar\AdminBundle\Normalizer;

use GeekStar\AdminBundle\Elements\ModuleInterface;
use GeekStar\AdminBundle\Util\Normalizer\AbstractNormalizer;

class ModuleNormalizer extends AbstractNormalizer
{
    /**
     * @param ModuleInterface $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|\Countable|float|int|mixed|string|\Traversable|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = [
            'title' => $object->getTitle(),
            'name' => $object->getName(),
            'icon' => $object->getIcon(),
            'uri' => $object->getUri(),
            'permission' => $object->getPermission(),
            'defaultPage' => $object->getDefaultPage(),
            'pages' => $object->getPages(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof ModuleInterface;
    }
}