<?php

namespace GeekStar\AdminBundle\Validator;

use GeekStar\AdminBundle\Exception\ManagerException;
use GeekStar\AdminBundle\Elements\User\UserManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ManagerValidator
{
    protected TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function configValidate(ContainerInterface $container, $config) {
        if(!isset($config['company_name'])) {
            throw new ManagerException($this->translator->trans('geek_star_admin.alert.config_not_set_parameter', [
                '{parameter}' => 'geek_star_admin.company_name'
            ]));
        }

        if(isset($config['security']['user_manager'])) {
            $userManager = $container->get($config['security']['user_manager']);

            if(!($userManager instanceof UserManagerInterface)) {
                throw new ManagerException($this->translator->trans('geek_star_admin.alert.сlass_not_implement_interface', [
                    '{class}' => 'user_manager',
                    '{interface}' => 'GeekStar\AdminBundle\Elements\User\UserManagerInterface',
                ]));
            }
        } else {
            throw new ManagerException($this->translator->trans('geek_star_admin.alert.config_not_set_parameter', [
                '{parameter}' => 'geek_star_admin.security.user_manager'
            ]));
        }
    }
}