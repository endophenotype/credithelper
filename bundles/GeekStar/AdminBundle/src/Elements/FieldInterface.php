<?php

namespace GeekStar\AdminBundle\Elements;

interface FieldInterface extends PageBlockInterface
{
    function getKey(): ?string;
    function setKey(string $key): FieldInterface;

    function getParent(): ComponentInterface;
    function setParent(ComponentInterface $parent): FieldInterface;

    function getParentForm(): FormComponentInterface;
    function setParentForm(FormComponentInterface $parentForm): FieldInterface;

}