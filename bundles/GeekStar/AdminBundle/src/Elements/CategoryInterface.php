<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\User\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

interface CategoryInterface
{
    function configure();
    function initialize();

    function getContainer(): ContainerInterface;
    function setContainer(ContainerInterface $container): CategoryInterface;

    function isVisible(): bool;
    function setVisible(bool $visible): CategoryInterface;

    function getSortIndex(): int;
    function setSortIndex(int $sortIndex): CategoryInterface;

    function getTitle(): string;
    function setTitle(string $title): CategoryInterface;

    function getName(): string;
    function setName(string $name): CategoryInterface;

    function getUrn(): string;
    function getUri(): string;

    function getPermission(): string;
    function setPermission(string $permission): CategoryInterface;
    function checkPermission(UserInterface $user): bool;

    /**
     * @param string|ModuleInterface $module
     * @return CategoryInterface
     */
    function addModule($module): CategoryInterface;
    function getModule(string $urn): ?ModuleInterface;

    /**
     * @return ModuleInterface[]
     */
    function getModules(): array;
    function sortModules();
}