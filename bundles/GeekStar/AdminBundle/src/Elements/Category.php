<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\User\UserInterface;
use GeekStar\AdminBundle\Exception\ElementException;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Category implements CategoryInterface
{
    protected ?ContainerInterface $container;

    protected bool $visible = true;
    protected int $sortIndex = 0;
    protected string $title = '';
    protected string $name;
    protected string $permission;

    abstract public function configure();
    abstract public function initialize();

    /** @var ModuleInterface[] $modules */
    public array $modules = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container): Category
    {
        $this->container = $container;

        return $this;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): Category
    {
        $this->visible = $visible;

        return $this;
    }

    public function getSortIndex(): int
    {
        return $this->sortIndex;
    }

    public function setSortIndex(int $sortIndex): Category
    {
        $this->sortIndex = $sortIndex;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Category
    {
        $this->title = $title;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Category
    {
        $this->name = $name;

        return $this;
    }

    public function getUri(): string
    {
        return $this->getUrn();
    }

    public function getUrn(): string
    {
        return lcfirst($this->getName());
    }

    public function getPermission(): string
    {
        return $this->permission;
    }

    public function setPermission(string $permission): Category
    {
        $this->permission = $permission;

        return $this;
    }

    public function checkPermission(UserInterface $user): bool
    {
        if($this->permission)
        {
            return $this->container->get('security.authorization_checker')->isGranted($this->permission);
        }

        return true;
    }

    public function addModule($module): Category
    {
        $isNew = false;

        if(is_string($module)) {
            $isNew = true;
            $module = $this->container->get($module);
        }

        if(!($module instanceof ModuleInterface)) {
            throw new ElementException('Module not implement ModuleInterface');
        } else if($isNew) {
            $module = clone $module;
        }

        $module->configure();

        if($module->isVisible())
        {
            $module
                ->setCategory($this)
                ->initialize();

            $module->sortPages();

            if($module->getDefaultPage())
            {
                $this->modules[$module->getUrn()] = $module;
            }
        }

        return $this;
    }

    public function getModule(string $urn): ?ModuleInterface
    {
        return $this->modules[$urn] ?? null;
    }

    public function getModules(): array
    {
        return $this->modules;
    }

    public function sortModules()
    {
        uasort($this->modules, function(ModuleInterface $a, ModuleInterface $b)
        {
            if($a->getSortIndex() > $b->getSortIndex()) return 1;
            if($a->getSortIndex() < $b->getSortIndex()) return -1;
            return 0;
        });
    }
}