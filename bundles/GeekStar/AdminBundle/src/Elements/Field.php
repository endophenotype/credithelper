<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

abstract class Field extends PageBlock implements FieldInterface
{
    protected ?ComponentInterface $parent = null;
    protected ?FormComponentInterface $parentForm = null;

    public function create(): PageBlock
    {
        parent::create();

        $this->addEvent('load', '(response) => {
            let $field = $(this);
            let newField = $(response.content).insertAfter($field)[0];
            
            this.actions.destroy();
            
            this.form.root.initField(newField, response, this.form);
            this.form.root.renderField(newField);
        }');

        $this->addAction('serialize', '() => {}');
        $this->addAction('reset', '() => {}');

        $this->addAction('showError', '(message) => {
            let $field = $(this);
            $field.addClass("error");
            
            let error = $field.find(".js-field-error-text");
            if(error.length !== 0) {
                error.html(message);
            }
        }');

        return $this;
    }

    abstract public function configure();
    abstract public function initialize(ComponentRequest $request, ComponentResponse $response);

    public function getKey(): ?string
    {
        return $this->config->get('_key');
    }

    public function setKey(string $key): Field
    {
        $this->config->set('_key', $key);

        return $this;
    }

    public function getParent(): ComponentInterface
    {
        return $this->parent;
    }

    public function setParent(ComponentInterface $parent): Field
    {
        $this->parent = $parent;

        $this->config->set('_parent', $parent->getKey());

        return $this;
    }

    public function getParentForm(): FormComponentInterface
    {
        return $this->parentForm;
    }

    public function setParentForm(FormComponentInterface $parentForm): Field
    {
        $this->parentForm = $parentForm;

        $this->config->set('_parentForm', $parentForm->getKey());

        return $this;
    }

    public function end(): ComponentInterface
    {
        return $this->getParent();
    }
}