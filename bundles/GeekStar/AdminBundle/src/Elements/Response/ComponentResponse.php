<?php

namespace GeekStar\AdminBundle\Elements\Response;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Model\ApiError;
use Symfony\Component\HttpFoundation\ParameterBag;

class ComponentResponse
{
    public ParameterBag $query;
    public ParameterBag $data;
    public ?string $content = null;
    public ?array $config = null;
    public ?array $events = null;
    public ?array $actions = null;
    public ?array $fields = null;
    public ApiError $error;

    public function __construct()
    {
        $this->data = new ParameterBag();
        $this->query = new ParameterBag();
        $this->error = new ApiError();
    }

    public function dump(): array
    {
        if($this->error->has()) {
            return $this->error->dump();
        } else {
            $response = [];

            if($this->query->count()) {
                $response['query'] = $this->query->all();
            }

            if($this->data->count()) {
                $response['data'] = $this->data->all();
            }

            if($this->content) {
                $response['content'] = $this->content;
            }

            if($this->config) {
                $response['config'] = $this->config;
            }

            if($this->events) {
                $response['events'] = $this->events;
            }

            if($this->actions) {
                $response['actions'] = $this->actions;
            }

            if($this->fields) {
                $response['fields'] = $this->fields;
            }

            return $response;
        }
    }

    public function fromRequest(ComponentRequest $request)
    {
        $this->query->add($request->query->all());
    }
}