<?php

namespace GeekStar\AdminBundle\Elements\Response;

use GeekStar\AdminBundle\Model\ApiError;
use Symfony\Component\HttpFoundation\ParameterBag;

class PageResponse
{
    public ParameterBag $query;
    public ParameterBag $data;
    public ?string $content = '';
    public ApiError $error;

    public function __construct()
    {
        $this->query = new ParameterBag();
        $this->data = new ParameterBag();
        $this->error = new ApiError();
    }

    public function dump(): array
    {
        if($this->error->has()) {
            return $this->error->dump();
        } else {
            $response = [];

            if($this->query->count()) {
                $response['query'] = $this->query->all();
            }

            if($this->data->count()) {
                $response['data'] = $this->data->all();
            }

            if($this->content) {
                $response['content'] = $this->content;
            }

            return $response;
        }
    }
}