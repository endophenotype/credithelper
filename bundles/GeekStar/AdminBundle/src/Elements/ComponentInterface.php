<?php

namespace GeekStar\AdminBundle\Elements;

interface ComponentInterface extends PageBlockInterface
{
    function register(?PageInterface $page = null, bool $isAddToPage = false);

    function getKey(): ?string;
    function setKey(string $key): ComponentInterface;

    function getNodes(): array;
    function setNodes(array $nodes): ComponentInterface;

    /** @return PageInterface|ComponentInterface|null */
    function getParent();
    /** @param PageInterface|ComponentInterface|null $parent */
    function setParent($parent): ComponentInterface;

    function getParentForm(): ?FormComponentInterface;
    function setParentForm(?FormComponentInterface $formParent): ComponentInterface;

    function getCurrentPage(): ?PageInterface;
    function setCurrentPage(PageInterface $page): ComponentInterface;

    /**
     * @param string|ComponentInterface $component
     * @param string|null $key
     * @return ComponentInterface
     */
    function addComponent($component, ?string $key = null): ComponentInterface;
    function getComponent(string $key): ?ComponentInterface;
    /** @return ComponentInterface[]|null */
    function getComponents(): array;

    /**
     * @param ComponentInterface|FieldInterface|string $item
     * @param string|null $key
     * @return ComponentInterface|FieldInterface
     */
    function addContentItem($item, ?string $key = null);
    /** @return PageInterface|ComponentInterface|null */
    function end();

    function renderContentItems(): string;
}