<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class PopupComponent extends Component
{
    public function create(): PopupComponent
    {
        parent::create();

        $this->setTitle('');

        $this->addAction('default', '() => {
            let popupComponent = this;
            let $popupComponent = $(this);
            
            let componentModal = $popupComponent.find("> .modal")[0];
            let $componentModal = $(componentModal);
            let $modalDialog = $componentModal.find("> .modal-dialog");
            let $btnClose = $modalDialog.find("> .modal-content > .modal-header .btn-close");
            
            let modal = new bootstrap.Modal(componentModal);
            let $parentPopups = $popupComponent.parents(".js-component.popup-component");
            let parentPopups = [];
            $parentPopups.each(function(index) {
                parentPopups[index] = this;
            });
            
            popupComponent.modal = modal;
            popupComponent.level = parentPopups.length;
            popupComponent.parentPopups = parentPopups;
            
            $modalDialog.css("max-width", (90 - popupComponent.level * 3) + "%");
            
            $btnClose.on("click", function(e) {
                popupComponent.modal.hide();
            });
            
            $componentModal.on("hidden.bs.modal", function (e) {
                popupComponent.actions.close();
                $.each(parentPopups, function(index){
                    parentPopups[index].modal.hide();
                    parentPopups[index].modal.show();
                });
            });
        }');

        $this->addAction('show', '(data) => {
            this.actions.open(data);
            this.modal.show();
        }');

        $this->addAction('open', '(data) => {
        }');

        $this->addAction('close', '(data) => {
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function getTitle(): string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): PopupComponent
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getWidth(): string
    {
        return $this->config->get('width');
    }

    public function setWidth(string $width): PopupComponent
    {
        $this->config->set('width', $width);

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PopupComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/popup.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}