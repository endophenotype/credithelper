<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class ButtonComponent extends Component
{
    const THEME_PRIMARY = 'primary';
    const THEME_SECONDARY = 'secondary';
    const THEME_SUCCESS = 'success';
    const THEME_DANGER = 'danger';
    const THEME_WARNING = 'warning';
    const THEME_INFO = 'info';
    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';
    const THEME_WHITE = 'white';
    const THEME_TRANSPARENT = 'transparent';

    const THEME_OUTLINE_PRIMARY = 'outline-primary';
    const THEME_OUTLINE_SECONDARY = 'outline-secondary';
    const THEME_OUTLINE_SUCCESS = 'outline-success';
    const THEME_OUTLINE_DANGER = 'outline-danger';
    const THEME_OUTLINE_WARNING = 'outline-warning';
    const THEME_OUTLINE_INFO = 'outline-info';
    const THEME_OUTLINE_LIGHT = 'outline-light';
    const THEME_OUTLINE_DARK = 'outline-dark';
    const THEME_OUTLINE_WHITE = 'outline-white';

    const SIZE_SMALL = 'sm';
    const SIZE_LARGE = 'lg';

    public function create(): Component
    {
        parent::create();

        $this
            ->setTheme(self::THEME_PRIMARY);

        $this->addAction('default', '() => {
            let buttonComponent = this;
            let $button = $(this).find("> .btn");    
            
            $button.on("click", function(e) {
                if(!buttonComponent.config.url) {
                    e.preventDefault();
                }
                
                buttonComponent.actions.click();
            });
        }');

        $this->addAction('click', '() => {}');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function getTitle(): string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): ButtonComponent
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->config->get('theme');
    }

    public function setTheme(string $theme): ButtonComponent
    {
        $this->config->set('theme', $theme);

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->config->get('size');
    }

    public function setSize(string $size): ButtonComponent
    {
        $this->config->set('size', $size);

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->config->get('icon');
    }

    public function setIcon(string $icon): ButtonComponent
    {
        $this->config->set('icon', $icon);

        return $this;
    }

    public function getWidth(): int
    {
        return $this->config->get('width');
    }

    public function setWidth(int $width): ButtonComponent
    {
        $this->config->set('width', $width);

        return $this;
    }

    public function getUrl(): string
    {
        return $this->config->get('url');
    }

    public function setUrl(string $url): ButtonComponent
    {
        $this->config->set('url', $url);

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): ButtonComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/button.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}