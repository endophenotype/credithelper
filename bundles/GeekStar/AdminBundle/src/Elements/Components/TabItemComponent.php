<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class TabItemComponent extends Component
{
    public function create(): TabItemComponent
    {
        parent::create();

        $this
            ->setActive(false)
            ->setId('');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function getId(): string
    {
        return $this->config->get('id');
    }

    public function setId(string $id): TabItemComponent
    {
        $this->config->set('id', $id);

        return $this;
    }

    public function getActive(): bool
    {
        return $this->config->get('active');
    }

    public function setActive(bool $active): TabItemComponent
    {
        $this->config->set('active', $active);

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): TabItemComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/tab_item.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}