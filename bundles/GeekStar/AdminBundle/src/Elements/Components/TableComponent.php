<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class TableComponent extends Component
{
    const THEME_PRIMARY = 'primary';
    const THEME_SECONDARY = 'secondary';
    const THEME_SUCCESS = 'success';
    const THEME_DANGER = 'danger';
    const THEME_WARNING = 'warning';
    const THEME_INFO = 'info';
    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';
    const THEME_WHITE = 'white';
    const THEME_TRANSPARENT = 'transparent';

    private ?array $templateRow = null;
    public array $table = [];

    public function create(): Component
    {
        parent::create();

        $this
            ->setWithFooter(false)
            ->setStriped(false)
            ->setWithOutBorder(false)
            ->setAlignCenter(false)
            ->setSortable(false)
            ->setCollapsed(false);

        $this->addAction('setSearchFilter', '() => {
            let component = this;
            let $component = $(this);
            let searchGroup = $component.find(".filters .searchFilter .input-group");
            if(searchGroup.length !== 0) {
                let input = searchGroup.find("input");
                let button = searchGroup.find("button");
                
                button.on("click", function(e){
                    e.preventDefault();
                    component.data.filter.page = 1;
                    component.data.filter.search = input.val();
                    component.actions.filter();
                });
            }
        }');

        $this->addAction('setCountFilter', '() => {
            let component = this;
            let $component = $(this);
            let countFilter = $component.find(".filters .countFilter");
            if(countFilter.length !== 0) {
                let select = countFilter.find("select");
                
                select.on("change", function(e){
                    component.data.filter.page = 1;
                    component.data.filter.limit = this.value;
                    component.actions.filter();
                });
            }
        }');

        $this->addAction('setPagination', '() => {
            let component = this;
            let $component = $(this);
            let pagination = $component.find(".navigation .pagination");
            if(pagination.length !== 0) {
                let pgItems = pagination.find(".page-item a");
                
                pgItems.on("click", function(e){
                    e.preventDefault();
                    component.data.filter.page = $(this).data("page");
                    component.actions.filter();
                });
            }
        }');

        $this->addAction('toBlock', '() => {
            let component = this;
            let $component = $(this);
            
            let $table = $component.find("table");
            $table.show();
            
            let $thead = $table.find("thead");
            let $cols = $thead.find("tr th");
            let colWidth = [];
            let tableWidth = $thead.width();
            $cols.each(function(key, item) {
                colWidth[key] = ($(item).outerWidth() / tableWidth * 100) + "%";
            });
            
            $table.hide();
            
            let $rows = $component.find("div.block-table .block-table-row");
            
            $rows.each(function(key, row) {
                let $cols = $(row).find(".block-table-col, .block-table-head-col");
                $cols.each(function(key, col) {
                    let $cols = $(col).css("width", colWidth[key]);
                });
            });
        }');

        $this->addAction('setScrollContent', '() => {
            let component = this;
            let $component = $(this);
            let scrollBody = $component.find(".scrollContent");
            if(scrollBody.length != 0) {
                scrollBody.mCustomScrollbar({
                    axis: "xy",
                    scrollInertia: 150,
                });
            }
        }');

        $this->addAction('setSortable', '() => {
            let component = this;
            let $component = $(this);
            
            if(component.config.sortable) {
                let $rows = $component.find(".block-table-body, .block-table-body .items");
                
                $rows.sortable({
                    axis: "y",
                    cursor: "move",
                    handle: "> .control-column .mover",
                    start: function(e, ui){
                        ui.item[0].startIndex = ui.item.index();
                        ui.placeholder.height(ui.item.height());
                    },
                    stop: function(e, ui){
                        component.actions.sort(ui.item);
                    }
                });
            }
        }');

        $this->addAction('setCollapsed', '() => {
            let component = this;
            let $component = $(this);
            
            let $table = $component.find("div.block-table");
            if(component.config.collapsed) {
                let $rows = $table.find(".block-table-row");
                $rows.each(function(key, row) {
                    let $row = $(row);
                    let $collapser = $row.find("> .control-column .collapser");
                    
                    if($collapser.length !== 0) {
                        $collapser.on("click", function(e){
                            $row.toggleClass("collapsed");
                        });
                    }
                });
            }
        }');

        $this->addAction('default', '() => {
            let component = this;
            let $component = $(this);
            if(component.data.filter === undefined) {
                component.data.filter = {};
            }
            
            component.actions.setSearchFilter();
            component.actions.setCountFilter();
            component.actions.setPagination();
            component.actions.toBlock();
            component.actions.setScrollContent();
            component.actions.setSortable();
            component.actions.setCollapsed();
            
            $(window).resize(function() {
                component.actions.toBlock();
            });
        }');

        $this->addAction('filter', '() => {
            let component = this;
            component.events.filter({
                filter: component.data.filter
            });
        }');

        $this->addEvent('filter', '(response) => {
            let $component = $(this);
            
            let top = $component.offset().top;
            
            let newComponent = $(response.content).insertAfter($component)[0];
            this.root.initComponent(newComponent, response, this.root, this.parent);
            this.root.renderComponent(newComponent);
            $component.remove();
            
            $(window).scrollTop(top);
        }');

        $this->addHandler('filter', [$this, 'filterHandler']);


        $this->addAction('sort', '($item) => {
            let component = this;
            let start = $item[0].startIndex;
            let stop  = $item.index();
            let step = start > stop ? 1 : -1;
            
            let $items = $item.parent().children();
            
            let data = {
                items: {}
            };
            
            if(component.config.collapsed) {
                data.nodes = $item.parent(".items").parent().data("nodes");
            }
            
            let oldIndex = $items.eq(stop).data("sort-index");
            
            for (let i = stop; i != (start + step); i = i + step) {
                let newIndex = $items.eq(i + step).data("sort-index");
                $items.eq(i).data("sort-index", newIndex);
                data.items[$items.eq(i).data("key")] = newIndex;
            }
            
            $items.eq(start).data("sort-index", oldIndex);
            data.items[$items.eq(start).data("key")] = oldIndex;
            
            component.events.sort({
                sort: data
            });
        }');

        $this->addEvent('sort', '(response) => {}');

        $this->addHandler('sort', [$this, 'sortHandler']);

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getWithFooter(): bool
    {
        return $this->config->get('withFooter');
    }

    public function setWithFooter(bool $withFooter): TableComponent
    {
        $this->config->set('withFooter', $withFooter);

        return $this;
    }

    public function getWithOutBorder(): bool
    {
        return $this->config->get('withOutBorder');
    }

    public function setWithOutBorder(bool $withOutBorder): TableComponent
    {
        $this->config->set('withOutBorder', $withOutBorder);

        return $this;
    }

    public function getAlignCenter(): bool
    {
        return $this->config->get('alignCenter');
    }

    public function setAlignCenter(bool $alignCenter): TableComponent
    {
        $this->config->set('alignCenter', $alignCenter);

        return $this;
    }

    public function getHeaderTheme(): ?string
    {
        return $this->config->get('headerTheme');
    }

    public function setHeaderTheme(string $headerTheme): TableComponent
    {
        $this->config->set('headerTheme', $headerTheme);

        return $this;
    }

    public function getStriped(): bool
    {
        return $this->config->get('striped');
    }

    public function setStriped(bool $stripped): TableComponent
    {
        $this->config->set('striped', $stripped);

        return $this;
    }

    public function getCountFilters(): ?array
    {
        return $this->config->get('countFilters');
    }

    public function setCountFilters(array $countFilters): TableComponent
    {
        $this->config->set('countFilters', $countFilters);

        return $this;
    }

    public function getSearchFilter(): ?bool
    {
        return $this->config->get('searchFilter');
    }

    public function setSearchFilter(bool $searchFilter): TableComponent
    {
        $this->config->set('searchFilter', $searchFilter);

        return $this;
    }

    public function getColumn(string $key): ?array
    {
        return $this->config->get('columns')[$key] ?? null;
    }

    public function addColumn(string $key, string $title, bool $compact = null, $template = null): TableComponent
    {
        $columns = $this->getColumns();
        $columns[$key]['title'] = $title !== '.' ? $title : '';
        $columns[$key]['compact'] = $compact !== null ? $compact : $title == '.';
        $columns[$key]['template'] = $template ?: '{{ ' . $key . ' }}';
        $this->config->set('columns', $columns);

        return $this;
    }

    public function getColumns(): array
    {
        return $this->config->get('columns') ?: [];
    }

    public function setColumns(array $columns): TableComponent
    {
        $this->config->set('columns', $columns);

        return $this;
    }

    public function getContentHeight(): ?string
    {
        return $this->config->get('contentHeight');
    }

    public function setContentHeight(string $contentHeight): TableComponent
    {
        $this->config->set('contentHeight', $contentHeight);

        return $this;
    }

    public function getSortable(): ?bool
    {
        return $this->config->get('sortable');
    }

    public function setSortable(bool $sortable): TableComponent
    {
        $this->config->set('sortable', $sortable);

        return $this;
    }

    public function getCollapsed(): ?bool
    {
        return $this->config->get('collapsed');
    }

    public function setCollapsed(bool $collapsed): TableComponent
    {
        $this->config->set('collapsed', $collapsed);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getTable(): array
    {
        return $this->table;
    }

    public function setTable(array $table): TableComponent
    {
        $this->table = $table;

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function getTemplateRow(bool $isUpdate = false): array
    {
        if($this->templateRow === null or $isUpdate) {
            $this->templateRow = [];

            foreach ($this->getColumns() as $key => $column) {
                $this->templateRow[$key] = $column['template'];
            }
        }

        return $this->templateRow;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): TableComponent
    {
        if($isUpdate or !$this->content) {
            $content = $this->twig->render('@GeekStarAdmin/Component/table.component.twig', $response->dump());
            $this->setContent($content);
        }

        return $this;
    }

    public function loadData(ComponentRequest $request, ComponentResponse $response): TableComponent
    {
        $this->loadTable($request, $response, $this->table);
        $this->normalizeTable($this->table);

        $filter = $request->query->get('filter') ?: [];

        $this->filter($request, $response, $this->table, $filter);
        $this->paginate($request, $response, $this->table, $filter);
        $response->data->set('filter', $filter);

        $renderTable = $this->renderTable($this->table);
        $response->data->set('table', $renderTable);

        return $this;
    }

    protected function normalizeTable(array &$table)
    {
        $normalizeTable = [];

        foreach ($table as $rowKey => $tableRow) {
            if (!isset($tableRow['_key'])) {
                $tableRow['_key'] = $rowKey;
            }

            if($this->getCollapsed() and isset($tableRow['items'])) {
                $this->normalizeTable($tableRow['items']);
            }

            $normalizeTable[$tableRow['_key']] = $tableRow;
        }

        $table = $normalizeTable;
    }

    protected function renderTable(array $items, array $nodes = []): array
    {
        $renderItems = [];

        foreach ($items as $rowKey => $item) {
            $renderItems[$rowKey] = $this->renderTableItem($item, $nodes);
        }

        return $renderItems;
    }

    protected function renderTableItem(array $item, array $nodes = []): array
    {
        $nodes[] = $item['_key'];

        $renderItem['_level'] = count($nodes);
        $renderItem['_collapsed'] = $item['_collapsed'] ?? false;

        if($this->getSortable() and isset($item['sortIndex'])) {
            $renderItem['sortIndex'] = $item['sortIndex'];
        }

        if($this->getCollapsed() and isset($item['items'])) {
            $renderItem['items'] = $this->renderTable($item['items'], $nodes);
        }

        if($this->getCollapsed()) {
            $renderItem['_nodes'] = json_encode($nodes);
        }

        $withControlColumn = ($this->getSortable() or $this->getCollapsed());

        if($withControlColumn) {
            if(!isset($item['_controlTitle'])) {
                $item['_control'] = '';
            }

            $renderItem['_control'] = $item['_control'];

            if($this->getColumn('_control') === null) {
                $controlColumn = [
                    '_control' => [
                        'title' => '',
                        'compact' => true,
                        'template' => '{{ _control }}'
                    ]
                ];

                $columns = array_merge($controlColumn, $this->getColumns());
                $this->setColumns($columns);
            }
        }

        $templateRow = $this->getTemplateRow();
        $this->decorate($this->getRequest(), $this->getResponse(), $item, $templateRow, $nodes, $item['scope'] ?? null);

        foreach ($this->getColumns() as $colKey => $column) {
            $renderItem[$colKey] = $this->twig
                ->createTemplate($templateRow[$colKey])
                ->render($item);
        }

        return $renderItem;
    }

    public function loadTable(ComponentRequest $request, ComponentResponse $response, array &$table)
    {
    }

    public function filter(ComponentRequest $request, ComponentResponse $response, array &$table, array &$filter)
    {
    }

    protected function calculatePagination(ComponentRequest $request, ComponentResponse $response, array &$table, array $filter): array
    {
        $limit = null;
        $page = 1;
        $total = count($table);

        if($filter) {
            $limit = $filter['limit'] ?? null;

            if(is_numeric($filter['page'] ?? null)) {
                $page = $filter['page'];
            }
        }

        if($limit === null and $countFilter = $this->getCountFilters()) {
            if($key = array_key_first($countFilter) and is_numeric($key)) {
                $limit = $key;
            }
        }

        if(!is_numeric($limit)) {
            $limit = null;
        }

        $pageCount = $limit ? ceil($total / $limit) : 1;

        if($page > $pageCount) {
            $page = 1;
        }

        return [
            'page' => $page,
            'pageCount' => $pageCount,
            'limit' => $limit,
            'total' => $total,
        ];
    }

    public function paginate(ComponentRequest $request, ComponentResponse $response, array &$table, array &$filter)
    {
        $pagination = $this->calculatePagination($request, $response, $this->table, $filter);

        $limit = $pagination['limit'] ?? null;
        $page = $pagination['page'] ?? 1;

        if($limit !== null) {
            $response->data->set('pagination', $pagination);

            $filter['limit'] = $limit;
            $filter['page'] = $page;

            $table = array_slice($table, $limit * ($page - 1), $limit, true);
        }
    }

    public function decorate(ComponentRequest $request, ComponentResponse $response, array $tableRow, array &$templateRow, array $nodes = null, string $scope = null)
    {
    }

    public function filterHandler(ComponentRequest $request, ComponentResponse $response)
    {
        $this->handle('load', $request, $response);
    }

    public function sortHandler(ComponentRequest $request, ComponentResponse $response)
    {
    }
}