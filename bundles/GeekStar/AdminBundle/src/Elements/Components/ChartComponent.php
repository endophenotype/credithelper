<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class ChartComponent extends Component
{
    protected array $options = [];
    protected array $series = [];

    public function create(): Component
    {
        parent::create();

        $this->addAction('default', '() => {
            let chart = new ApexCharts(document.querySelector("#chart-component-" + this.data.key), this.data.options);
            chart.render();
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function setType(string $type): ChartComponent
    {
        $this->options['chart']['type'] = $type;

        return $this;
    }

    public function setWidth(string $width): ChartComponent
    {
        $this->options['chart']['width'] = $width;

        return $this;
    }

    public function setHeight(string $height): ChartComponent
    {
        $this->options['chart']['height'] = $height;

        return $this;
    }

    public function setIsSparkline(): ChartComponent
    {
        $this->options['chart']['sparkline']['enabled'] = true;

        return $this;
    }

    public function setWithoutToolbar(): ChartComponent
    {
        $this->options['chart']['toolbar']['show'] = false;

        return $this;
    }

    public function setCategories(array $categories)
    {
        $this->options['xaxis']['categories'] = $categories;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions($options): ChartComponent
    {
        $this->options = $this->getArrayOptions($options);

        return $this;
    }

    public function addOptions($options): ChartComponent
    {
        $options = $this->getArrayOptions($options);
        $this->options = array_merge_recursive($this->options, $options);

        return $this;
    }

    public function addSeries(string $key, $name = null, $data = null, $type = null, $color = null): ChartComponent
    {
        $series = [];

        if($name) {
            $series['name'] = $name;
        }

        if($type) {
            $series['type'] = $type;
        }

        if($color) {
            $series['color'] = $color;
        }

        $series['data'] = $data ?: [];

        $this->addOptions([
            'series' => [
                $series
            ]
        ]);

        $this->series[$key] = count($this->getOptions()['series']) - 1;

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function addPoint(string $seriesKey, $y, $x = null): ChartComponent
    {
        if($y !== null) {
            $point = [
                'x' => $x,
                'y' => $y
            ];
        } else {
            $point = $y;
        }

        $this->options['series'][$this->series[$seriesKey]]['data'][] = $point;

        return $this;
    }

    public function addPoints(string $seriesKey, array $points): ChartComponent
    {
        $this->options['series'][$this->series[$seriesKey]]['data'] = array_merge($this->options['series'][$this->series[$seriesKey]]['data'], $points);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function loadData(ComponentRequest $request, ComponentResponse $response): ChartComponent
    {
        $this->loadChartData($request, $response);
        $response->data->set('options', $this->getOptions());

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): ChartComponent
    {
        if($isUpdate or !$this->content) {
            $content = $this->twig->render('@GeekStarAdmin/Component/chart.component.twig', $response->dump());
            $this->setContent($content);
        }

        return $this;
    }

    private function getArrayOptions($options): array
    {
        $result = [];

        if(is_array($options)) {
            $result = $options;
        } else if(is_string($options)) {
            $decode = json_decode($options, true);
            if($decode !== null) {
                $result = $decode;
            } else {
                $decode = $this->jsObjectToPhpArray($options);
                if($decode !== null) {
                    $result = $decode;
                }
            }
        }

        return $result;
    }

    private function jsObjectToPhpArray(string $object): array
    {
        $object = str_replace("'", '"', $object);
        $object = preg_replace('/([a-z_]+)\: /ui', '"$1":', $object);
        return json_decode($object, true) ?: [];
    }

    public function loadChartData(ComponentRequest $request, ComponentResponse $response)
    {
    }
}