<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class PanelComponent extends Component
{
    const THEME_PRIMARY = 'primary';
    const THEME_SECONDARY = 'secondary';
    const THEME_SUCCESS = 'success';
    const THEME_DANGER = 'danger';
    const THEME_WARNING = 'warning';
    const THEME_INFO = 'info';
    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';
    const THEME_WHITE = 'white';
    const THEME_TRANSPARENT = 'transparent';

    public function create(): PanelComponent
    {
        parent::create();

        $this->setHeight('100%');

        $this->addAction('default', '() => {
            let menu = $(this).find(".panel-header .menu");
            let component = this;
            if(menu.length !== 0) {
                let menuItems = menu.find(".dropdown-item");
                menuItems.each(function() {
                    let $item = $(this);
                    let action = $item.data("action");
                    let event = $item.data("event");
                    if(action !== undefined || event !== undefined) {
                        $item.on("click", function(e){
                            e.preventDefault();
                            if(event !== undefined) {
                                component.events[event]();
                            }                            
                            if(action !== undefined) {
                                component.actions[action]();
                            }
                        });
                    }
                });
            }
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): PanelComponent
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getHeaderTheme(): ?string
    {
        return $this->config->get('headerTheme');
    }

    public function setHeaderTheme(string $headerTheme): PanelComponent
    {
        $this->config->set('headerTheme', $headerTheme);

        return $this;
    }

    public function getBorderBottom(): ?string
    {
        return $this->config->get('borderBottom');
    }

    public function setBorderBottom(string $borderBottomTheme): PanelComponent
    {
        $this->config->set('borderBottom', $borderBottomTheme);

        return $this;
    }

    public function addMenuItem(string $title, string $href = null, string $action = null, string $event = null, $name = null): PanelComponent
    {
        $menuItems = $this->getMenuItems();
        $data = [
            'title' => $title,
            'href' => $href,
            'action' => $action,
            'event' => $event,
        ];

        if($name) {
            $menuItems[$name] = $data;
        } else {
            $menuItems[] = $data;
        }

        $this->config->set('menuItems', $menuItems);

        return $this;
    }

    public function getMenuItem(string $name): ?array
    {
        return $this->getMenuItems()[$name] ?? null;
    }

    public function removeMenuItem(string $name): PanelComponent
    {
        $menuItems = $this->getMenuItems();
        unset($menuItems[$name]);

        $this->config->set('menuItems', $menuItems);

        return $this;
    }

    public function getMenuItems(): array
    {
        return $this->config->get('menuItems') ?: [];
    }

    public function getHeight(): ?string
    {
        return $this->config->get('height');
    }

    public function setHeight(string $height): PanelComponent
    {
        $this->config->set('height', $height);

        return $this;
    }

    public function getContentHeight(): ?string
    {
        return $this->config->get('contentHeight');
    }

    public function setContentHeight(string $contentHeight): PanelComponent
    {
        $this->config->set('contentHeight', $contentHeight);

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PanelComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/panel.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}