<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class TabComponent extends Component
{
    public function create(): TabComponent
    {
        parent::create();

        $this->setTabs([]);

        $this->addAction('default', '() => {
            let $component = $(this);
            
            $component.find(".nav-tabs a").click(function (e) {
                e.preventDefault()
                $(this).tab("show")
            })
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function addTab(string $key, string $title): TabComponent
    {
        $tabs = $this->getTabs();
        $tabs[$key] = $title;
        $this->setTabs($tabs);

        return $this;
    }

    public function getTabs(): array
    {
        return $this->config->get('tabs') ?: [];
    }

    public function setTabs(array $tabs): TabComponent
    {
        $this->config->set('tabs', $tabs);

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): TabComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/tab.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}