<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\FormComponent as FormComponentAbstract;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class FormComponent extends FormComponentAbstract
{
    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): FormComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/form.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}