<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class GroupComponent extends Component
{
    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function getTitle(): string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): GroupComponent
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getNote(): string
    {
        return $this->config->get('note');
    }

    public function setNote(string $note): GroupComponent
    {
        $this->config->set('note', $note);

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): GroupComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/group.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}