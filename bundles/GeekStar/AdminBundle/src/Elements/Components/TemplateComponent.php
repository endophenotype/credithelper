<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class TemplateComponent extends Component
{
    protected string $template = '';

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function setTemplate(string $template): TemplateComponent
    {
        $this->template = $template;

        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): TemplateComponent
    {
        parent::render($request, $response, $isUpdate);

        $context = $response->dump();

        $content = $this->twig
            ->createTemplate($this->getTemplate())
            ->render($context + [
                'content' => $this->getContent(),
            ]);

        $content = $this->twig->render('@GeekStarAdmin/Component/template.component.twig', $context + [
            'content' => $content,
        ]);

        $this->setContent($content);

        return $this;
    }
}