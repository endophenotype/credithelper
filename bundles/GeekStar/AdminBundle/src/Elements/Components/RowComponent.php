<?php

namespace GeekStar\AdminBundle\Elements\Components;

use GeekStar\AdminBundle\Elements\Component;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class RowComponent extends Component
{
    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): RowComponent
    {
        parent::render($request, $response, $isUpdate);

        $content = $this->twig->render('@GeekStarAdmin/Component/row.component.twig', $response->dump() + [
            'content' => $this->getContent(),
        ]);

        $this->setContent($content);

        return $this;
    }
}