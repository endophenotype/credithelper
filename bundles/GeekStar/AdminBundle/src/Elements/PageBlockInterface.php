<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

interface PageBlockInterface
{
    function create(): PageBlockInterface;
    
    function configure();
    function initialize(ComponentRequest $request, ComponentResponse $response);

    function getContainer(): ContainerInterface;
    function setContainer(ContainerInterface $container): PageBlockInterface;

    function getRequest(): ComponentRequest;
    function setRequest(ComponentRequest $request): PageBlockInterface;
    function getResponse(): ComponentResponse;
    function setResponse(ComponentResponse $response): PageBlockInterface;

    function load(ComponentRequest $request, ComponentResponse $response): PageBlockInterface;
    function loadData(ComponentRequest $request, ComponentResponse $response): PageBlockInterface;

    function getData(): ParameterBag;
    function setData(array $data): PageBlockInterface;

    function getContent(bool $isUpdate = false): string;
    function setContent(string $content): PageBlockInterface;

    function preRender(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PageBlockInterface;
    function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PageBlockInterface;
    function postRender(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PageBlockInterface;

    function addAction(string $key, string $function): PageBlockInterface;
    function getActions(): array;
    function addEvent(string $key, string $function): PageBlockInterface;
    function getEvents(): array;

    function addHandler(string $handler, callable $callable): PageBlockInterface;
    function removeHandler(string $handler): PageBlockInterface;
    function hasHandler(string $handler): bool;
    function handle(string $handler, ComponentRequest $request, ComponentResponse $response);
    function handleEvents($events, ComponentRequest $request, ComponentResponse $response);

    function loadHandler(ComponentRequest $request, ComponentResponse $response);
}