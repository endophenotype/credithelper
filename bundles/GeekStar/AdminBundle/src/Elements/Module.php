<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Exception\ElementException;
use GeekStar\AdminBundle\Elements\User\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Module implements ModuleInterface
{
    protected ContainerInterface $container;

    protected bool $visible = true;
    protected int $sortIndex = 0;
    protected string $icon;
    protected string $name;
    protected string $title;
    protected ?CategoryInterface $category = null;
    protected string $permission;

    protected ?PageInterface $defaultPage = null;

    abstract public function configure();
    abstract public function initialize();

    /** @var PageInterface[] $modules */
    public array $pages = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container): Module
    {
        $this->container = $container;

        return $this;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): Module
    {
        $this->visible = $visible;

        return $this;
    }

    public function getSortIndex(): int
    {
        return $this->sortIndex;
    }

    public function setSortIndex(int $sortIndex): Module
    {
        $this->sortIndex = $sortIndex;

        return $this;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): Module
    {
        $this->icon = $icon;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Module
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Module
    {
        $this->title = $title;

        return $this;
    }

    public function getCategory(): CategoryInterface
    {
        return $this->category;
    }

    public function setCategory(CategoryInterface $category): Module
    {
        $this->category = $category;

        return $this;
    }

    public function getPermission(): string
    {
        return $this->permission;
    }

    public function setPermission(string $permission): Module
    {
        $this->permission = $permission;

        return $this;
    }

    public function checkPermission(UserInterface $user): bool
    {
        if($this->permission)
        {
            return $this->container->get('security.authorization_checker')->isGranted($this->permission);
        }

        return true;
    }

    public function getUri(): string
    {
        if(!isset($this->category)) {
            throw ElementException::create($this->getContainer()->get('translator')->trans('geek_star_admin.alert.category_not_set'));
        }

        return $this->category->getUri() . '/' . $this->getUrn();
    }

    public function getUrn(): string
    {
        return lcfirst($this->getName());
    }

    public function addPage($page, bool $isDefault = false): Module
    {
        $isNew = false;

        if(is_string($page)) {
            $isNew = true;
            $page = $this->container->get($page);
        }

        if(!($page instanceof PageInterface)) {
            throw new ElementException('Page not implement PageInterface');
        } else if($isNew) {
            $page = clone $page;
        }

        $page->configure();

        if($page->isVisible())
        {
            $page->setModule($this);

            $this->pages[$page->getUrn()] = $page;

            if($isDefault)
            {
                $this->setDefaultPage($page);
            }
        }

        return $this;
    }

    public function getPage(string $urn): ?PageInterface
    {
        return $this->pages[$urn] ?? null;
    }

    public function getPages(): array
    {
        return $this->pages;
    }

    public function sortPages()
    {
        uasort($this->pages, function(PageInterface $a, PageInterface $b)
        {
            if($a->getSortIndex() > $b->getSortIndex()) return 1;
            if($a->getSortIndex() < $b->getSortIndex()) return -1;
            return 0;
        });
    }

    public function getDefaultPage(): ?PageInterface
    {
        if(!$this->defaultPage and $defaultPage = current($this->pages)) {
            $this->setDefaultPage($defaultPage);
        }

        return $this->defaultPage;
    }

    public function setDefaultPage(PageInterface $page): Module
    {
        $this->defaultPage = $page;

        return $this;
    }
}