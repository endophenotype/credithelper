<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

abstract class PageBlock implements PageBlockInterface
{
    protected ContainerInterface $container;
    protected TranslatorInterface $translator;
    protected Environment $twig;

    protected ComponentRequest $request;
    protected ComponentResponse $response;

    public ParameterBag $config;
    public ParameterBag $data;

    protected ?string $content = null;

    protected array $actions = [];
    protected array $events = [];
    protected array $handlers = [];

    protected bool $isLoad = false;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator)
    {
        $this->container = $container;
        $this->twig = $twig;
        $this->translator = $translator;
    }

    public function create(): PageBlock
    {
        $this->config = new ParameterBag();
        $this->data = new ParameterBag();

        $this->addAction('destroy', '(data) => {
            if(this.components !== undefined) {
                $.each(this.components, function(key, component) {
                    component.actions.destroy();
                });
            }
            
            if(this.fields !== undefined) {
                $.each(this.fields, function(key, field) {
                    field.actions.destroy();
                });
            }
            
            this.actions.onDestroy(data);
            
            $(this).remove();
        }');

        $this->addAction('load', '(data) => {
            this.events.load(data);
        }');

        $this->addEvent('load', '(response) => {
            let $pageBlock = $(this);
            let newPageBlock = $(response.content).insertAfter($pageBlock)[0];
            
            this.actions.destroy();
        }');

        $this->addHandler('load', [$this, 'loadHandler']);

        return $this;
    }

    abstract public function configure();
    abstract public function initialize(ComponentRequest $request, ComponentResponse $response);


    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container): PageBlock
    {
        $this->container = $container;

        return $this;
    }


    public function getRequest(): ComponentRequest
    {
        if(!isset($this->request)) {
            $this->request = new ComponentRequest();
        }

        return $this->request;
    }

    public function setRequest(ComponentRequest $request): PageBlock
    {
        $this->request = $request;

        return $this;
    }

    public function getResponse(): ComponentResponse
    {
        if(!isset($this->response)) {
            $this->response = new ComponentResponse();
            $this->response->fromRequest($this->getRequest());
        }

        return $this->response;
    }

    public function setResponse(ComponentResponse $response): PageBlock
    {
        $this->response = $response;

        return $this;
    }


    public function load(ComponentRequest $request, ComponentResponse $response): PageBlock
    {
        //data
        $this->loadData($request, $response);
        $response->data->add($this->data->all());

        //config
        $response->config = $this->config->all();

        //content
        $response->content = $this->getContent();

        //actions and events
        $response->actions = $this->actions;
        $response->events = $this->events;

        return $this;
    }

    public function loadData(ComponentRequest $request, ComponentResponse $response): PageBlock
    {
        return $this;
    }


    public function getData(): ParameterBag
    {
        return $this->data;
    }

    public function setData(array $data): PageBlock
    {
        $this->data = new ParameterBag($data);

        return $this;
    }


    public function getContent(bool $isUpdate = false): string
    {
        if($isUpdate or $this->content === null) {
            $request = $this->getRequest();
            $response = $this->getResponse();

            if($response->config === null) {
                $response->config = $this->config->all();
            }

            $this->setContent('');

            $this
                ->preRender($request, $response, true)
                ->render($request, $response,true)
                ->postRender($request, $response,true);
        }

        return $this->content;
    }

    public function setContent(string $content): PageBlock
    {
        $this->content = $content;

        return $this;
    }


    public function preRender(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PageBlock
    {
        return $this;
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PageBlock
    {
        return $this;
    }

    public function postRender(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): PageBlock
    {
        return $this;
    }


    public function addAction(string $key, string $function): PageBlock
    {
        $this->actions[$key] = $function;

        return $this;
    }

    public function getActions(): array
    {
        return $this->actions;
    }

    public function addEvent(string $key, string $function): PageBlock
    {
        $this->events[$key] = $function;

        return $this;
    }

    public function getEvents(): array
    {
        return $this->events;
    }


    public function addHandler(string $handler, callable $callable): PageBlock
    {
        $this->handlers[$handler] = $callable;

        return $this;
    }

    public function removeHandler(string $handler): PageBlock
    {
        if($this->hasHandler($handler)){
            unset($this->handlers[$handler]);
        }

        return $this;
    }

    public function hasHandler(string $handler): bool
    {
        return isset($this->handlers[$handler]);
    }

    public function handle(string $handler, ComponentRequest $request, ComponentResponse $response)
    {
        if(!$this->hasHandler($handler)){
            $response->error->set('query/events', $this->translator->trans('geek_star_admin.alert.event_handler_not_found', ['{handler}' => $handler]));
            return;
        }

        $handler = $this->handlers[$handler];
        $handler($request, $response);
    }

    public function handleEvents($events, ComponentRequest $request, ComponentResponse $response)
    {
        if(!is_array($events)) {
            $events = [$events];
        }

        foreach ($events as $event) {
            if(!$this->hasHandler($event)) {
                $response->error->set('query/events', $this->translator->trans('geek_star_admin.alert.event_handler_not_found', ['{handler}' => $event]));
            }
        }

        if(!$response->error->has()) {
            foreach ($events as $event) {
                $this->handle($event, $request, $response);
            }
        }
    }

    public function loadHandler(ComponentRequest $request, ComponentResponse $response)
    {
        if(!$this->isLoad) {
            $this->isLoad = true;
            $this->load($request, $response);
        }
    }
}