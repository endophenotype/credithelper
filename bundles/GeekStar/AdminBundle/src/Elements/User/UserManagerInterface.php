<?php

namespace GeekStar\AdminBundle\Elements\User;

interface UserManagerInterface
{
    public function getUserByUsernamePassword(string $username, string $password): ?UserInterface;
}