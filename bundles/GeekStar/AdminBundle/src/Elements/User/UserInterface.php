<?php

namespace GeekStar\AdminBundle\Elements\User;

use Symfony\Component\Security\Core\User\UserInterface as CoreUserInterface;

interface UserInterface extends CoreUserInterface
{
    public function getAvatarInAdmin(): ?string;
    public function getNameInAdmin(): string;
}