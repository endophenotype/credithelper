<?php

namespace GeekStar\AdminBundle\Elements\Request;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

class PageRequest
{
    public ParameterBag $query;
    public ParameterBag $files;

    public function __construct($request = [])
    {
        $this->query = new ParameterBag(isset($request['query']) && is_array($request['query']) ? $request['query'] : []);
        $this->files = new ParameterBag(isset($request['files']) && is_array($request['files']) ? $request['files'] : []);
    }

    public static function create($request = []): PageRequest
    {
        return new static($request);
    }

    public function dump(): array
    {
        return [
            'query' => $this->query->all(),
            'files' => $this->files->all(),
        ];
    }

    public function fromRequest(Request $request): PageRequest
    {
        $query = array_merge_recursive($request->query->all(), $request->request->all());

        array_walk_recursive( $query, function(&$value) {
           if(is_string($value)) {
               $value = htmlspecialchars($value);
               $value = urldecode($value);
           }
        });

        $this->query->add($query);
        $this->files->add($request->files->all());

        return $this;
    }
}