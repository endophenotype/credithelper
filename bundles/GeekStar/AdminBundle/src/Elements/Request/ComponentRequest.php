<?php

namespace GeekStar\AdminBundle\Elements\Request;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

class ComponentRequest
{
    public ParameterBag $query;
    public ParameterBag $data;
    public ParameterBag $files;

    public function __construct($request = [])
    {
        $this->query = new ParameterBag(isset($request['query']) && is_array($request['query']) ? $request['query'] : []);
        $this->files = new ParameterBag(isset($request['files']) && is_array($request['files']) ? $request['files'] : []);
        $this->data = new ParameterBag(isset($request['data']) && is_array($request['data']) ? $request['data'] : []);
    }

    public static function create($request = [])
    {
        return new static($request);
    }

    public function dump(): array
    {
        $request = [];

        $query = $this->query->all();
        unset($query['events']);
        $request['query'] = $query;

        $request['data'] = $this->data->all();

        return $request;
    }

    public function fromRequest(Request $request): ComponentRequest
    {
        $query = array_merge_recursive($request->query->all(), $request->request->all());

        array_walk_recursive( $query, function(&$value) {
           if(is_string($value)) {
               $value = htmlspecialchars($value);
           }
        });

        $this->query->add($query);
        $this->files->add($request->files->all());

        return $this;
    }

    public function fromPageRequest(PageRequest $request): ComponentRequest
    {
        $this->query->add($request->query->all());
        $this->files->add($request->files->all());

        return $this;
    }
}