<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Request\PageRequest;
use GeekStar\AdminBundle\Elements\Response\PageResponse;
use GeekStar\AdminBundle\Elements\User\UserInterface;
use GeekStar\AdminBundle\Exception\ElementException;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Page implements PageInterface
{
    protected ContainerInterface $container;

    protected ModuleInterface $module;

    protected bool $visible = true;
    protected bool $withSearch = false;

    protected int $sortIndex = 0;

    protected string $name;
    protected string $title;
    protected string $permission;
    protected ?string $content = null;

    protected PageRequest $request;
    protected PageResponse $response;
    protected ComponentRequest $componentRequest;

    protected array $contentComponents = [];
    protected array $components = [];

    abstract public function configure();
    abstract public function initialize();

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function setContainer(ContainerInterface $container): Page
    {
        $this->container = $container;

        return $this;
    }

    public function getModule(): ModuleInterface
    {
        return $this->module;
    }

    public function setModule(ModuleInterface $module): Page
    {
        $this->module = $module;

        return $this;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }

    public function setVisible(bool $visible): Page
    {
        $this->visible = $visible;

        return $this;
    }

    public function isWithSearch(): bool
    {
        return $this->withSearch;
    }

    public function setWithSearch(bool $withSearch): Page
    {
        $this->withSearch = $withSearch;

        return $this;
    }

    public function getSortIndex(): int
    {
        return $this->sortIndex;
    }

    public function setSortIndex(int $sortIndex): Page
    {
        $this->sortIndex = $sortIndex;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Page
    {
        $this->name = $name;

        return $this;
    }

    public function getUri(): string
    {
        if(!isset($this->module)) {
            throw ElementException::create($this->getContainer()->get('translator')->trans('geek_star_admin.alert.module_not_set'));
        }

        return $this->module->getUri() . '/' . $this->getUrn();
    }

    public function getUrn(): string
    {
        return lcfirst($this->getName());
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): Page
    {
        $this->title = $title;

        return $this;
    }

    public function getPermission(): string
    {
        return $this->permission;
    }

    public function setPermission(string $permission): Page
    {
        $this->permission = $permission;

        return $this;
    }

    public function checkPermission(UserInterface $user): bool
    {
        if($this->permission) {
            return $this->container->get('security.authorization_checker')->isGranted($this->permission);
        }

        return true;
    }

    public function getContent(bool $isUpdate = false): string
    {
        if($isUpdate or $this->content === null) {
            $this->preRender(true);
            $this->render(true);
            $this->postRender(true);
        }

        return $this->content;
    }

    public function setContent(string $content): Page
    {
        $this->content = $content;

        return $this;
    }

    public function getRequest(): PageRequest
    {
        if(!isset($this->request)) {
            $this->request = new PageRequest();
        }

        return $this->request;
    }

    public function setRequest(PageRequest $request): Page
    {
        $this->request = $request;

        return $this;
    }

    public function getResponse(): PageResponse
    {
        if(!isset($this->response)) {
            $this->response = new PageResponse();
            $this->response->content = $this->getContent();

            $components = [];
            foreach ($this->getContentComponents() as $component) {
                $components[$component->getKey()] = $component->getResponse()->dump();
            }

            $this->response->data->set('components', $components);
        }

        return $this->response;
    }

    public function setResponse(PageResponse $response): Page
    {
        $this->response = $response;

        return $this;
    }

    public function getComponentRequest(): ComponentRequest
    {
        if(!isset($this->componentRequest)) {
            $this->componentRequest = ComponentRequest::create()->fromPageRequest($this->getRequest());
        }

        return $this->componentRequest;
    }

    public function setComponentRequest(ComponentRequest $componentRequest): Page
    {
        $this->componentRequest = $componentRequest;

        return $this;
    }

    public function addComponent(ComponentInterface $component, ?string $key = null, bool $isRoot = true): PageInterface
    {
        if(!($key === null)) {
            $this->components[$key] = $component;
        } else {
            $this->components[] = $component;
            end($this->components);
            $key = (string)key($this->components);
        }

        if($isRoot) {
            $component
                ->create()
                ->setRequest($this->getComponentRequest())
                ->setParent($this)
                ->register($this);
        }

        $component->setKey($key);

        if($isRoot) {
            $component->configure();
            $component->initialize($component->getRequest(), $component->getResponse());
        }

        return $this;
    }

    public function getComponent(string $key): ?ComponentInterface
    {
        return $this->components[$key] ?? null;
    }

    /** @return ComponentInterface[] */
    public function getComponents(): array
    {
        return $this->components;
    }

    public function addContentComponent($component, string $key = null): ComponentInterface
    {
        $isNew = false;

        if(is_string($component)) {
            $isNew = true;
            $component = $this->container->get($component);
        }

        if(!($component instanceof ComponentInterface)) {
            throw new ElementException('Component not implement ComponentInterface');
        } else if($isNew) {
            $component = clone $component;
        }

        $this->addComponent($component, $key);
        $this->contentComponents[$component->getKey()] = $component;

        return $component;
    }

    public function getContentComponent(string $key): ComponentInterface
    {
        return $this->contentComponents[$key];
    }

    public function getContentComponents(): array
    {
        return $this->contentComponents;
    }

    public function preRender(bool $isUpdate = false)
    {
    }

    public function render(bool $isUpdate = false): Page
    {
        if($isUpdate) {
            $content = '';
            foreach ($this->getContentComponents() as $key => $item) {
                $item->handle('load', $item->getRequest(), $item->getResponse());

                $content .= '<div class="js-component" data-key="' . $item->getKey() . '"></div>';
            }

            $this->setContent($content);
        }

        return $this;
    }

    public function postRender(bool $isUpdate = false)
    {
    }
}