<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\User\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

interface ModuleInterface
{
    function configure();
    function initialize();

    function getContainer(): ContainerInterface;
    function setContainer(ContainerInterface $container): ModuleInterface;

    function isVisible(): bool;
    function setVisible(bool $visible): ModuleInterface;

    function getSortIndex(): int;
    function setSortIndex(int $sortIndex): ModuleInterface;

    function getIcon(): string;
    function setIcon(string $icon): ModuleInterface;

    function getName(): string;
    function setName(string $name): ModuleInterface;

    function getUrn(): string;
    function getUri(): string;

    function getTitle(): string;
    function setTitle(string $title): ModuleInterface;

    function getCategory(): CategoryInterface;
    function setCategory(CategoryInterface $category): ModuleInterface;

    function getPermission(): string;
    function setPermission(string $permission): ModuleInterface;
    function checkPermission(UserInterface $user): bool;

    /**
     * @param string|PageInterface $page
     * @param bool $isDefault
     * @return ModuleInterface
     */
    function addPage($page, bool $isDefault): ModuleInterface;
    function getPage(string $urn): ?PageInterface;

    /**
     * @return PageInterface[]
     */
    function getPages(): array;
    function sortPages();

    function getDefaultPage(): ?PageInterface;
    function setDefaultPage(PageInterface $page);
}