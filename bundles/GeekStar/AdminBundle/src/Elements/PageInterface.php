<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Request\PageRequest;
use GeekStar\AdminBundle\Elements\Response\PageResponse;
use GeekStar\AdminBundle\Elements\User\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

interface PageInterface
{
    function configure();
    function initialize();

    function getContainer(): ContainerInterface;
    function setContainer(ContainerInterface $container): PageInterface;

    function getModule(): ModuleInterface;
    function setModule(ModuleInterface $module): PageInterface;

    function isVisible(): bool;
    function setVisible(bool $visible): PageInterface;

    function isWithSearch(): bool;
    function setWithSearch(bool $withSearch): PageInterface;

    function getSortIndex(): int;
    function setSortIndex(int $sortIndex): PageInterface;

    function getName(): string;
    function setName(string $name): PageInterface;

    function getUrn(): string;
    function getUri(): string;

    function getTitle(): string;
    function setTitle(string $title): PageInterface;

    function getPermission(): string;
    function setPermission(string $permission): PageInterface;
    function checkPermission(UserInterface $user): bool;

    function getContent(bool $isUpdate = false): string;
    function setContent(string $content): PageInterface;

    function getRequest(): PageRequest;
    function setRequest(PageRequest $request): PageInterface;
    function getResponse(): PageResponse;
    function setResponse(PageResponse $response): PageInterface;
    function getComponentRequest(): ComponentRequest;
    function setComponentRequest(ComponentRequest $componentRequest): PageInterface;

    function addComponent(ComponentInterface $component, ?string $key = null, bool $isRoot = true): PageInterface;
    function getComponent(string $key): ?ComponentInterface;
    /** @return ComponentInterface[] */
    function getComponents(): array;

    /**
     * @param string|ComponentInterface $component
     * @param string|null $key
     * @return ComponentInterface
     */
    function addContentComponent($component, string $key = null): ComponentInterface;
    function getContentComponent(string $key): ?ComponentInterface;
    /** @return ComponentInterface[] */
    function getContentComponents(): array;
}