<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\AdminBundle\Service\Api\FormComponentValidator;

interface FormComponentInterface extends ComponentInterface
{
    /**
     * @param string|FieldInterface $field
     * @param string|null $key
     * @param ComponentInterface|null $parent
     * @return FormComponentInterface
     */
    function addField($field, ?string $key = null, ?ComponentInterface $parent = null): FormComponentInterface;
    function getField(string $key): ?FieldInterface;
    /** @return FieldInterface[]|null */
    function getFields(): array;

    function validate(ComponentRequest $request, ComponentResponse $response, array $formData, FormComponentValidator $validator);
    function preSave(ComponentRequest $request, ComponentResponse $response, array $formData);
    function save(ComponentRequest $request, ComponentResponse $response, array $formData);
    function postSave(ComponentRequest $request, ComponentResponse $response, array $formData);

    function submitHandler(ComponentRequest $request, ComponentResponse $response);
}