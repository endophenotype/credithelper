<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\AdminBundle\Model\FileListItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class FileListField extends Field
{
    const IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'ico', 'tif', 'svg'];

    /** @var FileListItem[] */
    public ?array $files = null;
    public SerializerInterface $serializer;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, SerializerInterface $serializer)
    {
        parent::__construct($container, $twig, $translator);

        $this->serializer = $serializer;
    }

    public function create(): FileListField
    {
        parent::create();

        $this
            ->setSingleLoad(true)
            ->setSortable(true)
            ->setWithOutUpload(false)
            ->setWithOutControls(false)
            ->setImgExtensions(self::IMAGE_EXTENSIONS);

        $this->config->set('translate', [
            'add_files' => $this->translator->trans('geek_star_admin.content.add_files'),
            'drag_and_drop_files_to_upload' => $this->translator->trans('geek_star_admin.content.drag_and_drop_files_to_upload'),
            'delete_file' => $this->translator->trans('geek_star_admin.content.delete_file') . '?',
        ]);

        $this->addAction('default', '() => {
            let field = this;
            let $field = $(this);
            let $uploader = $field.find(".js-files-uploader");
            
            $uploader.gsFilesUploader({
                "singleLoad" : this.config.singleLoad,
                "sortable": this.config.sortable,
                "withOutUpload": this.config.withOutUpload,
                "withOutControls": this.config.withOutControls,
                "imgExtensions": this.config.imgExtensions,
                "translate": this.config.translate,
                "onUpload": function(item) {
                    field.actions.uploadFile(item);
                }
            });
            
            $.each(field.data.files, function(key, item) {
                $uploader.gsFilesUploader("addItem", $.extend({}, item));
            });
        }');

        $this->addAction('serialize', '() => {
            let field = this;
            let $field = $(this);
            let $uploader = $field.find(".js-files-uploader");
            
            let initFiles = {};
            let notRemoveInitFiles = [];
            let fieldFiles = $uploader[0].items;
            
            $.each(this.data.files, function(key, item) {
                initFiles[item.key] = {
                    "title": item.title,
                    "sortIndex": item.sortIndex
                }
            });
            
            $.each(fieldFiles, function(key, item) {
                if(item !== undefined) {
                    if(item.data.key) {
                        notRemoveInitFiles.push(item.data.key);
                        
                        if(initFiles[item.data.key].title !== item.data.title) {
                            field.form.formData.set("formData[" + field.data.key + "][changeFiles][" + item.data.key + "][title]", item.data.title);
                        }      
                                          
                        if(initFiles[item.data.key].sortIndex !== item.data.sortIndex) {
                            field.form.formData.set("formData[" + field.data.key + "][changeFiles][" + item.data.key + "][sortIndex]", item.data.sortIndex);
                        }
                    } else {
                        $.each(item.data, function(key, value) {
                            if(key === "preview" || key === "index") {
                                return;
                            }
                            
                            field.form.formData.set("formData[" + field.data.key + "][uploadFiles][" + item.data.index + "][" + key + "]", value);
                        });
                    }
                }
            });
            
            $.each(initFiles, function(key, item) {
                key = key + "";
                if(notRemoveInitFiles.indexOf(key) === -1)
                {  
                    field.form.formData.set("formData[" + field.data.key + "][changeFiles][" + key + "][delete]", true);
                }
            });
        }');

        $this->addAction('reset', '() => {
            let $uploader = $(this).find(".js-files-uploader");
            $uploader.gsFilesUploader("reset");
        }');

        $this->addAction('uploadFile', '(item) => {
            let data = new FormData();
            
            $.each(item, function(key, value) {
                if(key === "preview") {
                    return;
                }
                data.set("file[" + key + "]", value);
            });
            
            this.events.uploadFile(data);
        }');

        $this->addEvent('uploadFile', '(response) => {
            let $uploader = $(this).find(".js-files-uploader");
            
            function isset(element, path) {
                try {
                    let current = element;
                    path.split(".").forEach(function(key){ current = current[key]; });
                    return current !== undefined && current !== null;
                } catch (e) {
                    return false;
                }
            }

            if( !isset(response, "error") && isset(response, "query.file.index") && isset(response, "data.file") ) {
                $uploader.gsFilesUploader("successLoad", $.extend({}, response.data.file), response.query.file.index);
            } else {
                console.log("Bad response");
            }
        }');

        $this->addHandler('uploadFile', [$this, 'uploadFileHandler']);

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function isSingleLoad(): ?bool
    {
        return $this->config->get('singleLoad');
    }

    public function setSingleLoad(bool $singleLoad): FileListField
    {
        $this->config->set('singleLoad', $singleLoad);

        return $this;
    }

    public function isSortable(): ?bool
    {
        return $this->config->get('sortable');
    }

    public function setSortable(bool $sortable): FileListField
    {
        $this->config->set('sortable', $sortable);

        return $this;
    }

    public function isWithOutUpload(): ?bool
    {
        return $this->config->get('withOutUpload');
    }

    public function setWithOutUpload(bool $withOutUpload): FileListField
    {
        $this->config->set('withOutUpload', $withOutUpload);

        return $this;
    }

    public function isWithOutControls(): ?bool
    {
        return $this->config->get('withOutControls');
    }

    public function setWithOutControls(bool $withOutControls): FileListField
    {
        $this->config->set('withOutControls', $withOutControls);

        return $this;
    }

    public function getImgExtensions(): array
    {
        return $this->config->get('imgExtensions');
    }

    public function setImgExtensions(array $imgExtensions): FileListField
    {
        $this->config->set('imgExtensions', $imgExtensions);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getFile(string $key): ?FileListItem
    {
        return $this->files[$key] ?? null;
    }

    public function addFile(FileListItem $file): FileListField
    {
        $this->files[$file->getKey()] = $file;

        return $this;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    /*
     * ===== end parameters =====
     */

    public function uploadFileHandler(ComponentRequest $request, ComponentResponse $response)
    {
        $file = $this->uploadFile($request, $response);
        if($file) {
            $data = $this->serializer->normalize($file);
            $response->data->set('file', $data);
        }
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): FileListField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/file_list.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }

    public function loadData(ComponentRequest $request, ComponentResponse $response): FileListField
    {
        $data = $this->serializer->normalize($this->files);
        $this->data->set('files', $data);

        return $this;
    }

    public function uploadFile(ComponentRequest $request, ComponentResponse $response): ?FileListItem
    {
        return null;
    }
}