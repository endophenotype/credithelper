<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RangeField extends Field
{
    const RANGE_TYPE_MIN = 'min';
    const RANGE_TYPE_MAX = 'max';
    const RANGE_TYPE_RANGE = 'range';

    const THEME_PRIMARY = 'primary';
    const THEME_SECONDARY = 'secondary';
    const THEME_SUCCESS = 'success';
    const THEME_DANGER = 'danger';
    const THEME_WARNING = 'warning';
    const THEME_INFO = 'info';
    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';
    const THEME_WHITE = 'white';
    const THEME_TRANSPARENT = 'transparent';

    public function create(): RangeField
    {
        parent::create();

        $this
            ->setRangeType('min')
            ->setValue(0)
            ->setMin(0)
            ->setMax(100);

        $this
            ->addAction('default', '() => {
                let field = this;
                let $slider = $(".js-slider", field);
                let slider = $slider[0];
                
                let options = {
                    min: field.config.min,
                    max: field.config.max,
                    slide: function(event, ui) {
                        field.actions.updateValue(this, ui);
                    }
                }
                
                if(field.config.step !== undefined) {
                    options.step = field.config.step;
                }
                
                if(field.config.rangeType === "min" || field.config.rangeType === "max") {
                    options.range = field.config.rangeType;
                    options.value = field.data.value;
                } else if(field.config.rangeType === "range") {
                    options.range = true;
                    options.values = field.data.values;
                }
                
                options.create = function(event, ui ) {
                    let slider = this;
                    slider.tooltips = [];
                    $(this).find(".ui-slider-handle").each(function( index ) {
                        slider.tooltips[index] = $(\'<div class="ui-slider-tooltip"></div>\');
                        slider.tooltips[index].text = $("<span></span>");
                        slider.tooltips[index].append(slider.tooltips[index].text);
                        $(this).append(slider.tooltips[index]);
                    });
                    
                    if(slider.tooltips.length === 1) {
                        slider.tooltips[0].input = $(\'<input type="hidden" name="\' + field.data.key + \'">\');
                        $(slider).append(slider.tooltips[0].input);
                    } else {
                        $.each(slider.tooltips, function(index){
                            slider.tooltips[index].input = $(\'<input type="hidden" name="\' + field.data.key + "[" + index + \']">\');
                            $(slider).append(slider.tooltips[index].input);
                        });
                    }
                    
                    field.actions.updateValue(slider, options);
                };
                
                $slider.slider(options);
            }');

        $this->addAction('updateValue', '(slider, data) => {
            if(slider.tooltips.length === 1) {
                slider.tooltips[0].input.val(data.value);
                slider.tooltips[0].text.html(data.value);
            } else {
                $.each(slider.tooltips, function(index){
                    slider.tooltips[index].input.val(data.values[index]);
                    slider.tooltips[index].text.html(data.values[index]);
                });
            }
        }');

        $this->addAction('reset', '() => {
            let $slider = $(".js-slider", this);
            let slider = $slider[0];
            
            if(this.config.rangeType === "range") {
                $.each(this.data.values, function(index){
                    $slider.slider("values", index, this);
                });
            } else {
                $slider.slider("value", this.data.value);
            }
            
            this.actions.updateValue(slider, this.data);
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTheme(): ?string
    {
        return $this->config->get('theme');
    }

    public function setTheme(string $theme): RangeField
    {
        $this->config->set('theme', $theme);

        return $this;
    }

    public function getMin(): ?int
    {
        return $this->config->get('min');
    }

    public function setMin(int $min): RangeField
    {
        $this->config->set('min', $min);

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->config->get('max');
    }

    public function setMax(int $max): RangeField
    {
        $this->config->set('max', $max);

        return $this;
    }

    public function getRangeType(): ?string
    {
        return $this->config->get('rangeType');
    }

    public function setRangeType(string $rangeType): RangeField
    {
        $this->config->set('rangeType', $rangeType);

        return $this;
    }

    public function getStep(): ?int
    {
        return $this->config->get('step');
    }

    public function setStep(int $step): RangeField
    {
        $this->config->set('step', $step);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getValue(): ?int
    {
        return $this->data->get('value');
    }

    public function setValue(int $value): RangeField
    {
        $this->data->set('value', $value);

        return $this;
    }

    public function getValues(): ?array
    {
        return $this->data->get('values');
    }

    public function setValues(array $values): RangeField
    {
        $this->data->set('values', $values);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): RangeField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/range.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}