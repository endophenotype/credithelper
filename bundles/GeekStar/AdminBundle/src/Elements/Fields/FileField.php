<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class FileField extends Field
{
    public function create(): FileField
    {
        parent::create();

        $this->setMultiple(false);

        $this->addAction('default', '() => {
            this.input = $(this).find("input")[0];
            this.isMultiple = $(this.input).attr("multiple");
        }');

        $this->addAction('serialize', '() => {
            let field = this;
            
            if(field.isMultiple === undefined) {
                if(field.input.files.length !== 0) {
                    field.form.formData.set("formData[" + field.data.key + "]", field.input.files[0]);
                }
            } else {
                $.each(field.input.files, function(key, file) {
                    field.form.formData.set("formData[" + field.data.key + "][" + key + "]", file);
                });
            }
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): FileField
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function isMultiple(): ?bool
    {
        return $this->config->get('multiple');
    }

    public function setMultiple(bool $multiple): FileField
    {
        $this->config->set('multiple', $multiple);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): FileField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/file.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}