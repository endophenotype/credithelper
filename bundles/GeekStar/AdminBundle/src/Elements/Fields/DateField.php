<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class DateField extends Field
{
    public function create(): DateField
    {
        parent::create();

        $this->setFormat('d-m-Y');

        $this->addAction('default', '() => {
            let input = $(this).find("input");
            let format = this.config.format;
            let options = {
                locale: "ru"
            };
            
            format = format.replace(/d/g,"DD");
            format = format.replace(/m/g,"MM");
            
            format = format.replace(/h/g,"hh");
            format = format.replace(/H/g,"HH");
            format = format.replace(/i/g,"mm");
            format = format.replace(/s/g,"ss");
            
            if((format.indexOf("hh") !== -1) || (format.indexOf("HH") !== -1) || (format.indexOf("mm") !== -1)) {
                options.sideBySide = true;
            }
            
            options.format = format;
        
            input.datetimepicker(options);
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): DateField
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getPlaceholder(): ?string
    {
        return $this->config->get('placeholder');
    }

    public function setPlaceholder(string $placeholder): DateField
    {
        $this->config->set('placeholder', $placeholder);

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->config->get('format');
    }

    public function setFormat(string $format): DateField
    {
        $this->config->set('format', $format);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getValue(): ?string
    {
        return $this->data->get('value');
    }

    public function setValue(\DateTime $value): DateField
    {
        $this->data->set('value', $value->format($this->getFormat()));

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): DateField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/date.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}