<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class CheckboxField extends Field
{
    public function create(): CheckboxField
    {
        parent::create();

        $this->setChecked(false);

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): CheckboxField
    {
        $this->config->set('title', $title);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getChecked(): ?bool
    {
        return $this->data->get('checked');
    }

    public function setChecked(bool $checked): CheckboxField
    {
        $this->data->set('checked', $checked);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): CheckboxField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/checkbox.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}