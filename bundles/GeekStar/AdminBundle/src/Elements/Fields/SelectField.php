<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class SelectField extends Field
{
    public function create(): SelectField
    {
        parent::create();

        $this->setOptions([]);

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): SelectField
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function addOption(string $value, string $title, bool $selected = false): SelectField
    {
        $options = $this->config->get('options') ?: [];

        $options[$value] = [
            'value' => $value,
            'title' => $title,
        ];

        $this->config->set('options', $options);

        if($selected) {
            $this->setValue($value);
        }

        return $this;
    }

    public function getOptions(): ?string
    {
        return $this->config->get('options');
    }

    public function setOptions(array $options): SelectField
    {
        $this->config->set('options', $options);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getValue(): ?string
    {
        return $this->data->get('value');
    }

    public function setValue(string $value): SelectField
    {
        $this->data->set('value', $value);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): SelectField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/select.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}