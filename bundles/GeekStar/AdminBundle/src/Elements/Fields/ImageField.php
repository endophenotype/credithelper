<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class ImageField extends Field
{
    public function create(): ImageField
    {
        parent::create();

        $this
            ->setUrl('')
            ->setPreview('');

        $this->addAction('default', '() => {
            let $field = $(this);
            let $preview = $field.find(".preview");
            let $previewImage = $preview.find("img");
            let $fileInput = $field.find("input[type=file]");
            
            this.actions.reset();
            
            $fileInput.on("change", function (e) {
                let file = this.files[0];
                $preview.show();
                $previewImage.attr("src", window.URL.createObjectURL(file));
            });
        }');

        $this->addAction('serialize', '() => {
            let $field = $(this);
            let fileInput = $field.find("input[type=file]")[0];
            let $fileInput = $(fileInput);
            let file = fileInput.files[0];
            
            if(file) {
                let name = $fileInput.attr("name");
            
                let pos = name.indexOf("[");
                let fieldKey = "", fieldNodes = "";
                if(pos !== -1) {
                    fieldKey = name.substr(0, pos);
                    fieldNodes = name.substr(pos);                    
                } else {
                    fieldKey = name;
                }
                
                this.form.formData.set("formData[" + fieldKey + "]" + fieldNodes, file);
            }
        }');

        $this->addAction('reset', '() => {
            let $field = $(this);
            let $preview = $field.find(".preview");
            let $previewImage = $preview.find("img");
            
            if(!this.config.url) {
                $preview.hide();
                $preview.on("click", function(e) {
                    e.preventDefault();
                });
            } else {
                $preview.show();
                $previewImage.attr("src", this.config.preview ? this.config.preview : this.config.url);
            }
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getUrl(): ?string
    {
        return $this->config->get('url');
    }

    public function setUrl(string $url): ImageField
    {
        $this->config->set('url', $url);

        return $this;
    }

    public function getPreview(): ?string
    {
        return $this->config->get('preview');
    }

    public function setPreview(string $preview): ImageField
    {
        $this->config->set('preview', $preview);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): ImageField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/image.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}