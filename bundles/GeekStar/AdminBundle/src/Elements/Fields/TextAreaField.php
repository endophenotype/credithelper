<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class TextAreaField extends Field
{
    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): TextAreaField
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getPlaceholder(): ?string
    {
        return $this->config->get('placeholder');
    }

    public function setPlaceholder(string $placeholder): TextAreaField
    {
        $this->config->set('placeholder', $placeholder);

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->config->get('height');
    }

    public function setHeight(string $height): TextAreaField
    {
        $this->config->set('height', $height);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getValue(): ?string
    {
        return $this->data->get('value');
    }

    public function setValue(string $value): TextAreaField
    {
        $this->data->set('value', $value);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): TextAreaField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/textarea.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}