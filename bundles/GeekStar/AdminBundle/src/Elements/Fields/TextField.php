<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class TextField extends Field
{
    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): TextField
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getType(): ?string
    {
        return $this->config->get('type');
    }

    public function setType(string $type): TextField
    {
        $this->config->set('type', $type);

        return $this;
    }

    public function getPlaceholder(): ?string
    {
        return $this->config->get('placeholder');
    }

    public function setPlaceholder(string $placeholder): TextField
    {
        $this->config->set('placeholder', $placeholder);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function setValue(string $value): TextField
    {
        $this->data->set('value', $value);

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->data->get('value');
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): TextField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/text.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}