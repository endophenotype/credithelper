<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\AdminBundle\Manager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class RichTextField extends Field
{
    protected Manager $manager;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, Manager $manager)
    {
        parent::__construct($container, $twig, $translator);

        $this->manager = $manager;
    }

    public function create(): RichTextField
    {
        parent::create();

        $this->addAction('default', '() => {
            let $textarea = $(this).find("textarea");
            let options = {
                language: "' . $this->manager->locale . '",
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "table emoticons template paste help"
                ],
                toolbar: 
                    "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | " +
                    "bullist numlist outdent indent | link image | print preview media fullpage | " +
                    "forecolor backcolor emoticons | help",
                menubar: "file edit view insert format tools table help",
                content_style: "body { font-family:Helvetica,Arial,sans-serif; font-size:14px}"
            }
            
            if(this.config.height !== undefined) {
                options.height = this.config.height;
            }
            
            $textarea.tinymce(options);
        }');

        $this->addAction('onDestroy', '() => {
            let $textarea = $(this).find("textarea");
            $textarea.tinymce().remove();
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): RichTextField
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->config->get('height');
    }

    public function setHeight(string $height): RichTextField
    {
        $this->config->set('height', $height);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getValue(): ?string
    {
        return $this->data->get('value');
    }

    public function setValue(string $value): RichTextField
    {
        $this->data->set('value', $value);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): RichTextField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/richtext.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}