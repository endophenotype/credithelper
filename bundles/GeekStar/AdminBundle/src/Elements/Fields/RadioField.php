<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class RadioField extends Field
{
    public function create(): RadioField
    {
        parent::create();

        $this->setChecked(false);

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getName(): ?string
    {
        return $this->config->get('name');
    }

    public function setName(string $name): RadioField
    {
        $this->config->set('name', $name);

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): RadioField
    {
        $this->config->set('title', $title);

        return $this;
    }

    /*
     * ===== data parameters =====
     */

    public function getValue(): ?string
    {
        return $this->data->get('value');
    }

    public function setValue(string $value): RadioField
    {
        $this->data->set('value', $value);

        return $this;
    }

    public function setChecked(bool $checked): RadioField
    {
        $this->data->set('checked', $checked);

        return $this;
    }

    public function getChecked(): ?bool
    {
        return $this->data->get('checked');
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): RadioField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/radio.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}