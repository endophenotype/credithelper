<?php

namespace GeekStar\AdminBundle\Elements\Fields;

use GeekStar\AdminBundle\Elements\Field;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;

class ButtonField extends Field
{
    const TYPE_BUTTON = 'button';
    const TYPE_SUBMIT = 'submit';
    const TYPE_RESET = 'reset';

    const THEME_PRIMARY = 'primary';
    const THEME_SECONDARY = 'secondary';
    const THEME_SUCCESS = 'success';
    const THEME_DANGER = 'danger';
    const THEME_WARNING = 'warning';
    const THEME_INFO = 'info';
    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';
    const THEME_WHITE = 'white';
    const THEME_TRANSPARENT = 'transparent';

    const THEME_OUTLINE_PRIMARY = 'outline-primary';
    const THEME_OUTLINE_SECONDARY = 'outline-secondary';
    const THEME_OUTLINE_SUCCESS = 'outline-success';
    const THEME_OUTLINE_DANGER = 'outline-danger';
    const THEME_OUTLINE_WARNING = 'outline-warning';
    const THEME_OUTLINE_INFO = 'outline-info';
    const THEME_OUTLINE_LIGHT = 'outline-light';
    const THEME_OUTLINE_DARK = 'outline-dark';
    const THEME_OUTLINE_WHITE = 'outline-white';

    const SIZE_SMALL = 'sm';
    const SIZE_LARGE = 'lg';

    public function create(): ButtonField
    {
        parent::create();

        $this
            ->setType(self::TYPE_BUTTON)
            ->setTheme(self::THEME_PRIMARY);

        $this->addAction('default', '() => {
            let buttonField = this;
            let $button = $(this).find("button");
            
            if (this.config.type == "button") {
                $button.on("click", function(e){
                    e.preventDefault();
                    buttonField.actions.click();
                });
                
            } else if (this.config.type == "submit") {
                $button.on("click", function(e){
                    e.preventDefault();
                    buttonField.actions.submit();
                });
                
            } else if (this.config.type == "reset") {
                $button.on("click", function(e){
                    buttonField.actions.formReset();
                });
            }
        }');

        $this->addAction('click', '() => {}');

        $this->addAction('submit', '() => {
            this.form.actions.submit();
        }');

        $this->addAction('formReset', '() => {
            this.form.actions.reset();
        }');

        return $this;
    }

    public function configure()
    {
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
    }

    /*
     * ===== configuration parameters =====
     */

    public function getType(): ?string
    {
        return $this->config->get('type');
    }

    public function setType(string $type): ButtonField
    {
        $this->config->set('type', $type);

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->config->get('title');
    }

    public function setTitle(string $title): ButtonField
    {
        $this->config->set('title', $title);

        return $this;
    }

    public function getTheme(): ?string
    {
        return $this->config->get('theme');
    }

    public function setTheme(string $theme): ButtonField
    {
        $this->config->set('theme', $theme);

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->config->get('size');
    }

    public function setSize(string $size): ButtonField
    {
        $this->config->set('size', $size);

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->config->get('icon');
    }

    public function setIcon(string $icon): ButtonField
    {
        $this->config->set('icon', $icon);

        return $this;
    }

    /*
     * ===== end parameters =====
     */

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): ButtonField
    {
        $content = $this->twig->render('@GeekStarAdmin/Field/button.field.twig', $response->dump());
        $this->setContent($content);

        return $this;
    }
}