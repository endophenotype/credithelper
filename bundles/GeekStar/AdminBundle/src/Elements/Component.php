<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Components\TemplateComponent;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\AdminBundle\Exception\ElementException;

abstract class Component extends PageBlock implements ComponentInterface
{
    /** @var PageInterface|ComponentInterface|null $parent */
    protected $parent = null;

    protected ?FormComponentInterface $formParent = null;

    protected ?PageInterface $currentPage = null;

    /** @var ComponentInterface[] $components */
    protected array $components = [];

    /** @var ComponentInterface[]|FieldInterface[] $contentItems */
    protected array $contentItems = [];

    public function create(): PageBlock
    {
        parent::create();

        $this->setNodes([]);

        $this->addEvent('load', '(response) => {
            let $window = $(window);
            let top = $(window).scrollTop();
            let root = this.root;
            let parent = this.parent;
            
            let $component = $(this);
            let newComponent = $(response.content).insertAfter($component)[0];
            
            this.actions.destroy();
            
            root.initComponent(newComponent, response, root, parent);
            root.renderComponent(newComponent);
            
            $(window).scrollTop(top);
            newComponent.actions.onLoad(response);
        }');

        $this->addAction('onLoad', '(response) => {
        }');

        return $this;
    }

    abstract public function configure();
    abstract public function initialize(ComponentRequest $request, ComponentResponse $response);

    public function load(ComponentRequest $request, ComponentResponse $response): Component
    {
        $componentsResponse = [];
        foreach ($this->getComponents() as $component) {
            $component->handle('load', $component->getRequest(), $component->getResponse());
            $componentsResponse[$component->getKey()] = $component->getResponse()->dump();
        }

        $response->data->set('components', $componentsResponse);

        parent::load($request, $response);

        return $this;
    }

    public function register(?PageInterface $page = null, bool $isAddToPage = false)
    {
        if($page) {
            $this->setCurrentPage($page);
        }

        if($isAddToPage and $page = $this->getCurrentPage()) {
            $page->addComponent($this, $this->getKey(), false);
        }
    }

    public function getKey(): ?string
    {
        return $this->config->get('_key');
    }

    public function setKey(string $key): Component
    {
        $this->config->set('_key', $key);

        return $this;
    }

    public function getNodes(): array
    {
        return $this->config->get('_nodes');
    }

    public function setNodes(array $nodes): Component
    {
        $this->config->set('_nodes', $nodes);

        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent): Component
    {
        $this->parent = $parent;

        $parentId = $this->getParent() instanceof PageInterface ? 'root' : $this->getParent()->getKey();
        $this->config->set('_parent', $parentId);

        return $this;
    }

    public function getParentForm(): ?FormComponentInterface
    {
        return $this->formParent;
    }

    public function setParentForm(?FormComponentInterface $formParent): Component
    {
        $this->formParent = $formParent;

        return $this;
    }

    public function getCurrentPage(): ?PageInterface
    {
        return $this->currentPage;
    }

    public function setCurrentPage(PageInterface $page): Component
    {
        $this->currentPage = $page;

        return $this;
    }

    public function addComponent($component, ?string $key = null): Component
    {
        $isNew = false;

        if(is_string($component)) {
            $isNew = true;
            $component = $this->container->get($component);
        }

        if(!($component instanceof ComponentInterface)) {
            throw new ElementException('Component not implement ComponentInterface');
        } else if($isNew) {
            $component = clone $component;
        }

        $component
            ->create()
            ->setRequest($this->getRequest())
            ->setParent($this);

        if($key != null) {
            $component->setKey($key);
        }

        $component->register($this->getCurrentPage(), true);

        if(!($component instanceof FormComponentInterface)) {
            $component->setParentForm($this->getParentForm());
        }

        $component->setNodes($this->getNodes() + [$this->getKey()]);

        $component->configure();
        $component->initialize($component->getRequest(), $component->getResponse());

        $this->components[$component->getKey()] = $component;

        return $this;
    }

    public function getComponent(string $key): ?ComponentInterface
    {
        return $this->components[$key] ?? null;
    }

    public function getComponents(): array
    {
        return $this->components;
    }

    public function addContentItem($item, ?string $key = null)
    {
        if(is_string($item) and class_exists($item)) {
            $item = $this->container->get($item);
            $item = clone $item;
        }

        switch(true) {
            case $item instanceof ComponentInterface:
                $this->addContentItemComponent($item, $key);
                break;
            case $item instanceof FieldInterface:
                $this->addContentItemField($item, $key);
                break;
            case is_string($item):
                $component = clone $this->container->get(TemplateComponent::class);
                $component->setTemplate($item);
                $item = $component;

                $this->addContentItemComponent($item, $key);
                break;
            default:
                throw new ElementException('Item of unknown format');
        }

        return $item;
    }

    protected function addContentItemComponent(ComponentInterface $component, ?string $key = null)
    {
        $this->addComponent($component, $key);
        $this->contentItems['component-' . $component->getKey()] = $component;
    }

    protected function addContentItemField(FieldInterface $field, ?string $key = null)
    {
        if($parentForm = $this->getParentForm()) {
            $parentForm->addField($field, $key, $this);
            $this->contentItems['field-' . $field->getKey()] = $field;
        }
    }

    public function end()
    {
        return $this->getParent();
    }

    public function renderContentItems(): string
    {
        $content = '';

        foreach ($this->contentItems as $item) {
            switch(true) {
                case $item instanceof ComponentInterface:
                    $content .= $this->renderContentComponentItem($item) . "\r\n";
                    break;
                case $item instanceof FieldInterface:
                    $content .= $this->renderContentFieldItem($item) . "\r\n";
                    break;
                default:
                    throw new ElementException('Item of unknown format');
            }
        }

        return $content;
    }

    public function renderContentComponentItem(ComponentInterface $item): string
    {
        return '<div class="js-component" data-key="' . $item->getKey() . '" data-nodes=' . json_encode($item->getNodes()) .'></div>';
    }

    public function renderContentFieldItem(FieldInterface $item): string
    {
        $item->handle('load', $item->getRequest(), $item->getResponse());

        return $item->getContent();
    }

    public function render(ComponentRequest $request, ComponentResponse $response, bool $isUpdate = false): Component
    {
        if($isUpdate or $this->getContent() === null) {
            $this->setContent($this->renderContentItems());
        }

        return $this;
    }
}