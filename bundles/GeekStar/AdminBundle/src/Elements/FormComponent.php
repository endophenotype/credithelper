<?php

namespace GeekStar\AdminBundle\Elements;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\AdminBundle\Exception\ElementException;
use GeekStar\AdminBundle\Service\Api\FormComponentValidator;

abstract class FormComponent extends Component implements FormComponentInterface
{
    /** @var FieldInterface[] $fields */
    private array $fields = [];

    public function create(): FormComponent
    {
        parent::create();

        $this->setParentForm($this);

        $this->addAction('default', '() => {
            let component = this;
            if(component.data.fields !== undefined && component.data.fields.length !== 0) {
                component.fields = {};
                $(".js-component-field", component).each(function() {
                    let fieldKey = $(this).data("key");
                    if(component.data.fields[fieldKey] !== undefined) {
                        component.root.initField(this, component.data.fields[fieldKey], component);
                    }
                });
            }
        }');

        $this->addAction('reset', '() => {
            $.each(this.fields, function(index, field){
                $(field).removeClass("error");
                field.actions.reset();
            });
        }');

        $this->addAction('serialize', '() => {
            let component = this;
            let $form = $(this).find("form");
            
            this.formData = new FormData();
            let formOptions = $form.serializeArray();
            
            $(formOptions).each(function(index, obj){
                let pos = obj.name.indexOf("[");
                let fieldKey = "", fieldNodes = "";
                if(pos !== -1) {
                    fieldKey = obj.name.substr(0, pos);
                    fieldNodes = obj.name.substr(pos);                    
                } else {
                    fieldKey = obj.name;
                }
                
                component.formData.set("formData[" + fieldKey + "]" + fieldNodes, obj.value);
            });
            
            $.each(this.fields, function(index, field){
                field.actions.serialize();
            });
        }');

        $this->addAction('submit', '() => {
            $.each(this.fields, function(index, field){
                $(field).removeClass("error");
            });
            
            this.actions.serialize();
            this.events.submit(this.formData);
        }');

        $this->addEvent('submit', '(response) => {
            if(response.error === undefined) {
                this.actions.onSubmit();
                this.actions.load();
            }
        }');

        $this->addAction('onSubmit', '() => {
        }');

        $this->addHandler('submit', [$this, 'submitHandler']);

        return $this;
    }

    public function addField($field, ?string $key = null, ?ComponentInterface $parent = null): FormComponent
    {
        $isNew = false;

        if(is_string($field)) {
            $isNew = true;
            $field = $this->container->get($field);
        }

        if(!($field instanceof FieldInterface)) {
            throw new ElementException('Field not implement FieldInterface');
        } else if($isNew) {
            $field = clone $field;
        }

        if(!($key === null)) {
            $this->fields[$key] = $field;
        } else {
            $this->fields[] = $field;
            end($this->fields);
            $key = (string)key($this->fields);
        }

        $field
            ->create()
            ->setRequest($this->getRequest())
            ->setKey($key)
            ->setParentForm($this);

        if($parent !== null) {
            $field->setParent($parent);
        }

        $field->configure();
        $field->initialize($field->getRequest(), $field->getResponse());

        return $this;
    }

    function getField(string $key): ?FieldInterface
    {
        return $this->fields[$key] ?? null;
    }

    function getFields(): array
    {
        return $this->fields;
    }

    public function load(ComponentRequest $request, ComponentResponse $response): FormComponent
    {
        $fields = [];
        foreach ($this->getFields() as $field) {
            $field->handle('load', $field->getRequest(), $field->getResponse());

            $fields[$field->getKey()] = $field->getResponse()->dump();
        }

        $response->fields = $fields;

        parent::load($request, $response);

        return $this;
    }

    public function submitHandler(ComponentRequest $request, ComponentResponse $response)
    {
        $validator = new FormComponentValidator($this->getContainer(), $request, $response);

        $formData = [];
        $formData['query'] = $request->query->get('formData') ?? [];
        $formData['files'] = $request->files->get('formData') ?? [];

        $this->validate($request, $response, $formData, $validator);
        $this->preSave($request, $response, $formData);
        $this->save($request, $response, $formData);
        $this->postSave($request, $response, $formData);
    }

    public function validate(ComponentRequest $request, ComponentResponse $response, array $formData, FormComponentValidator $validator)
    {
    }

    public function preSave(ComponentRequest $request, ComponentResponse $response, array $formData)
    {
    }

    public function save(ComponentRequest $request, ComponentResponse $response, array $formData)
    {
    }

    public function postSave(ComponentRequest $request, ComponentResponse $response, array $formData)
    {
    }
}