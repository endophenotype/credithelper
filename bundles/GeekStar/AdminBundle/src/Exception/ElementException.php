<?php

namespace GeekStar\AdminBundle\Exception;

use GeekStar\AdminBundle\Util\Exception\AbstractException;

class ElementException extends AbstractException
{
}