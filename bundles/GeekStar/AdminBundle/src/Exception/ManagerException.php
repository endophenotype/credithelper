<?php

namespace GeekStar\AdminBundle\Exception;

use GeekStar\AdminBundle\Util\Exception\AbstractException;

class ManagerException extends AbstractException
{
}