<?php

namespace GeekStar\AdminBundle\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class NotFoundException extends NotFoundHttpException
{
    public static function create($message = "", $code = 0, Throwable $previous = null): NotFoundException
    {
        return new static($message, $previous, $code);
    }
}