<?php

namespace GeekStar\AdminBundle\Exception;

use GeekStar\AdminBundle\Model\ApiError;
use GeekStar\AdminBundle\Util\Exception\AbstractException;

class ApiException extends AbstractException
{
    protected ApiError $restError;

    public function getApiError(): ApiError
    {
        return $this->restError;
    }

    public function setApiError(ApiError $restError): ApiException
    {
        $this->restError = $restError;
        return $this;
    }
}