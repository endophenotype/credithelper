<?php

namespace GeekStar\AdminBundle\Exception;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Throwable;

class AccessDeniedException extends AccessDeniedHttpException
{
    public static function create($message = "", $code = 0, Throwable $previous = null): AccessDeniedException
    {
        return new static($message, $previous, $code);
    }
}