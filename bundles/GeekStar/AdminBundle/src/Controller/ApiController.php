<?php

namespace GeekStar\AdminBundle\Controller;

use GeekStar\AdminBundle\Elements\Request\PageRequest;
use GeekStar\AdminBundle\Exception\NotFoundException;
use GeekStar\AdminBundle\Manager;
use GeekStar\AdminBundle\Service\Api\ApiHandler;
use GeekStar\AdminBundle\Service\PageService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    /**
     * Получить csrf_token
     *
     * @Route("/csrf_token", name="geek_star_admin_api_csrf_token", methods={"POST"})
     *
     * @param ApiHandler $handler
     * @param CsrfTokenManagerInterface $csrfTokenManager
     * @return Response
     */
    public function csrf_token(ApiHandler $handler, CsrfTokenManagerInterface $csrfTokenManager): Response
    {
        return $handler->response([
            'token' => $csrfTokenManager->getToken('geek_star_admin_api')->getValue()
        ]);
    }

    /**
     * Авторизации
     *
     * @Route("/login", name="geek_star_admin_api_login", methods={"POST"})
     *
     * @param Manager $manager
     * @param Request $request
     * @param ApiHandler $handler
     * @param UserProviderInterface $userProvider
     * @param TranslatorInterface $translator
     * @return Response
     * @throws \GeekStar\AdminBundle\Exception\ApiException
     */
    public function login(
        Manager $manager,
        Request $request,
        ApiHandler $handler,
        UserProviderInterface $userProvider,
        TranslatorInterface $translator
    ): Response
    {
        $handler->fromRequest($request);
        $handler->validate([
            'request' => [
                'username' => [new NotBlank(), new Email()],
                'password' => [new NotBlank()],
            ]
        ]);

        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $rememberMe = $request->request->get('remember_me') ? true : false;

        $user = $manager->userManager->getUserByUsernamePassword($username, $password);

        if(!$user) {
            $handler->error->send($translator->trans('geek_star_admin.alert.invalid_login_password'));
        }

        $firewall = $manager->config['security']['firewall'] ?? 'main';
        $token = $manager->securityService->authorizeUser($user, $firewall);
        $response = $handler->response(['success' => true]);

        if($rememberMe) {
            $manager->securityService->rememberMe($userProvider, $request, $response, $token, $firewall);
        }

        return $response;
    }

    /**
     * Выход из системы
     *
     * @Route("/logout", name="geek_star_admin_api_logout", methods={"POST"})
     *
     * @param Manager $manager
     * @param ApiHandler $handler
     * @param Request $request
     * @return Response
     */
    public function logout(
        Manager $manager,
        ApiHandler $handler,
        Request $request
    ): Response
    {
        $firewall = $manager->config['security']['firewall'] ?? 'main';
        $response = $handler->response(['success' => true]);

        $manager->securityService->logout($request, $response, $firewall);

        return $response;
    }

    /**
     * Получить страницу
     *
     * @Route("/page", name="geek_star_admin_api_page", methods={"POST"})
     *
     * @param Request $request
     * @param ApiHandler $handler
     * @param PageService $pageService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function page(Request $request, ApiHandler $handler, PageService $pageService, TranslatorInterface $translator): Response
    {
        $handler->fromRequest($request);
        $handler->validate([
            'request' => [
                'category' => [new NotBlank()],
                'module' => [new NotBlank()],
                'page' => [new NotBlank()],
            ]
        ]);

        $category = $request->request->get('category');
        $module = $request->request->get('module');
        $page = $request->request->get('page');

        $result = null;

        $page = $pageService->getPage($category, $module, $page, $this->getUser());
        $pageRequest = PageRequest::create()->fromRequest($request);

        $page->setRequest($pageRequest);
        $page->initialize();

        $result = $page->getResponse()->dump();

        return $handler->response($result);
    }

    /**
     * Получить компонент
     *
     * @Route("/component", name="geek_star_admin_api_component", methods={"POST"})
     *
     * @param Request $request
     * @param ApiHandler $handler
     * @param PageService $pageService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function component(Request $request, ApiHandler $handler, PageService $pageService, TranslatorInterface $translator): Response
    {
        $handler->fromRequest($request);
        $handler->validate([
            'request' => [
                'category' => [new NotBlank()],
                'module' => [new NotBlank()],
                'page' => [new NotBlank()],
                'component' => [new NotBlank()],
            ]
        ]);

        $category = $request->request->get('category');
        $module = $request->request->get('module');
        $page = $request->request->get('page');
        $componentKey = $request->request->get('component');

        $component = null;
        $result = null;

        try {
            $page = $pageService->getPage($category, $module, $page, $this->getUser());
            $pageRequest = PageRequest::create()->fromRequest($request);

            $component = $pageService->getComponent($page, $componentKey, $pageRequest);

            $events = $component->getRequest()->query->get('events') ?? [];
            $component->handleEvents($events, $component->getRequest(), $component->getResponse());

            $result = $component->getResponse()->dump();

        } catch (NotFoundException $e) {
            $handler->error->notFound($translator->trans('geek_star_admin.alert.component_not_found'));
        }

        return $handler->response($result);
    }

    /**
     * Получить поле
     *
     * @Route("/field", name="geek_star_admin_api_field", methods={"POST"})
     *
     * @param Request $request
     * @param ApiHandler $handler
     * @param PageService $pageService
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function field(Request $request, ApiHandler $handler, PageService $pageService, TranslatorInterface $translator): Response
    {
        $handler->fromRequest($request);
        $handler->validate([
            'request' => [
                'category' => [new NotBlank()],
                'module' => [new NotBlank()],
                'page' => [new NotBlank()],
                'component' => [new NotBlank()],
                'field' => [new NotBlank()],
            ]
        ]);

        $category = $request->request->get('category');
        $module = $request->request->get('module');
        $page = $request->request->get('page');
        $componentKey = $request->request->get('component');
        $fieldKey = $request->request->get('field');

        $component = null;
        $field = null;
        $result = null;

        try {
            $page = $pageService->getPage($category, $module, $page, $this->getUser());

            $pageRequest = PageRequest::create()->fromRequest($request);

            $field = $pageService->getField($page, $componentKey, $fieldKey, $pageRequest);

            $events = $field->getRequest()->query->get('events') ?? [];
            $field->handleEvents($events, $field->getRequest(), $field->getResponse());
            $result = $field->getResponse()->dump();

        } catch (NotFoundException $e) {
            $handler->error->notFound($translator->trans('geek_star_admin.alert.field_not_found'));
        }

        return $handler->response($result);
    }
}