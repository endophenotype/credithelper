<?php

namespace GeekStar\AdminBundle\Controller;

use GeekStar\AdminBundle\Exception\AccessDeniedException;
use GeekStar\AdminBundle\Exception\NotFoundException;
use GeekStar\AdminBundle\Manager;
use GeekStar\AdminBundle\Service\PageService;
use GeekStar\AdminBundle\Elements\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DefaultController extends AbstractController
{
    /**
     * Главная страница
     *
     * @Route("/", name="geek_star_admin_index", methods={"GET"})
     *
     * @param Manager $manager
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function indexAction(Manager $manager, TranslatorInterface $translator): Response
    {
        $user = $this->getUser();

        try {
            if ($user instanceof UserInterface) {
                $defaultPage = $manager->getDefaultPage($user);
            } else {
                throw $this->createAccessDeniedException();
            }
        } catch (AccessDeniedException $e) {
            throw $this->createAccessDeniedException($translator->trans('geek_star_admin.alert.access_denied'));
        } catch (NotFoundException $e) {
            throw $this->createNotFoundException($translator->trans('geek_star_admin.alert.page_not_found'));
        }

        $url = $this->generateUrl('geek_star_admin_index') . $defaultPage->getUri();

        return $this->redirect($url);
    }

    /**
     * Страница авторизации
     *
     * @Route("/login/", name="geek_star_admin_login", methods={"GET"})
     *
     * @return Response
     * @throws \Exception
     */
    public function login(): Response
    {
        return $this->render('@GeekStarAdmin/Default/authorize.page.twig');
    }

    /**
     * Страница проверки авторизации
     *
     * @Route("/logi-check/", name="geek_star_admin_login_check", methods={"GET"})
     *
     * @return Response
     * @throws \Exception
     */
    public function loginCheck(): Response
    {
        return $this->redirectToRoute('geek_star_admin_login');
    }

    /**
     * Страница авторизации
     *
     * @Route("/logout/", name="geek_star_admin_logout", methods={"GET"})
     *
     * @param Manager $manager
     * @param Request $request
     * @return Response
     */
    public function logout(Manager $manager, Request $request): Response
    {
        $firewall = $manager->config['security']['firewall'] ?? 'main';
        $response = $this->redirectToRoute('geek_star_admin_login');

        $manager->securityService->logout($request, $response, $firewall);

        return $response;
    }

    /**
     * Страница
     *
     * @Route("/{category}/{module}/{page}", name="geek_star_admin_page", methods={"GET"})
     *
     * @param Request $request
     * @param PageService $pageService
     * @param TranslatorInterface $translator
     * @param string $category
     * @param string $module
     * @param string $page
     * @return Response
     */
    public function getPage(
        PageService $pageService,
        TranslatorInterface $translator,
        string $category,
        string $module,
        string $page
    ): Response
    {
        try {
            $page = $pageService->getPage($category, $module, $page, $this->getUser());

        } catch (AccessDeniedException $e) {
            throw $this->createAccessDeniedException($translator->trans('geek_star_admin.alert.access_denied'));
        } catch (NotFoundException $e) {
            throw $this->createNotFoundException($translator->trans('geek_star_admin.alert.page_not_found'));
        }

        return $this->render('@GeekStarAdmin/Default/page.page.twig', ['this' => $page]);
    }
}