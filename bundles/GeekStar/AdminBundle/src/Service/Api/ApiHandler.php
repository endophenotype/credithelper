<?php

namespace GeekStar\AdminBundle\Service\Api;

use GeekStar\AdminBundle\Model\ApiError;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApiHandler
{
    protected ContainerInterface $container;
    protected ValidatorInterface $validator;
    protected TranslatorInterface $translator;
    public Request $request;
    public ApiError $error;
    public $data;

    public function __construct(ContainerInterface $container, ValidatorInterface $validator, TranslatorInterface $translator)
    {
        $this->container = $container;
        $this->validator = $validator;
        $this->translator = $translator;
        $this->error = new ApiError();
    }

    public function fromRequest(Request $request): ApiHandler
    {
        $this->setRequest($request);
        $this->checkCsrfToken();

        return $this;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function setRequest(Request $request): ApiHandler
    {
        $this->request = $request;

        return $this;
    }

    public function setData($data): ApiHandler
    {
        $this->data = $data;

        return $this;
    }

    public function checkCsrfToken($id = 'geek_star_admin_api'): ApiHandler
    {
        if (!$this->isCsrfTokenValid($id, $this->request->get('csrf_token'))) {
            $this->error->send($this->translator->trans('geek_star_admin.alert.csrf_token_valid_error'));
        }

        return $this;
    }

    public function isCsrfTokenValid($id, $token)
    {
        return $this->container->get('security.csrf.token_manager')->isTokenValid(new CsrfToken($id, $token));
    }

    public function validate($path, $constraints = null, $sendError = true): ApiHandler
    {
        if (is_array($path)) {
            foreach ($path as $scope => $keys) {
                foreach ($keys as $key => $constraints) {
                    $this->validate("{$scope}/{$key}", $constraints, false);
                }
            }

        } else {

            list($scope, $key) = explode('/', $path);

            foreach ($this->validator->validate($this->request->{$scope}->get($key), $constraints) as $violation) {
                $this->error->{$scope}->set($key, $violation->getMessage());
                break;
            }
        }

        if ($sendError) {
            $this->error->send();
        }

        return $this;
    }
    
    public function serialize()
    {
        return $this->container->get('serializer')->serialize($this->data, 'json');
    }

    public function response($data = null, $status = 200): Response
    {
        if (null !== $data) {
            $this->setData($data);
        }

        $response = new Response($this->serialize(), $status);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}