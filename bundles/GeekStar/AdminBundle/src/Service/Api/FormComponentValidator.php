<?php

namespace GeekStar\AdminBundle\Service\Api;

use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FormComponentValidator
{
    protected ContainerInterface $container;
    protected ValidatorInterface $validator;

    public ComponentRequest $request;
    public ComponentResponse $response;

    public function __construct(ContainerInterface $container, ComponentRequest $request, ComponentResponse $response)
    {
        $this->container = $container;
        $this->request = $request;
        $this->response = $response;

        $validator = $container->get('validator');
        if($validator instanceof ValidatorInterface) {
            $this->validator = $validator;
        }
    }

    public function validate($path, $constraints = null, $sendError = true): FormComponentValidator
    {
        if (is_array($path)) {
            foreach ($path as $scope => $keys) {
                foreach ($keys as $key => $constraints) {
                    $this->validate("{$scope}/{$key}", $constraints, false);
                }
            }

        } else {

            list($scope, $key) = explode('/', $path);

            foreach ($this->validator->validate($this->request->{$scope}->get('formData')[$key] ?? null, $constraints) as $violation) {
                $this->response->error->{$scope}->set($key, $violation->getMessage());
                break;
            }
        }

        if ($sendError) {
            $this->response->error->send();
        }

        return $this;
    }
}