<?php

namespace GeekStar\AdminBundle\Service;

use GeekStar\AdminBundle\Elements\User\UserInterface;
use GeekStar\AdminBundle\Exception\ServiceException;
use GeekStar\AdminBundle\Manager;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\RememberMeFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;
use Symfony\Component\Yaml\Yaml;

class SecurityService
{
    private ContainerInterface $container;
    private Manager $manager;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function initialize(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return UserInterface|null
     */
    public function getCurrentUser(): ?UserInterface
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }

    public function authorizeUser(UserInterface $user, string $firewall = 'main'): TokenInterface
    {
        $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());

        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('_security_' . $firewall, serialize($token));

        return $token;
    }

    public function rememberMe(UserProviderInterface $userProvider, Request $request, Response $response, TokenInterface $token, string $firewall = 'main')
    {
        $rememberMeServices = $this->getRememberMeService($userProvider, $firewall);
        $r = new \ReflectionMethod($rememberMeServices, 'onLoginSuccess');
        $r->setAccessible(true);
        $r->invoke($rememberMeServices, $request, $response, $token);
    }

    public function getRememberMeService(UserProviderInterface $userProvider, $firewall): TokenBasedRememberMeServices
    {
        $defaultOptions = $this->getDefaultRememberMeOption();
        $config = $this->getSecurityConfig();

        $options = $defaultOptions;

        $firewallRememberMeOptions = $config['security']['firewalls'][$firewall]['remember_me'] ?? null;
        if($firewallRememberMeOptions) {
            $options = array_merge($options, $firewallRememberMeOptions);
        }

        $secret = $this->container->getParameter('kernel.secret');

        return new TokenBasedRememberMeServices([$userProvider], $secret, $firewall, $options);
    }

    public function logout(Request $request, Response &$response, string $firewall = 'main')
    {
        $defaultOptions = $this->getDefaultRememberMeOption();
        $config = $this->getSecurityConfig();

        $firewallOptions = $config['security']['firewalls'][$firewall]['remember_me'] ?? null;

        $cookieName = $firewallOptions['name'] ?? $defaultOptions['name'] ?? 'REMEMBERME';
        $cookiePath = $firewallOptions['path'] ?? $defaultOptions['path'] ?? '/';

        $this->container->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        $response->headers->clearCookie($cookieName, $cookiePath);
    }

    public function getDefaultRememberMeOption()
    {
        $r = new \ReflectionClass(RememberMeFactory::class);

        return $r->getDefaultProperties()['options'];
    }

    public function getSecurityConfig()
    {
        if(!isset($this->manager)) {
            throw new ServiceException($this->container->get('translator')->trans('geek_star_admin.alert.manager_not_set'));
        }

        $securityConfigPath = $this->manager->config['security']['config_path'] ?? 'config/packages/security.yaml';
        $filePath = sprintf("%s/%s", $this->container->getParameter('kernel.project_dir'), $securityConfigPath);

        return Yaml::parse(file_get_contents($filePath));
    }
}