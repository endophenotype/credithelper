<?php

namespace GeekStar\AdminBundle\Service;

use GeekStar\AdminBundle\Elements\ComponentInterface;
use GeekStar\AdminBundle\Elements\FieldInterface;
use GeekStar\AdminBundle\Elements\FormComponentInterface;
use GeekStar\AdminBundle\Elements\PageInterface;
use GeekStar\AdminBundle\Elements\Request\PageRequest;
use GeekStar\AdminBundle\Elements\User\UserInterface;
use GeekStar\AdminBundle\Exception\AccessDeniedException;
use GeekStar\AdminBundle\Exception\NotFoundException;
use GeekStar\AdminBundle\Manager;
use Symfony\Component\Security\Core\User\UserInterface as CoreUserInterface;

class PageService
{
    protected Manager $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function getPage(string $category, string $module, string $page, ?CoreUserInterface $user): ?PageInterface
    {
        if(!($user instanceof UserInterface))
        {
            throw new AccessDeniedException();
        }

        if(
            !$category = $this->manager->getCategory($category) or
            !$module = $category->getModule($module) or
            !$page = $module->getPage($page)
        )
        {
            throw new NotFoundException();
        }

        if(!(
            $category->checkPermission($user) and
            $module->checkPermission($user) and
            $page->checkPermission($user)
        )) {
            throw new AccessDeniedException();
        }

        return $page;
    }

    public function getComponent(PageInterface $page, string $componentKey, PageRequest $request): ?ComponentInterface
    {
        $page->setRequest($request);
        $page->initialize();

        if(!$component = $page->getComponent($componentKey)){
            throw new NotFoundException();
        }

        return $component;
    }

    public function getField(PageInterface $page, string $componentKey, string $fieldKey, PageRequest $request): ?FieldInterface
    {
        $component = $this->getComponent($page, $componentKey, $request);

        $field = null;

        if($component instanceof FormComponentInterface) {
            $field = $component->getField($fieldKey);
        }

        if(!$field) {
            throw new NotFoundException();
        }

        return $field;
    }
}