<?php

namespace GeekStar\AdminBundle\Model;

class FileListItem
{
    public string $key = '';
    public string $url = '';
    public string $preview = '';
    public string $fileName = '';
    public string $title = '';
    public string $extension = '';
    public int $size = 0;
    public int $sortIndex = 0;

    public static function create(): FileListItem
    {
        return new static();
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): FileListItem
    {
        $this->key = $key;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): FileListItem
    {
        $this->url = $url;

        return $this;
    }

    public function getPreview(): string
    {
        return $this->preview;
    }

    public function setPreview($preview): FileListItem
    {
        $this->preview = $preview;

        return $this;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): FileListItem
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle($title): FileListItem
    {
        $this->title = $title;

        return $this;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): FileListItem
    {
        $this->extension = $extension;

        return $this;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size): FileListItem
    {
        $this->size = $size;

        return $this;
    }

    public function getSortIndex(): int
    {
        return $this->sortIndex;
    }

    public function setSortIndex($sortIndex): FileListItem
    {
        $this->sortIndex = $sortIndex;

        return $this;
    }
}