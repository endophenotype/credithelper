<?php

namespace GeekStar\AdminBundle\Model;

use GeekStar\AdminBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\ParameterBag;

class ApiError
{
    protected ?string $message = null;
    protected int $code = 0;
    protected int $status = 400;

    public ParameterBag $query;
    public ParameterBag $request;
    public ParameterBag $files;

    public function __construct()
    {
        $this->query = new ParameterBag();
        $this->request = new ParameterBag();
        $this->files = new ParameterBag();
    }

    /**
     * @param string|null $message
     * @param int|null $code
     * @param int|null $status
     * @throws ApiException
     */
    public function send(string $message = null, int $code = null, int $status = null)
    {
        if (null !== $message) {
            $this->message = $message;
        }

        if (null !== $code) {
            $this->code = $code;
        }

        if (null !== $status) {
            $this->status = $status;
        }

        if ($this->has()) {
            $exception = new ApiException($this->message, $this->code);
            $exception->setApiError($this);

            throw $exception;
        }
    }

    public function unauthorized($message = '')
    {
        $this->send($message, 401, 401);
    }

    public function forbidden($message = '')
    {
        $this->send($message, 403, 403);
    }

    public function notFound($message = '')
    {
        $this->send($message, 404, 404);
    }

    public function has(): bool
    {
        return ($this->message !== null || $this->request->count() || $this->query->count() || $this->files->count());
    }

    public function dump(): array
    {
        $data = [
            'message' => (string)$this->message,
            'code' => (int)$this->code,
        ];

        if ($this->query->count()) {
            $data['query'] = $this->query->all();
        }

        if ($this->request->count()) {
            $data['request'] = $this->request->all();
        }

        if ($this->files->count()) {
            $data['files'] = $this->files->all();
        }

        return ['error' => $data];
    }

    public function setStatus(int $status): ApiError
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setCode(int $code): ApiError
    {
        $this->code = $code;
        return $this;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function setMessage(string $message): ApiError
    {
        $this->message = $message;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function set(string $path, string $message): ApiError
    {
        list($scope, $key) = explode('/', $path);
        $this->{$scope}->set($key, $message);

        return $this;
    }
}