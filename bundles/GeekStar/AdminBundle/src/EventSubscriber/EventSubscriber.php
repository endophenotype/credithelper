<?php

namespace GeekStar\AdminBundle\EventSubscriber;

use GeekStar\AdminBundle\Manager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Environment;

class EventSubscriber implements EventSubscriberInterface
{
    protected ContainerInterface $container;
    protected RouterInterface $router;
    protected Environment $twig;
    protected SerializerInterface $serializer;
    protected Manager $manager;

    public function __construct(
        ContainerInterface $container,
        RouterInterface $router,
        Environment $twig,
        SerializerInterface $serializer,
        Manager $manager
    )
    {
        $this->container = $container;
        $this->router = $router;
        $this->twig = $twig;
        $this->serializer = $serializer;
        $this->manager = $manager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $this->manager->locale = $event->getRequest()->getLocale();
        $this->twig->addGlobal('locale', $this->manager->locale);

        $this->manager->user = $this->manager->securityService->getCurrentUser();
        $this->twig->addGlobal('admin', $this->manager);
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }
    }
}