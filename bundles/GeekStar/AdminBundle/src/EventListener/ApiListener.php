<?php

namespace GeekStar\AdminBundle\EventListener;

use GeekStar\AdminBundle\Exception\ApiException;
use GeekStar\AdminBundle\Model\ApiError;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\InsufficientAuthenticationException;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApiListener
{
    public static ?string $apiUrl = null;

    protected ContainerInterface $container;
    protected UrlGeneratorInterface $router;
    protected TranslatorInterface $translator;

    public function __construct(ContainerInterface $container, UrlGeneratorInterface $router, TranslatorInterface $translator)
    {
        $this->container = $container;
        $this->router = $router;
        $this->translator = $translator;

        if(!self::$apiUrl) {
            self::$apiUrl = $this->router->generate('geek_star_admin_index') . 'api/';
        }
    }

    public function checkApiPath(string $path) {
        return preg_match('#^' . self::$apiUrl . '#', $path);
    }

    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }
    }

    public function onKernelException(ExceptionEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if (!$this->checkApiPath($event->getRequest()->getPathInfo())) {
            return;
        }

        if ($this->container->get('request_stack')->getCurrentRequest() !== $event->getRequest()) {
            return;
        }

        $exception = $event->getThrowable();

        if ($exception instanceof ApiException) {
            $error = $exception->getApiError();

        } else if ($exception->getPrevious() instanceof InsufficientAuthenticationException) {
            $error = new ApiError();
            $error->setMessage($this->translator->trans('geek_star_admin.alert.authorization_required'));
            $error->setCode($exception->getCode());
            $error->setStatus(401);

        } else if ($exception instanceof AccessDeniedHttpException) {
            $error = new ApiError();
            $error->setMessage($this->translator->trans('geek_star_admin.alert.access_denied'));
            $error->setCode($exception->getCode());
            $error->setStatus(403);

        } else if ($exception instanceof NotFoundHttpException) {
            $error = new ApiError();
            $error->setMessage($exception->getMessage() ?: $this->translator->trans('geek_star_admin.alert.page_not_found'));
            $error->setCode($exception->getCode());
            $error->setStatus(404);

        } else {
            $error = new ApiError();
            $error->setMessage(
                $this->container->getParameter('kernel.debug') ? $exception->getMessage() : $this->translator->trans('geek_star_admin.alert.kernel_error')
            );
            $error->setCode($exception->getCode());
            $error->setStatus(500);
        }

        $event->setResponse(new JsonResponse($error->dump(), $error->getStatus()));
    }
}