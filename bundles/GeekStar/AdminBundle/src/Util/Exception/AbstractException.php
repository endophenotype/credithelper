<?php

namespace GeekStar\AdminBundle\Util\Exception;

use Throwable;

abstract class AbstractException extends \Exception
{
    public static function create($message = "", $code = 0, Throwable $previous = null): AbstractException
    {
        return new static($message, $code, $previous);
    }
}