<?php

namespace GeekStar\AdminBundle;

use GeekStar\AdminBundle\Elements\CategoryInterface;
use GeekStar\AdminBundle\Elements\PageInterface;
use GeekStar\AdminBundle\Exception\AccessDeniedException;
use GeekStar\AdminBundle\Exception\NotFoundException;
use GeekStar\AdminBundle\Service\SecurityService;
use GeekStar\AdminBundle\Elements\User\UserInterface;
use GeekStar\AdminBundle\Elements\User\UserManagerInterface;
use GeekStar\AdminBundle\Validator\ManagerValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Manager
{
    protected ContainerInterface $container;
    protected ManagerValidator $validator;
    protected SerializerInterface $serializer;
    protected TranslatorInterface $translator;
    public SecurityService $securityService;

    public ?UserInterface $user;
    public ?UserManagerInterface $userManager;
    public array $config;
    public string $locale;

    /** @var CategoryInterface[] $categories */
    public array $categories = [];
    protected ?PageInterface $defaultPage = null;

    protected ?PageInterface $currentPage = null;

    public function __construct(
        ContainerInterface $container,
        TranslatorInterface $translator,
        SerializerInterface $serializer,
        ManagerValidator $validator,
        SecurityService $securityService
    )
    {
        $this->container = $container;
        $this->translator = $translator;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->securityService = $securityService;

        $this->securityService->initialize($this);

        $this->config = $this->container->getParameter('geek_star_admin.config');
        $this->validator->configValidate($this->container, $this->config);

        $this->userManager = $this->container->get($this->config['security']['user_manager']);
    }

    public function initialize()
    {
        $this->sortCategories();
    }

    public function addCategory(CategoryInterface $category)
    {
        $category->setContainer($this->container);
        $category->configure();

        if($category->isVisible())
        {
            $category->initialize();
            $category->sortModules();
            $this->categories[$category->getUrn()] = $category;
        }
    }

    public function getCategory(string $urn): ?CategoryInterface
    {
        return $this->categories[$urn] ?? null;
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function sortCategories()
    {
        uasort($this->categories, function(CategoryInterface $a, CategoryInterface $b)
        {
            if($a->getSortIndex() > $b->getSortIndex()) return 1;
            if($a->getSortIndex() < $b->getSortIndex()) return -1;
            return 0;
        });
    }

    /**
     * @param UserInterface $user
     * @return PageInterface|null
     * @throws AccessDeniedException
     * @throws NotFoundException
     */
    public function getDefaultPage(UserInterface $user): ?PageInterface
    {
        if($this->defaultPage)
        {
            return $this->defaultPage;
        }
        else
        {
            $pageCount = 0;
            $renderPage = null;

            foreach($this->categories as $category)
            {
                if($category->checkPermission($user)) {
                    foreach($category->getModules() as $module)
                    {
                        if($module->checkPermission($user)) {
                            if($defaultPage = $module->getDefaultPage() and $defaultPage->checkPermission($user)) {
                                $this->defaultPage = $defaultPage;

                                return $defaultPage;

                            } else {
                                foreach($module->getPages() as $page)
                                {
                                    $pageCount++;

                                    if($page->checkPermission($user)) {
                                        $this->defaultPage = $page;

                                        return $page;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!$pageCount)
            {
                throw new NotFoundException();
            }

            if($renderPage === null)
            {
                throw new AccessDeniedException();
            }

            return null;
        }
    }
}