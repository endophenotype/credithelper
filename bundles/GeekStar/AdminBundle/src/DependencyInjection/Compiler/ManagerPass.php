<?php

namespace GeekStar\AdminBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ManagerPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    public function process(ContainerBuilder $container)
    {
        $managerDefinition = $container->getDefinition('GeekStar\AdminBundle\Manager');

        $categoryIds = $this->findAndSortTaggedServices('geek_star_admin.category', $container);

        foreach ($categoryIds as $categoryId) {
            $managerDefinition->addMethodCall('addCategory', [new Reference((string) $categoryId)]);
        }

        $managerDefinition->addMethodCall('initialize');
    }
} 