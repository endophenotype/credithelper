<?php

namespace GeekStar\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('geek_star_admin');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('company_name')
                    ->isRequired()
                ->end()
                ->scalarNode('favicon_path')
                    ->defaultValue('bundles/geekstaradmin/build/img/favicon.ico')
                ->end()
                ->arrayNode('security')
                    ->children()
                        ->scalarNode('user_manager')
                            ->beforeNormalization()
                                ->ifTrue(function ($v) {
                                    return is_string($v) && 0 === strpos($v, '@');
                                })
                                ->then(function ($v) {
                                    return substr($v, 1);
                                })
                            ->end()
                            ->isRequired()
                        ->end()
                        ->scalarNode('config_path')
                            ->defaultValue('config/packages/security.yaml')
                        ->end()
                        ->scalarNode('firewall')
                            ->isRequired()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('assets')
                    ->children()
                        ->scalarNode('stylesheets')->end()
                        ->scalarNode('javascripts_header')->end()
                        ->scalarNode('javascripts_footer')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
