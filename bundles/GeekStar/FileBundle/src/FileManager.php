<?php

namespace GeekStar\FileBundle;

use GeekStar\FileBundle\PropelModel\File;
use GeekStar\FileBundle\PropelModel\FileQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileManager
{
    const DEFAULT_UPLOAD_DIR = 'uploads';

    protected ContainerInterface $container;
    protected Filesystem $filesystem;

    public function __construct(ContainerInterface $container, Filesystem $filesystem)
    {
        $this->container = $container;
        $this->filesystem = $filesystem;
    }

    public function uploadFile(UploadedFile $uploadedFile, string $directory = null, $isPublic = false): File
    {
        $hashSum = hash_file('sha256', $uploadedFile->getRealPath());
        $uploadDir = $directory ?: $this->getUploadDir();

        $file = FileQuery::create()
            ->filterByPath($uploadDir)
            ->filterByHashSum($hashSum)
            ->findOne();

        if(!$file) {
            $extension = $uploadedFile->getClientOriginalExtension();
            $size = $uploadedFile->getSize();
            $mime = $uploadedFile->getMimeType();
            $originalName = $uploadedFile->getClientOriginalName();

            $name = md5(uniqid()) . '.' . $extension;
            $path = $this->getUploadPath($uploadDir, $isPublic);
            $uploadedFile->move($path, $name);

            $file = new File();
            $file
                ->setPath($uploadDir)
                ->setName($name)
                ->setOriginalName($originalName)
                ->setExtension($extension)
                ->setMime($mime)
                ->setSize($size)
                ->setHashSum($hashSum)
                ->setIsPublic($isPublic)
                ->save();
        }

        return $file;
    }

    protected function getUploadPath(string $directory, $isPublic = false): string
    {
        $rootDirectory = $isPublic ? 'public' : 'var';

        $path = join(DIRECTORY_SEPARATOR, [
            $this->container->getParameter('kernel.project_dir'),
            $rootDirectory,
            $directory,
        ]);

        if (!$this->filesystem->exists($path)) {
            $this->makeUploadDir($path);
        }

        return $path;
    }

    public function getUploadDir(): string
    {
        return self::DEFAULT_UPLOAD_DIR;
    }

    protected function makeUploadDir($path)
    {
        $this->filesystem->mkdir($path);
    }

    public function removeUnusedFiles()
    {
        $unusedFiles = FileQuery::create()
            ->useFileHolderQuery(null, Criteria::LEFT_JOIN)
                ->filterById(null, Criteria::ISNULL)
            ->endUse()
            ->distinct()
            ->find();

        foreach ($unusedFiles as $file) {
            $filePath = join(DIRECTORY_SEPARATOR, [
                $this->getUploadPath($file->getPath(), $file->isPublic()),
                $file->getName(),
            ]);
            $this->filesystem->remove($filePath);
        }

        $unusedFiles->delete();
    }
}