<?php

namespace GeekStar\FileBundle\PropelModel;

use GeekStar\FileBundle\FileManager;
use GeekStar\FileBundle\PropelModel\Base\File as BaseFile;

/**
 * Skeleton subclass for representing a row from the 'file' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class File extends BaseFile
{
    public function getFullPath(): string
    {
        return DIRECTORY_SEPARATOR . $this->getPath() . DIRECTORY_SEPARATOR . $this->getName();
    }

    public function getRealPath(): string
    {
        $rootDirectory = $this->getIsPublic() ? 'public' : 'var';

        return $rootDirectory . $this->getFullPath();
    }
}
