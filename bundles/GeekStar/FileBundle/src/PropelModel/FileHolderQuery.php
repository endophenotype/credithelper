<?php

namespace GeekStar\FileBundle\PropelModel;

use GeekStar\FileBundle\PropelModel\Base\FileHolderQuery as BaseFileHolderQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'file_holder' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class FileHolderQuery extends BaseFileHolderQuery
{

}
