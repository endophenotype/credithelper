<?php

namespace GeekStar\FileBundle\PropelModel;

use GeekStar\FileBundle\PropelModel\Base\FileHolder as BaseFileHolder;

/**
 * Skeleton subclass for representing a row from the 'file_holder' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class FileHolder extends BaseFileHolder
{

}
