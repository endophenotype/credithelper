<?php

namespace GeekStar\FileBundle\Normalizer;

use GeekStar\FileBundle\PropelModel\FileHolder;
use GeekStar\FileBundle\Util\Normalizer\AbstractNormalizer;

class FileHolderNormalizer extends AbstractNormalizer
{
    /**
     * @param FileHolder $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|\Countable|float|int|mixed|string|\Traversable|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = [
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'file' => $object->getFile(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof FileHolder;
    }
}