<?php

namespace GeekStar\FileBundle\Normalizer;

use GeekStar\FileBundle\PropelModel\File;
use GeekStar\FileBundle\Util\Normalizer\AbstractNormalizer;

class FileNormalizer extends AbstractNormalizer
{
    /**
     * @param File $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|\Countable|float|int|mixed|string|\Traversable|null
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = [
            'id' => $object->getId(),
            'name' => $object->getName(),
            'originalName' => $object->getOriginalName(),
            'extension' => $object->getExtension(),
            'path' => $object->getPath(),
            'mime' => $object->getMime(),
            'size' => $object->getSize(),
            'hashSum' => $object->getHashSum(),
        ];

        return $this->serializer->normalize($data, $format, $context);
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof File;
    }
}