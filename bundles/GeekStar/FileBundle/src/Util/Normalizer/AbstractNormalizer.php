<?php

namespace GeekStar\FileBundle\Util\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractNormalizer implements NormalizerInterface
{
    protected Serializer $serializer;

    public function setSerializer(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function hasGroup($context, $group): bool
    {
        return isset($context['groups']) and in_array($group, $context['groups']);
    }
}