<?php

namespace App\Business\Security\Service;

use App\Business\Security\Model\User;
use App\Business\Security\Model\UserManager;
use App\Business\Security\Model\UserSign;
use App\Business\Security\Model\UserSignManager;
use App\Util\Service\AbstractService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserService extends AbstractService
{
    private UserManager $userManager;
    private UserSignManager $userSignManager;

    public function __construct(ContainerInterface $container, UserManager $userManager, UserSignManager $userSignManager)
    {
        $this->userManager = $userManager;
        $this->userSignManager = $userSignManager;

        parent::__construct($container);
    }

    /**
     * @param int $id
     * @return User|null
     * @throws \Exception
     */
    public function getUser(int $id): ?User
    {
        return $this->userManager->getUser($id);
    }

    /**
     * @param string $provider
     * @param string $username
     * @param User $user
     * @param bool $enabled
     * @return UserSign|null
     */
    public function getUserSign(string $provider, string $username, User $user = null, bool $enabled = null): ?UserSign
    {
        return $this->userSignManager->getUserSign($provider, $username, $user, $enabled);
    }

    /**
     * @return User|null
     */
    public function getCurrentUser(): ?User
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }
}