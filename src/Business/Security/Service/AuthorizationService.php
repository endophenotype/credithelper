<?php

namespace App\Business\Security\Service;

use App\Business\Security\DTO\SecurityAuthorizeParameters;
use App\Business\Security\DTO\SecurityChangePasswordParameters;
use App\Business\Security\DTO\SecurityRegistrationParameters;
use App\Business\Security\Event\SecurityAuthorizeEvent;
use App\Business\Security\Event\SecurityLogoutEvent;
use App\Business\Security\Event\SecurityRegistrationEvent;
use App\Business\Security\Model\User;
use App\Business\Security\Model\UserSign;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class AuthorizationService implements AuthorizationInterface
{
    const SESSION_OAUTH_MODE = 'oauth_mode';
    const OAUTH_MODE_SIGN_IN = 1;
    const OAUTH_MODE_CONNECT = 2;

    protected ContainerInterface $container;
    protected UserService $userService;
    protected UserPasswordEncoderInterface $encoder;
    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ContainerInterface $container,
        UserService $userService,
        UserPasswordEncoderInterface $encoder,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->container = $container;
        $this->userService = $userService;
        $this->encoder = $encoder;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function createUser(SecurityRegistrationParameters $parameters): User
    {
        $user = new User();

        $user
            ->setName($parameters->getName())
            ->setEmail($parameters->getEmail())
            ->save();

        $userSign = new UserSign();
        $user->setCurrentUserSign($userSign);

        $userSign
            ->setProvider($parameters->getProvider())
            ->setUsername($parameters->getUsername())
            ->setUser($user)
            ->setEnabled(true)
            ->setSecret($this->generateSecret())
            ->setPassword('')
            ->setSalt('');

        $this->changePassword($userSign, $parameters->getPassword());

        $userSign->save();

        return $user;
    }

    public function changePassword(UserSign $userSign, $password)
    {
        $password = $this->encoder->encodePassword($userSign->getUser(), $password);

        $userSign
            ->setSalt($this->generateSalt())
            ->setPassword($password);
    }

    public function generateSecret(): string
    {
        return md5(uniqid());
    }

    public function generatePassword($length = 6): string
    {
        return mb_substr(md5(uniqid()), 0, $length);
    }

    public function generateSalt(): string
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }

    public function authorizeUser(UserInterface $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());

        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('_security_main', serialize($token));
    }

    public function loadUserByUsername($username)
    {
        $user = null;
        if(is_numeric($username)) {
            $user = $this->userService->getUser((int)$username);

        } else {
            $sign = $this->userService->getUserSign('email', $username, null, true);
            if($sign) {
                $user = $sign->getUser();
            }
        }

        if (!$user) {
            $exception = new UsernameNotFoundException;
            $exception->setUsername($username);
            throw $exception;
        }

        return $user;
    }

    public function refreshUser(UserInterface $user): ?User
    {
        if ($refreshed = $this->userService->getUser($user->getId())) {
            return $refreshed;
        }

        throw new UnsupportedUserException();
    }

    public function supportClass($class): bool
    {
        return 'App\Business\Security\Model\User' == $class;
    }

    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {

    }

    protected function clearOAuthSessionData()
    {
        $this->container->get('session')->remove(self::SESSION_OAUTH_MODE);
    }

    public function oauthRequestHandler(Request $request)
    {
        if (preg_match('/^\/oauth\/connect\//', $request->getPathInfo())) {
            $this->clearOAuthSessionData();

            $session = $this->container->get('session');
            if ($request->query->has('signin')) {
                $session->set(self::SESSION_OAUTH_MODE, self::OAUTH_MODE_SIGN_IN);

            } else {
                $session->set(self::SESSION_OAUTH_MODE, self::OAUTH_MODE_CONNECT);
            }
        }
    }

    public function registration(SecurityRegistrationParameters $parameters): array
    {
        $user = $this->createUser($parameters);
        $this->authorizeUser($user);

        $registrationEvent = new SecurityRegistrationEvent();
        $registrationEvent->setUserId($user->getId());

        $this->eventDispatcher->dispatch($registrationEvent, $registrationEvent::EVENT_NAME);

        return ['success' => true];
    }

    public function login(SecurityAuthorizeParameters $parameters): array
    {
        if(!$userSign = $parameters->getUserSign()) {
            $userSign = $this->userService->getUserSign($parameters->getProvider(), $parameters->getUsername());
        }

        $user = $userSign->getUser();
        $this->authorizeUser($user);

        $loginEvent = new SecurityAuthorizeEvent();
        $loginEvent->setUserId($user->getId());

        $this->eventDispatcher->dispatch($loginEvent, $loginEvent::EVENT_NAME);

        return ['success' => true];
    }

    public function changeUserPassword(SecurityChangePasswordParameters $parameters): array
    {
        if(!$userSign = $parameters->getUserSign()) {
            $userSign = $parameters->getUser()->getUserSign($parameters->getProvider());
        }

        $this->changePassword($userSign, $parameters->getOldPassword());
        $userSign->save();

        return ['success' => true];
    }

    public function logout(Request $request): array
    {
        $user = $this->userService->getCurrentUser();

        if($user) {
            $this->container->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();

            $logoutEvent = new SecurityLogoutEvent();
            $logoutEvent->setUserId($user->getId());

            $this->eventDispatcher->dispatch($logoutEvent, $logoutEvent::EVENT_NAME);

            return ['success' => true];

        } else {
            return ['success' => false];
        }
    }
}