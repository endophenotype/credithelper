<?php

namespace App\Business\Security\EventSubscriber;

use App\Business\Security\Event\SecurityAuthorizeEvent;
use App\Business\Security\Event\SecurityLogoutEvent;
use App\Business\Security\Event\SecurityRegistrationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SecurityEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            SecurityAuthorizeEvent::EVENT_NAME => 'onSecurityAuthorize',
            SecurityLogoutEvent::EVENT_NAME => 'onSecurityLogout',
            SecurityRegistrationEvent::EVENT_NAME => 'onSecurityRegistration',
        ];
    }

    public function onSecurityAuthorize(SecurityAuthorizeEvent $event)
    {
    }

    public function onSecurityLogout(SecurityLogoutEvent $event)
    {
    }

    public function onSecurityRegistration(SecurityRegistrationEvent $event)
    {
    }
}