<?php

namespace App\Business\Security\Normalizer;

use App\Business\Security\Model\User;
use App\Util\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;

class UserNormalizer extends AbstractNormalizer
{
    const GROUP_DETAIL = 'user_group_self';

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param User $object Object to normalize
     * @param string $format Format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|int|float|bool
     *
     * @throws InvalidArgumentException   Occurs when the object given is not an attempted type for the normalizer
     * @throws CircularReferenceException Occurs when the normalizer detects a circular reference when no circular
     *                                    reference handler can fix it
     * @throws LogicException             Occurs when the normalizer is not called in an expected context
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        if(in_array('geek_star_admin', $context))
        {
            $data = [
                'avatarInAdmin' => $object->getAvatarInAdmin(),
                'nameInAdmin' => $object->getNameInAdmin(),
            ];
        }
        else
        {
            $data = [
                'id' => $object->getId(),
                'name' => $object->getName(),
            ];
        }


        if($this->hasGroup($context, self::GROUP_DETAIL)) {
            $data += [
                'email' => $object->getEmail(),
                'roles' => $object->getRoles(),
            ];
        }

        return $this->serializer->normalize($data, $format, $context);
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize
     * @param string $format The format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof User;
    }
}