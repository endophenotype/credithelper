<?php

namespace App\Business\Security\Model;

use App\Model\UserQuery;
use GeekStar\AdminBundle\Elements\User\UserManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager implements UserManagerInterface
{
    private UserSignManager $userSignManager;
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder, UserSignManager $userSignManager)
    {
        $this->userSignManager = $userSignManager;
        $this->encoder = $encoder;
    }

    public function getUserQuery(): UserQuery
    {
        return UserQuery::create();
    }

    /**
     * @param int $id
     * @return User|null
     * @throws \Exception
     */
    public function getUser(int $id): ?User
    {
        $propelUser = $this->getUserQuery()->findPk($id);

        return $propelUser ? new User($propelUser) : null;
    }

    public function getUserByUsernamePassword(string $username, string $password): ?User
    {
        if($userSign = $this->userSignManager->getUserSign('email', $username, null, true)) {
            $user = $userSign->getUser();

            if($this->encoder->isPasswordValid($user, $password)) {
                return $user;
            }
        }

        return null;
    }
}