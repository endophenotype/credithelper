<?php

namespace App\Business\Security\Model;

use App\Business\Security\DTO\UserSignParameters;
use App\Model\User as PropelUser;
use App\Model\UserRoleQuery;
use App\Util\Model\AbstractModelFromPropel;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use GeekStar\AdminBundle\Elements\User\UserInterface as AdminUserInterface;

/**
 * @property PropelUser $propelModel
 *
 * @method int getId()
 * @method User setId(int $id)
 *
 * @method string getName()
 * @method User setName(string $name)
 *
 * @method string getEmail()
 * @method User setEmail(string $email)
 *
 * @method bool isNew()
 * @method delete()
 * @method save()
 */
class User extends AbstractModelFromPropel implements UserInterface, AdminUserInterface
{
    const PROPEL_CLASS = 'App\Model\User';
    const MAIN_PROVIDER = 'email';

    private UserSignManager $userSignManager;
    private ?UserSign $currentUserSign = null;
    private ?array $roles = null;

    public function __construct(ActiveRecordInterface $propelModel = null)
    {
        $this->userSignManager = new UserSignManager();

        parent::__construct($propelModel);

        if($propelModel !== null and $mainUserSign = $this->getMainUserSign()) {
            $this->setCurrentUserSign($mainUserSign);
        }
    }

    /*
     * ============= Start Security section ==============
     */

    public function getUserSign(string $provider, string $username = null): ?UserSign
    {
        return $this->userSignManager->getUserSign($provider, $username, $this);
    }

    public function setUserSign(UserSignParameters $parameters): User
    {
        if($parameters->getUserSign() === null) {
            $userSign = $this->getUserSign($parameters->getProvider(), $parameters->getUsername());
        } else {
            $userSign = $parameters->getUserSign();
        }

        if(!$userSign) {
            if(!$this->propelModel->getId()) {
                throw new \Exception("Save User data before set UserSign.");
            }

            $userSign = new UserSign();
            $userSign
                ->setPassword('')
                ->setSalt('')
                ->setSecret('')
                ->setEnabled(false)
                ->setUser($this);
        }

        $userSign
            ->setUsername($parameters->getUsername())
            ->setProvider($parameters->getProvider());

        if($parameters->getPassword() !== null) {
            $userSign->setPassword($parameters->getPassword());
        }

        if($parameters->getSalt() !== null) {
            $userSign->setSalt($parameters->getSalt());
        }

        if($parameters->getEnable() !== null) {
            $userSign->setEnabled($parameters->getEnable());
        }

        $userSign->save();

        return $this;
    }

    public function getMainUserSign(): ?UserSign
    {
        return $this->getUserSign(self::MAIN_PROVIDER);
    }

    public function setCurrentUserSign(UserSign $currentUserSign)
    {
        $this->currentUserSign = $currentUserSign;
    }

    public function getSalt(): string
    {
        $this->checkCurrentUserSign();

        return $this->currentUserSign->getSalt();
    }

    public function setSalt(string $salt): User
    {
        $this->checkCurrentUserSign();

        $this->currentUserSign
            ->setSalt($salt)
            ->save();

        return $this;
    }

    public function getPassword(): string
    {
        $this->checkCurrentUserSign();

        return $this->currentUserSign->getPassword();
    }

    public function setPassword(string $password): User
    {
        $this->checkCurrentUserSign();

        $this->currentUserSign
            ->setPassword($password)
            ->save();

        return $this;
    }

    public function getUsername(): string
    {
        $this->checkCurrentUserSign();

        return $this->currentUserSign->getUsername();
    }

    public function checkCurrentUserSign() {
        if($this->currentUserSign === null) {
            throw new \Exception("Don't set CurrentUserSign.");
        }
    }

    public function eraseCredentials()
    {
    }

    public function getRoles(): array
    {
        if($this->roles === null) {
            $this->roles = [];

            if(!$this->isNew()) {
                $propelUserRoles = UserRoleQuery::create()
                    ->useUserRoleGroupUserRoleRelQuery()
                        ->useUserRoleGroupQuery()
                            ->useUserRoleGroupUserRelQuery()
                                ->filterByUserId($this->getId())
                            ->endUse()
                        ->endUse()
                    ->endUse()
                    ->distinct()
                    ->orderByName()
                    ->find();

                foreach ($propelUserRoles as $propelUserRole) {
                    $this->roles[] = $propelUserRole->getName();
                }
            }

            if(!count($this->roles)) {
                $this->roles[] = 'ROLE_USERS';
            }
        }

        return $this->roles;
    }

    /*
     * ============= End Security section ==============
     */

    public function getAvatarInAdmin(): ?string
    {
        return 'build/img/user.jpg';
    }

    public function getNameInAdmin(): string
    {
        return $this->getName();
    }
}