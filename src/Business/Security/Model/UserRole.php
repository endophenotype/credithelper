<?php

namespace App\Business\Security\Model;

use App\Model\UserRole as PropelUserRole;
use App\Util\Model\AbstractModelFromPropel;

/**
 * @property PropelUserRole $propelModel
 *
 * @method string getTitle()
 * @method UserRole setTitle(string $title)
 *
 * @method string getName()
 * @method UserRole setName(string $name)
 *
 * @method bool isNew()
 * @method delete()
 * @method save()
 */
class UserRole extends AbstractModelFromPropel
{
    const PROPEL_CLASS = 'App\Model\UserRole';
}