<?php

namespace App\Business\Security\Model;

use App\Model\UserSignQuery;

class UserSignManager
{
    public function getUserSign(string $provider, string $username = null, User $user = null, bool $enabled = null): ?UserSign
    {
        $propelUser = ($user and $user->getPropelModel()) ? $user->getPropelModel() : null;

        $propelUserSign = UserSignQuery::create()
            ->filterByProvider($provider)
            ->_if(null !== $username)
                ->filterByUsername($username)
            ->_endif()
            ->_if(null !== $propelUser)
                ->filterByUser($propelUser)
            ->_endif()
            ->_if(null !== $enabled)
                ->filterByEnabled($enabled)
            ->_endif()
            ->findOne();

        return $propelUserSign ? new UserSign($propelUserSign) : null;
    }
}