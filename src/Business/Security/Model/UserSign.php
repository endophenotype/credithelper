<?php

namespace App\Business\Security\Model;

use App\Model\UserSign as PropelUserSign;
use App\Util\Model\AbstractModelFromPropel;

/**
 * @property PropelUserSign $propelModel
 *
 * @propelRelParam user App\Business\Security\Model\User
 *
 * @method string getProvider()
 * @method UserSign setProvider(string $provider)
 *
 * @method string getUsername()
 * @method UserSign setUsername(string $username)
 *
 * @method string getPassword()
 * @method UserSign setPassword(string $password)
 *
 * @method string getSalt()
 * @method UserSign setSalt(string $salt)
 *
 * @method string getSecret()
 * @method UserSign setSecret(string $secret)
 *
 * @method bool getEnabled()
 * @method UserSign setEnabled(bool $enable)
 *
 * @method User getUser()
 * @method UserSign setUser(User $user)
 *
 * @method bool isNew()
 * @method delete()
 * @method save()
 */
class UserSign extends AbstractModelFromPropel
{
    const PROPEL_CLASS = 'App\Model\UserSign';
}