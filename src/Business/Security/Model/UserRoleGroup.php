<?php

namespace App\Business\Security\Model;

use App\Model\UserRoleGroup as PropelUserRoleGroup;
use App\Util\Model\AbstractModelFromPropel;

/**
 * @property PropelUserRoleGroup $propelModel
 *
 * @method string getTitle()
 * @method UserRole setTitle(string $title)
 *
 * @method bool isNew()
 * @method delete()
 * @method save()
 */
class UserRoleGroup extends AbstractModelFromPropel
{
    const PROPEL_CLASS = 'App\Model\UserRoleGroup';

    public function getRoles(): array
    {
        $roles = [];

        $propelUserRoleRels = $this->propelModel->getUserRoleGroupUserRoleRelsJoinUserRole();

        foreach ($propelUserRoleRels as $propelUserRoleRel) {
            $roles[] = new UserRole($propelUserRoleRel->getUserRole());
        }

        return $roles;
    }
}