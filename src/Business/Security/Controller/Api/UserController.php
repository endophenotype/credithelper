<?php

namespace App\Business\Security\Controller\Api;

use App\Business\Security\Model\User;
use App\Business\Security\Normalizer\UserNormalizer;
use App\Business\Security\Service\UserService;
use Symfony\Component\HttpFoundation\Response;
use Creonit\RestBundle\Annotation\Parameter\PathParameter;
use Creonit\RestBundle\Handler\RestHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * Получить информацию о текущем пользователе
     *
     * @Route("/self", methods={"GET"})
     *
     * @param RestHandler $handler
     * @return Response
     * @throws \Exception
     */
    public function getUserSelf(RestHandler $handler): Response
    {
        $handler->checkAuthorization();

        /** @var User $user */
        $user = $this->getUser();

        $handler->data->addGroup(UserNormalizer::GROUP_DETAIL);

        return $handler->response($user);
    }

    /**
     * Получить информацию о пользователе
     *
     * @PathParameter("id", type="string", description="Идентификатор")
     *
     * @Route("/{id}", methods={"GET"})
     *
     * @param RestHandler $handler
     * @param UserService $userService
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function getUserInfo(RestHandler $handler, UserService $userService, int $id): Response
    {
        $user = $userService->getUser($id);
        $handler->checkFound($user);

        return $handler->response($user);
    }
}
