<?php

namespace App\Business\Security\Controller\Api;

use App\Business\Security\DTO\SecurityAuthorizeParameters;
use App\Business\Security\DTO\SecurityChangePasswordParameters;
use App\Business\Security\DTO\SecurityRegistrationParameters;
use App\Business\Security\Model\User;
use App\Business\Security\Service\AuthorizationService;
use App\Business\Security\Exception\GeekStarSecurityException;
use App\Business\Security\Validator\SecurityValidator;
use Creonit\RestBundle\Handler\RestHandler;
use Creonit\RestBundle\Annotation\Parameter as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @Route("/security")
 */
class SecurityController extends AbstractController
{
    /**
     * Регистрация пользователя
     *
     * @Rest\RequestParameter("csrf_token", type="string", required=true)
     * @Rest\RequestParameter("name", type="string", required=true)
     * @Rest\RequestParameter("email", type="string", required=true)
     * @Rest\RequestParameter("password", type="string", required=true)
     *
     * @Route("/registration", methods={"POST"})
     *
     * @param RestHandler $handler
     * @param Request $request
     * @param AuthorizationService $authorizationService
     * @param SecurityValidator $validator
     * @return mixed
     * @throws \Creonit\RestBundle\Exception\RestErrorException
     * @throws \Exception
     */
    public function registration(
        RestHandler $handler,
        Request $request,
        AuthorizationService $authorizationService,
        SecurityValidator $validator
    )
    {
        $handler->checkCsrfToken('authorize');

        $handler->validate([
            'request' => [
                'name' => [new NotBlank()],
                'email' => [new NotBlank()],
                'password' => [new NotBlank()],
            ]
        ]);

        if($handler->isAuthenticated()) {
            $handler->error->send('Вы уже авторизованы');
        }

        $parameters = SecurityRegistrationParameters::create()->fromRequest($request);

        try {
            $validator->registrationValidate($parameters);
        } catch (GeekStarSecurityException $exception) {
            $handler->error->send($exception->getMessage(), $exception->getCode(), $exception->getStatus());
        }

        return $handler->response($authorizationService->registration($parameters));
    }

    /**
     * Авторизация пользователя
     *
     * @Rest\RequestParameter("csrf_token", type="string", required=true)
     * @Rest\RequestParameter("username", type="string", required=true)
     * @Rest\RequestParameter("password", type="string", required=true)
     *
     * @Route("/authorize", methods={"POST"})
     *
     * @param RestHandler $handler
     * @param Request $request
     * @param AuthorizationService $authorizationService
     * @param SecurityValidator $validator
     * @return mixed
     * @throws \Creonit\RestBundle\Exception\RestErrorException
     * @throws \Exception
     */
    public function authorize(
        RestHandler $handler,
        Request $request,
        AuthorizationService $authorizationService,
        SecurityValidator $validator
    )
    {
        $handler->checkCsrfToken('authorize');

        $handler->validate([
            'request' => [
                'username' => [new NotBlank()],
                'password' => [new NotBlank()],
            ]
        ]);

        $parameters = SecurityAuthorizeParameters::create()->fromRequest($request);

        try {
            $validator->authorizeValidate($parameters);
        } catch (GeekStarSecurityException $exception) {
            $handler->error->send($exception->getMessage(), $exception->getCode(), $exception->getStatus());
        }

        return $handler->response($authorizationService->login($parameters));
    }

    /**
     * Сменить пароль
     *
     * @Rest\RequestParameter("csrf_token", type="string", required=true)
     * @Rest\RequestParameter("oldPassword", type="string", required=true)
     * @Rest\RequestParameter("newPassword", type="string", required=true)
     *
     * @Route("/changePassword", methods={"POST"})
     *
     * @param RestHandler $handler
     * @param Request $request
     * @param AuthorizationService $authorizationService
     * @param SecurityValidator $validator
     * @return mixed
     * @throws \Creonit\RestBundle\Exception\RestErrorException
     * @throws \Exception
     */
    public function changePassword(
        RestHandler $handler,
        Request $request,
        AuthorizationService $authorizationService,
        SecurityValidator $validator
    )
    {
        $handler->checkCsrfToken('authorize');
        $handler->validate([
            'request' => [
                'oldPassword' => [new NotBlank()],
                'newPassword' => [new NotBlank()],
            ]
        ]);

        $handler->checkAuthorization();

        /** @var User $user */
        $user = $this->getUser();

        $parameters = SecurityChangePasswordParameters::create()
            ->fromRequest($request)
            ->setUser($user);

        try {
            $validator->changePasswordValidate($parameters);
        } catch (GeekStarSecurityException $exception) {
            $handler->error->send($exception->getMessage(), $exception->getCode(), $exception->getStatus());
        }

        return $handler->response($authorizationService->changeUserPassword($parameters));
    }

    /**
     * Выход из системы
     *
     * @Route("/logout", methods={"POST"})
     *
     * @param RestHandler $handler
     * @param Request $request
     * @param AuthorizationService $authorizationService
     * @return mixed
     * @throws \Exception
     */
    public function logout(RestHandler $handler, Request $request, AuthorizationService $authorizationService)
    {
        return $handler->response($authorizationService->logout($request));
    }
}
