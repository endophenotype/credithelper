<?php

namespace App\Business\Security\DTO;

use App\Business\Security\Model\User;
use App\Business\Security\Model\UserSign;
use App\Util\DTO\AbstractParameters;
use Symfony\Component\HttpFoundation\Request;

class SecurityChangePasswordParameters extends AbstractParameters
{
    private User $user;
    private UserSign $userSign;
    private string $provider;
    private string $oldPassword;
    private string $newPassword;

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): SecurityChangePasswordParameters
    {
        $this->user = $user;

        return $this;
    }

    public function getUserSign(): UserSign
    {
        return $this->userSign;
    }

    public function setUserSign(UserSign $userSign): SecurityChangePasswordParameters
    {
        $this->userSign = $userSign;

        return $this;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function setProvider(string $provider): SecurityChangePasswordParameters
    {
        $this->provider = $provider;

        return $this;
    }

    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): SecurityChangePasswordParameters
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    public function getNewPassword(): string
    {
        return $this->newPassword;
    }

    public function setNewPassword(string $newPassword): SecurityChangePasswordParameters
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    public function fromRequest(Request $request): SecurityChangePasswordParameters
    {
        $this->setProvider('email');
        $this->setNewPassword($request->request->get('newPassword'));
        $this->setOldPassword($request->request->get('oldPassword'));

        return $this;
    }
}