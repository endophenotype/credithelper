<?php

namespace App\Business\Security\DTO;

use App\Business\Security\Model\UserSign;
use App\Util\DTO\AbstractParameters;
use Symfony\Component\HttpFoundation\Request;

class SecurityAuthorizeParameters extends AbstractParameters
{

    private string $provider;
    private string $username;
    private string $password;
    private ?UserSign $userSign;

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function setProvider(string $provider): SecurityAuthorizeParameters
    {
        $this->provider = $provider;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): SecurityAuthorizeParameters
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): SecurityAuthorizeParameters
    {
        $this->password = $password;

        return $this;
    }

    public function getUserSign(): ?UserSign
    {
        return $this->userSign;
    }

    public function setUserSign(?UserSign $userSign): SecurityAuthorizeParameters
    {
        $this->userSign = $userSign;

        return $this;
    }

    public function fromRequest(Request $request): SecurityAuthorizeParameters
    {
        $this->setProvider('email');
        $this->setUsername($request->request->get('username'));
        $this->setPassword($request->request->get('password'));

        return $this;
    }
}