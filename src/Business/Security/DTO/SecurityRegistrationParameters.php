<?php

namespace App\Business\Security\DTO;

use App\Util\DTO\AbstractParameters;
use Symfony\Component\HttpFoundation\Request;

class SecurityRegistrationParameters extends AbstractParameters
{
    private string $name;
    private string $email;
    private string $provider;
    private string $username;
    private string $password;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): SecurityRegistrationParameters
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): SecurityRegistrationParameters
    {
        $this->email = $email;

        return $this;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function setProvider(string $provider): SecurityRegistrationParameters
    {
        $this->provider = $provider;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): SecurityRegistrationParameters
    {
        $this->username = $username;

        return $this;
    }
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): SecurityRegistrationParameters
    {
        $this->password = $password;

        return $this;
    }

    public function fromRequest(Request $request): SecurityRegistrationParameters
    {
        $this->setName($request->request->get('name'));
        $this->setEmail($request->request->get('email'));
        $this->setProvider('email');
        $this->setUsername($request->request->get('email'));
        $this->setPassword($request->request->get('password'));

        return $this;
    }
}