<?php

namespace App\Business\Security\DTO;

use App\Business\Security\Model\UserSign;
use App\Util\DTO\AbstractParameters;

class UserSignParameters extends AbstractParameters
{
    private ?UserSign $userSign = null;
    private string $provider;
    private string $username;
    private ?string $password = null;
    private ?string $salt = null;
    private ?string $secret = null;
    private ?bool $enable = null;

    public function getUserSign(): ?UserSign
    {
        return $this->userSign;
    }

    public function setUserSign($userSign): UserSignParameters
    {
        $this->userSign = $userSign;

        return $this;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function setProvider($provider): UserSignParameters
    {
        $this->provider = $provider;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername($username): UserSignParameters
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword($password): UserSignParameters
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt($salt): UserSignParameters
    {
        $this->salt = $salt;

        return $this;
    }

    public function getSecret(): ?string
    {
        return $this->secret;
    }

    public function setSecret($secret): UserSignParameters
    {
        $this->secret = $secret;

        return $this;
    }

    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable($enable): UserSignParameters
    {
        $this->enable = $enable;

        return $this;
    }
}