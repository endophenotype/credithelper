<?php

namespace App\Business\Security\Validator;

use App\Business\Security\DTO\SecurityAuthorizeParameters;
use App\Business\Security\DTO\SecurityChangePasswordParameters;
use App\Business\Security\DTO\SecurityRegistrationParameters;
use App\Business\Security\Exception\GeekStarSecurityException;
use App\Business\Security\Service\UserService;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityValidator
{
    private UserService $userService;
    private UserPasswordEncoderInterface $encoder;

    private static array $errors = [
        0 => ["Неправильная пара логин/пароль", 400],
        1 => ["Доступ запрещен", 403],
        2 => ["Вы уже зарегистрированы", 400],
        3 => ["Провайдер не найден", 400],
        4 => ["Не правильно введен старый пароль", 400],
    ];

    public function __construct(UserService $userService, UserPasswordEncoderInterface $encoder)
    {
        $this->userService = $userService;
        $this->encoder = $encoder;
    }

    public function flashError(int $code, string $message = null)
    {
        if(isset(static::$errors[$code])) {
            $message = $message ?: static::$errors[$code][0];
            throw new GeekStarSecurityException($message, $code, static::$errors[$code][1]);
        }
    }

    public function authorizeValidate(SecurityAuthorizeParameters &$parameters) {

        $userSign = $this->userService->getUserSign($parameters->getProvider(), $parameters->getUsername());
        $parameters->setUserSign($userSign);

        if(!$userSign) {
            $this->flashError(0);
        }

        if(!$userSign->getEnabled()) {
            $this->flashError(1);
        }

        $user = $userSign->getUser();

        if(!$user) {
            $this->flashError(0);
        }

        $user->setCurrentUserSign($userSign);

        if(!$this->encoder->isPasswordValid($user, $parameters->getPassword())) {
            $this->flashError(0);
        }
    }

    public function registrationValidate(SecurityRegistrationParameters &$parameters) {
        $userSign = $this->userService->getUserSign($parameters->getProvider(), $parameters->getUsername());

        if($userSign) {
            $this->flashError(2);
        }
    }

    public function changePasswordValidate(SecurityChangePasswordParameters &$parameters) {
        $user = $parameters->getUser();
        $userSign = $user->getUserSign($parameters->getProvider());
        $parameters->setUserSign($userSign);

        if(!$userSign) {
            $this->flashError(3, "Провайдер " . $parameters->getProvider() . " у пользователя не найден");
        }

        $user->setCurrentUserSign($userSign);

        if(!$this->encoder->isPasswordValid($user, $parameters->getOldPassword())) {
            $this->flashError(4);
        }
    }
}