<?php

namespace App\Business\Security\Event;

use Symfony\Contracts\EventDispatcher\Event;

class SecurityAuthorizeEvent extends Event
{
    const EVENT_NAME = 'geek_star.security.authorize';

    private int $userId;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): SecurityAuthorizeEvent
    {
        $this->userId = $userId;

        return $this;
    }
}