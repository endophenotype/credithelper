<?php

namespace App\Business\Security\Event;

use Symfony\Contracts\EventDispatcher\Event;

class SecurityRegistrationEvent extends Event
{
    const EVENT_NAME = 'geek_star.security.registration';

    private int $userId;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): SecurityRegistrationEvent
    {
        $this->userId = $userId;

        return $this;
    }
}