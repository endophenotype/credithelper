<?php

namespace App\Business\Security\Event;

use Symfony\Contracts\EventDispatcher\Event;

class SecurityLogoutEvent extends Event
{
    const EVENT_NAME = 'geek_star.security.logout';

    private int $userId;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): SecurityLogoutEvent
    {
        $this->userId = $userId;

        return $this;
    }
}