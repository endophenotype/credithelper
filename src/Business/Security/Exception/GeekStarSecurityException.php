<?php
namespace App\Business\Security\Exception;

use App\Util\Exception\ValidateException;

class GeekStarSecurityException extends ValidateException
{
}