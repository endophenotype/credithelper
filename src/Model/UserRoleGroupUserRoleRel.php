<?php

namespace App\Model;

use App\Model\Base\UserRoleGroupUserRoleRel as BaseUserRoleGroupUserRoleRel;

/**
 * Skeleton subclass for representing a row from the 'user_role_group_user_role_rel' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UserRoleGroupUserRoleRel extends BaseUserRoleGroupUserRoleRel
{

}
