<?php

namespace App\Model;

use App\Model\Base\UserRoleGroupUserRelQuery as BaseUserRoleGroupUserRelQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'user_role_group_user_rel' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UserRoleGroupUserRelQuery extends BaseUserRoleGroupUserRelQuery
{

}
