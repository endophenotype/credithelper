<?php


namespace App\Admin\Categories\Content;

use App\Admin\Categories\Content\Storage\StorageModule;
use App\Admin\ExtraCategory;

class ContentCategory extends ExtraCategory
{
    public function configure()
    {
        $this
            ->setVisible(true)
            ->setSortIndex(1)
            ->setTitle('Контент')
            ->setName('content')
            ->setPermission('ROLE_ADMIN');
    }

    public function initialize()
    {
        $this
            ->addModule(StorageModule::class);
    }
}