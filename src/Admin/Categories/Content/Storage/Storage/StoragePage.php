<?php

namespace App\Admin\Categories\Content\Storage\Storage;

class StoragePage extends \GeekStar\StorageBundle\Admin\Page\StoragePage
{
    public function configure()
    {
        $this
            ->setTitle('Контент')
            ->setName('storage')
            ->setVisible(true)
            ->setPermission('ROLE_ADMIN');
    }
}