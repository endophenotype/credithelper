<?php

namespace App\Admin\Categories\Content\Storage;

use App\Admin\Categories\Content\Storage\Storage\StoragePage;
use App\Admin\ExtraModule;

class StorageModule extends ExtraModule
{
    public function configure()
    {
        $this
            ->setTitle('Управление контентом')
            ->setName('storage')
            ->setIcon('file-image-o')
            ->setVisible(true)
            ->setPermission('ROLE_ADMIN');
    }

    public function initialize()
    {
        $this
            ->addPage(StoragePage::class);
    }
}