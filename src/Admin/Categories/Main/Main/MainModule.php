<?php

namespace App\Admin\Categories\Main\Main;

use App\Admin\Categories\Main\Main\Main\MainPage;
use App\Admin\ExtraModule;

class MainModule extends ExtraModule
{
    public function configure()
    {
        $this
            ->setTitle('Main')
            ->setName('main')
            ->setIcon('bank')
            ->setVisible(true)
            ->setPermission('ROLE_ADMIN');
    }

    public function initialize()
    {
        $this
            ->addPage(MainPage::class);
    }
}