<?php

namespace App\Admin\Categories\Main\Main\Main;

use App\Admin\Categories\Main\Main\Main\Components\MainPanelComponent;
use GeekStar\AdminBundle\Elements\Page;

class MainPage extends Page
{
    public function configure()
    {
        $this
            ->setTitle('Main page')
            ->setName('main')
            ->setVisible(true)
            ->setPermission('ROLE_ADMIN');
    }

    public function initialize()
    {
        $this
            ->addContentComponent(MainPanelComponent::class);
    }
}