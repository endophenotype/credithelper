<?php

namespace App\Admin\Categories\Main\Main\Main\Components;

use GeekStar\AdminBundle\Elements\Components\PanelComponent;
use GeekStar\AdminBundle\Elements\Request\ComponentRequest;
use GeekStar\AdminBundle\Elements\Response\ComponentResponse;
use GeekStar\AdminBundle\Manager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class MainPanelComponent extends PanelComponent
{
    protected Manager $manager;

    public function __construct(ContainerInterface $container, Environment $twig, TranslatorInterface $translator, Manager $manager)
    {
        $this->manager = $manager;

        parent::__construct($container, $twig, $translator);
    }

    public function configure()
    {
        $this
            ->setTitle('Main panel')
            ->setContentHeight('200px')
            ->setBorderBottom('info')

            ->addMenuItem('Open admin', '/admin/')
            ->addMenuItem('Reload panel', null, 'load');
    }

    public function initialize(ComponentRequest $request, ComponentResponse $response)
    {
        $this
            ->addContentItem('
                <span>Hello, ' . $this->manager->user->getNameInAdmin() . '!</span>
            ')
            ->end();
    }
}