<?php


namespace App\Admin\Categories\Main;

use App\Admin\Categories\Main\Main\MainModule;
use App\Admin\ExtraCategory;

class MainCategory extends ExtraCategory
{
    public function configure()
    {
        $this
            ->setVisible(true)
            ->setSortIndex(1)
            ->setName('main')
            ->setPermission('ROLE_ADMIN');
    }

    public function initialize()
    {
        $this
            ->addModule(MainModule::class);
    }
}