<?php

namespace App\Util\DTO;

abstract class AbstractParameters
{
    public static function create()
    {
        return new static();
    }
}