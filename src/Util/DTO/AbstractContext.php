<?php

namespace App\Util\DTO;

abstract class AbstractContext
{
    public static function create()
    {
        return new static();
    }
}