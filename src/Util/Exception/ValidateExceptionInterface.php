<?php

namespace App\Util\Exception;

use Throwable;

interface ValidateExceptionInterface
{
    public static function create($message = "", $code = 0, $status = 200, $requestField = "", Throwable $previous = null): ValidateExceptionInterface;

    /** @return string */
    public function getMessage();
    public function setMessage(string $message): ValidateExceptionInterface;

    /** @return int */
    public function getCode();
    public function setCode(int $code): ValidateExceptionInterface;

    public function getStatus(): int;
    public function setStatus(int $status): ValidateExceptionInterface;

    public function getRequestField(): string;
    public function setRequestField(string $requestField): ValidateExceptionInterface;
}