<?php

namespace App\Util\Exception;

use Throwable;

class ValidateException extends \RuntimeException implements ValidateExceptionInterface
{
    protected string $requestField;
    protected int $status;

    public function __construct($message = "", $code = 0, $status = 200, $requestField = "", Throwable $previous = null)
    {
        $this->status = $status;
        $this->requestField = $requestField;

        parent::__construct($message, $code, $previous);
    }

    public static function create($message = "", $code = 0, $status = 200, $requestField = "", Throwable $previous = null): ValidateExceptionInterface
    {
        return new static($message, $code, $status, $requestField, $previous);
    }

    public function setMessage(string $message): ValidateException
    {
        $this->message = $message;

        return $this;
    }

    public function setCode(int $code): ValidateException
    {
        $this->code = $code;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): ValidateException
    {
        $this->status = $status;

        return $this;
    }

    public function getRequestField(): string
    {
        return $this->requestField;
    }

    public function setRequestField(string $requestField): ValidateException
    {
        $this->requestField = $requestField;

        return $this;
    }
}