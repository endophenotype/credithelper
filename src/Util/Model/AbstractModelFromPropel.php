<?php

namespace App\Util\Model;

use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use ReflectionClass;

abstract class AbstractModelFromPropel implements ModelInterface
{
    const PROPEL_CLASS = '';

    protected ActiveRecordInterface $propelModel;

    protected static array $schemas = [];

    protected array $parameters = [];

    protected bool $isNew;

    public function __construct(ActiveRecordInterface $propelModel = null)
    {
        //build from annotation
        static::buildFromAnnotation();

        //init propel model
        $propelModelClass = static::PROPEL_CLASS;

        if(!$propelModel) {
            $this->isNew = true;
            $this->propelModel = new $propelModelClass();

            foreach (static::$schemas[static::class]['parameterNames']  as $name) {
                $this->parameters[$name] = null;
            }

        } else {
            $this->isNew = false;

            if(get_class($propelModel) !== $propelModelClass) {
                throw new \Exception("Expected class $propelModelClass in property propelModel.");
            }

            $this->propelModel = $propelModel;
            $this->fromPropelModel();
        }
    }

    public static function buildFromAnnotation()
    {
        if(!static::PROPEL_CLASS) {
            throw new \Exception("Not found PROPEL_CLASS constant.");
        }

        if(!isset(static::$schemas[static::class])) {
            static::$schemas[static::class]['methods'] = [];
            static::$schemas[static::class]['relParameters'] = [];
            static::$schemas[static::class]['parameterNames'] = [];

            $r = new ReflectionClass(static::class);
            $doc = $r->getDocComment();

            //propelRelParams
            preg_match_all('#@propelRelParam(.*?)\n#s', $doc, $propelRelParams);

            foreach ($propelRelParams[1] as $param) {
                $param = explode(" ", trim($param));
                if(count($param) != 2) {
                    throw new \Exception("Incorrect property propelRelParam.");
                }
                static::$schemas[static::class]['relParameters'][$param[0]] = $param[1];
            }

            //methods
            preg_match_all('#@method(.*?)\(#s', $doc, $methods);

            foreach ($methods[1] as $method) {
                $method = trim($method);
                $pos = strrpos($method, " ");

                if($pos !== false) {
                    $method = substr($method, $pos + 1);
                }

                static::$schemas[static::class]['methods'][] = $method;

                $prefix = substr($method, 0, 3);
                if($prefix == 'get' or $prefix == 'set') {
                    $property = strtolower($method[3]) . substr($method, 4);
                    if(!in_array($property, static::$schemas[static::class]['parameterNames'])) {
                        static::$schemas[static::class]['parameterNames'][] = $property;
                    }
                }
            }
        }
    }

    public function __call($name, $arguments)
    {
        //build from annotation
        static::buildFromAnnotation();

        if(!in_array($name, static::$schemas[static::class]['methods'])) {
            throw new \Exception("Method $name doesn't exist.");
        }

        $prefix = substr($name, 0, 3);

        if($prefix == 'get' or $prefix == 'set') {
            $property = strtolower($name[3]) . substr($name, 4);

            switch ($prefix) {
                case 'get':
                    if(isset(static::$schemas[static::class]['relParameters'][$property]) and !isset($this->parameters[$property])) {
                        $getProperty = 'get' . ucfirst($property);
                        $propertyPropelModel = $this->propelModel->$getProperty();
                        $propertyClass = static::$schemas[static::class]['relParameters'][$property];

                        $this->parameters[$property] = new $propertyClass($propertyPropelModel);
                    }
                    return $this->parameters[$property];

                case 'set':
                    if (count($arguments) != 1) {
                        throw new \Exception("Setter for $name requires exactly one parameter.");
                    }
                    $this->parameters[$property] = $arguments[0];
                    return $this;
            }
        }

        throw new \Exception("Property $name doesn't exist.");
    }

    public function getPropelModel(): ?ActiveRecordInterface
    {
        return $this->propelModel;
    }

    public function fromPropelModel() {
        //build from annotation
        static::buildFromAnnotation();

        if($this->propelModel) {
            foreach (static::$schemas[static::class]['parameterNames'] as $name) {
                $getProperty = 'get' . ucfirst($name);

                if(!isset(static::$schemas[static::class]['relParameters'][$name])) {
                    $this->parameters[$name] = $this->propelModel->$getProperty();
                }
            }
        }
    }

    public function toPropelModel() {
        //build from annotation
        static::buildFromAnnotation();

        if($this->propelModel) {
            foreach (static::$schemas[static::class]['parameterNames'] as $name) {
                if(isset($this->parameters[$name])) {
                    if(!$this->isNew() or ($this->isNew() and $this->parameters[$name] !== null)) {
                        $setProperty = 'set' . ucfirst($name);

                        if(!isset(static::$schemas[static::class]['relParameters'][$name])) {
                            $this->propelModel->$setProperty($this->parameters[$name]);
                        } else {
                            if($paramPropelModel = $this->parameters[$name]->getPropelModel()) {
                                $this->propelModel->$setProperty($paramPropelModel);
                            }
                        }
                    }
                }
            }
        }
    }

    public function isNew(): bool
    {
        return $this->isNew;
    }

    public function delete() {
        if($this->propelModel) {
            $this->propelModel->delete();
        } else {
            throw new \Exception("Propel object not initial.");
        }
    }

    public function save() {
        if($this->propelModel) {
            $this->toPropelModel();

            $this->propelModel->save();

            $this->isNew = false;

            $this->fromPropelModel();

        } else {
            throw new \Exception("Propel object not initial.");
        }
    }
}