<?php

namespace App\Util\Model;

interface ModelInterface
{
    public function isNew(): bool;
    public function delete();
    public function save();
}