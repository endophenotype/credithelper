<?php

namespace App\Util\Validator;

use App\Util\Exception\ValidateExceptionInterface;

abstract class AbstractValidator
{
    public static ValidateExceptionInterface $exception;

    public static array $errors = [
        0 => ["Системная ошибка", 500],
    ];

    public function flashError(int $code, string $message = null, string $requestField = "")
    {
        if(isset(static::$errors[$code])) {
            $message = $message ?: static::$errors[$code][0];
            throw static::$exception::create($message, $code, static::$errors[$code][1], $requestField);
        }
    }
}