<?php

namespace App\Event;

use App\Util\Exception\ValidateExceptionInterface;
use Creonit\RestBundle\Handler\RestError;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class RestValidatorListener
{
    protected ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if (!preg_match('#^/api/#', $event->getRequest()->getPathInfo())) {
            return;
        }

        if ($this->container->get('request_stack')->getCurrentRequest() !== $event->getRequest()) {
            return;
        }

        $exception = $event->getThrowable();

        if ($exception instanceof ValidateExceptionInterface) {
            $event->stopPropagation();
            $error = new RestError();

            if($exception->getRequestField()) {
                $error->set($exception->getRequestField(), $exception->getMessage());
            } else {
                $error->setMessage($exception->getMessage());
            }

            $error->setCode($exception->getCode());
            $error->setStatus($exception->getStatus());
            $error->send();
        }
    }
}
