let fs = require('fs')
let resolve = require('path').resolve
let join = require('path').join
let cp = require('child_process')

let buildArgs = ['build'];
if(process.env.NODE_MAX_RAM !== '') {
    buildArgs[1] = '--max-old-space-size=' + process.env.NODE_MAX_RAM;
}

let directoryTemplates = [
    resolve(__dirname, '../vendor/*/*/'),
    resolve(__dirname, './')
]

let directories = [];

let handleDirs = function (dir, index) {
    let commandInstall = () => {
        cp.spawn('yarn', ['install'], { env: process.env, cwd: dir, stdio: 'inherit' }).on("close", code => {
            commandBuild();
        })
    };

    let commandBuild = () => {
        cp.spawn('yarn', buildArgs, { env: process.env, cwd: dir, stdio: 'inherit' }).on("close", code => {
            if(directories[index + 1] !== undefined) {
                handleDirs(directories[index + 1], index + 1)
            }
        })
    };

    commandInstall();
}

let readDir = function (dir) {
    let rPos = dir.indexOf('*')

    if (rPos !== -1) {
        let rootDir = dir.slice(0, rPos)
        let subDir = dir.slice(rPos + 2)

        fs.readdirSync(rootDir)
            .forEach(function (mod) {
                let stat = fs.statSync(rootDir + mod)
                if (stat.isDirectory()) {
                    readDir(rootDir + mod + '/' + subDir)
                }
            })
    } else {
        if (fs.existsSync(join(dir, 'package.json'))) {
            directories.push(dir);
        }
    }
}

//build package
directoryTemplates.forEach((dir) => {
    readDir(dir)
})
if(directories[0] !== undefined) {
    handleDirs(directories[0], 0)
}
