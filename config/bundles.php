<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    HWI\Bundle\OAuthBundle\HWIOAuthBundle::class => ['all' => true],
    Http\HttplugBundle\HttplugBundle::class => ['all' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    Symfony\Bundle\DebugBundle\DebugBundle::class => ['dev' => true, 'test' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    Symfony\WebpackEncoreBundle\WebpackEncoreBundle::class => ['all' => true],
    Propel\Bundle\PropelBundle\PropelBundle::class => ['all' => true],
    Creonit\PropelSchemaConverterBundle\CreonitPropelSchemaConverterBundle::class => ['all' => true],
    Creonit\RestBundle\CreonitRestBundle::class => ['all' => true],
    GeekStar\AdminBundle\GeekStarAdminBundle::class => ['all' => true],
    GeekStar\MarkupBundle\GeekStarMarkupBundle::class => ['all' => true],
    Gregwar\ImageBundle\GregwarImageBundle::class => ['all' => true],
    GeekStar\FileBundle\GeekStarFileBundle::class => ['all' => true],
    GeekStar\StorageBundle\GeekStarStorageBundle::class => ['all' => true],
];
